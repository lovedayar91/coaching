@section('styles')

    <link rel="stylesheet" href="{{site_path()}}/css/jquery-ui.css">
    <style>

        @media (max-width: 475.98px) {
            .boxes .col-sm-6 div#datatable_filter {
                float: none;
                text-align: center;
            }

            .boxes .col-sm-6 {
                float:  none;
                text-align: center;
                display:  inline-block;
                width:  10px;
            }
        }

        @media (min-width: 476px) and (max-width: 767.98px) {
            .boxes .col-sm-6 div#datatable_filter {
                float: right;
            }

            .boxes .col-sm-6 {
                float:  right;
                display:  inline-block;
                width:  50%;
            }
        }

        .dropify-wrapper{
            height: 150px !important;
        }

        .custombox-modal-container.custombox-modal-container-fadein {
            width: 950px !important;
        }

        a#rmvImage {
            display: list-item;
        }

    </style>
@endsection

@extends('dashboard.index')
@section('title')
    تعديل
@endsection
@section('content')

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive boxes">
                <!-- /*** start multiable form HTML ***/ -->
                <form action="{{route('updatecoach')}}" method="post" autocomplete="off" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="id" value="{{$data->id}}">
                    <div class="row tab" style="margin-top: 15px;">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-1" class="col-sm-4 control-label">الأسم بالعربية</label>
                                <input type="text" autocomplete="nope" id="name_ar" name="name_ar" value="{{$data->name_ar}}" required class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-1" class="col-sm-4 control-label">الأسم بالانجليزية</label>
                                <input type="text" autocomplete="nope" id="name_en" name="name_en" value="{{$data->name_en}}" required class="form-control">
                            </div>
                        </div>  
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-1" class="col-sm-4 control-label">البريد الالكتروني</label>
                                <input type="email" autocomplete="nope" id="email" name="email" value="{{$data->email}}" required class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-1" class="col-sm-6 control-label">الرقم التعريفي لسكاي بي</label>
                                <input type="text" autocomplete="nope" id="skype_id" name="skype_id" value="{{$data->skype_id}}" required class="form-control">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label class="col-sm-4 control-label">التفاصيل بالعربية</label>
                                    <textarea name="desc_ar" id="desc_ar" class="form-control" id="" cols="30" rows="10">{{$data->desc_ar}}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label class="col-sm-4 control-label">التفاصيل بالانجليزية</label>
                                    <textarea name="desc_en" id="desc_en" class="form-control" id="" cols="30" rows="10">{{$data->desc_en}}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Holiday Dates -->
                    <div class="row tab">
                        <h4 class="custom-modal-title" style="background-color: #4c739a;width:50%;margin-bottom: 15px;margin-top: 15px">
                           تواريخ الاجازات
                        </h4>
                        
                        <div id="datesDiv">
                            @foreach ($data->Dates as $item)
                                <div class="col-sm-2" id="date{{$item->id}}" style="margin-bottom: 10px">
                                    <input type="text" style="text-align: center" id="" value="{{$item->date}}" class="form-control" readonly>
                                    <a onclick="removeDate('{{$item->id}}')" class="btn btn-danger" id="rmvDate" style="width:100%;margin-top:5px">حذف</a>
                                </div>  
                            @endforeach
                        </div>

                        <div class="col-xs-12"><hr></div>

                        <div class="col-sm-8" id="addDateDiv">
                            <input type="text" name="date" id="date" value="" class="form-control datepicker" readonly onclick="check_avaliable_date()">
                        </div>  
                        <div class="col-xs-4">
                            <a onclick="addDate()" class="btn btn-danger" id="addDate" style="margin-top:5px">اضافة تاريخ جديد</a>    
                        </div>                      
                        

                    </div>

                    <!-- images -->
                    <div class="row tab">
                        <h4 class="custom-modal-title" style="background-color: #4c739a;width:50%;margin-bottom: 15px;margin-top: 15px">
                           الصور
                        </h4>
                        
                        @foreach ($data->Images as $image)
                            <div class="col-sm-4" id="image{{$image->id}}" style="margin-bottom: 10px">
                                <img src="{{url(''.$image->image)}}" style="width: 100%;height: 150px;">
                                <a onclick="rmvImage('{{$image->id}}')" class="btn btn-danger" id="rmvImage">حذف</a>
                            </div>    
                        @endforeach

                        <div class="col-xs-12"><hr></div>
                        
                        <div class="col-sm-4" style="margin-bottom: 10px">
                            <input type="file" name="image[]" multiple>
                        </div>
                    </div>

                    <div class="row" style="overflow:auto;">
                        <div class="col-xs-12" style="text-align:center;">
                            <button class="btn btn-success" type="submit" style="width: 50%">حفظ</button>
                        </div>
                    </div>

                </form>
            </div>
        </div><!-- end col -->

    </div>

@endsection

@section('script')

    <script src="{{site_path()}}/js/jquery-ui.js"></script>
    <script>
        $(document).ready(function(){
            check_avaliable_date();
        });
        
        function rmvImage(id) {
            if(id == '') return false;
            var token = '{{csrf_token()}}';
            $.ajax({
                type     : 'POST',
                url      : '{{ route('rmvImage') }}' ,
                datatype : 'json' ,
                data     : {
                    'id'         :  parseInt(id) ,
                    '_token'     :  token
                }, success   : function(msg){
                    if(msg=='err'){
                        $('#rmvImage').html('لا يمكن حذف اخر صورة');
                        return false;
                    }
                    $('#image'+id).fadeOut();
                }
            });
        }

        function addDate() {
            var date = $('#date').val();
            if(date == '' || date == null) return false;
            $.ajax({
                type     : 'POST',
                url      : '{{ route('addData') }}' ,
                datatype : 'json' ,
                data     : {
                    'coach_id'   :  parseInt('{{$data->id}}') ,
                    'date'       :  date ,
                    '_token'     :  '{{csrf_token()}}'
                }, success   : function(msg){
                    $('#datesDiv').append('<div class="col-sm-2" id="date'+msg.id+'" style="margin-bottom: 10px"><input type="text" style="text-align: center" id="" value="'+date+'" class="form-control" readonly><a onclick="removeDate('+msg.id+')" class="btn btn-danger" id="rmvDate" style="width:100%;margin-top:5px">حذف</a></div>');
                    $('#date').val('');
                }
            });
        }

        function removeDate(id) {
            if(id == '') return false;
            var token = '{{csrf_token()}}';
            $.ajax({
                type     : 'POST',
                url      : '{{ route('rmvData') }}' ,
                datatype : 'json' ,
                data     : {
                    'id'         :  parseInt(id) ,
                    '_token'     :  token
                }, success   : function(msg){
                    if(msg=='err'){
                        $('#rmvData').html('لا يمكن حذف اخر صورة');
                        return false;
                    }
                    $('#date'+id).fadeOut();
                }
            });
        }

        function check_avaliable_date() {
            eventHolidays   = {};
            eventDates      = {};
            holidays        = '';
            dates           = '';

            for (var i = 0; i < dates.length; i++) {
                eventDates[new Date(dates[i])] = new Date(dates[i]);
            }
            for (var i = 0; i < holidays.length; i++) {
                eventHolidays[new Date(holidays[i])] = new Date(holidays[i]);
            }

            $.ajax({
                type: "POST",
                url: "{{route('check_avaliable_date_dashboard')}}",
                data: {coach_id: parseInt('{{$data->id}}'), _token: '{{csrf_token()}}'},
                success: function( msg ) {
                    holidays = msg.holidays;
                    dates    = msg.dates;
                    console.log(holidays,dates);
                    for (var i = 0; i < dates.length; i++) {
                        eventDates[new Date(dates[i])] = new Date(dates[i]);
                    }
                    for (var i = 0; i < holidays.length; i++) {
                        eventHolidays[new Date(holidays[i])] = new Date(holidays[i]);
                    }
                }
            });    
        }

        var eventHolidays = {};
        var eventDates = {};

        var holidays = '';
        var dates    = '';

        for (var i = 0; i < dates.length; i++) {
            eventDates[new Date(dates[i])] = new Date(dates[i]);
        }
        for (var i = 0; i < holidays.length; i++) {
            eventHolidays[new Date(holidays[i])] = new Date(holidays[i]);
        }

        $(".datepicker").datepicker({
            dateFormat: "yy-mm-dd", //format
            minDate: 0, // disabled old dates
            // multidate: true, //multiable
            beforeShowDay: function (date) {

                var highlight = eventDates[date];
                var value1    = eventHolidays[date];

                if (value1) {

                    return [true, "ui-state-disabled", 'holiday'];

                } else if (highlight) {

                    return [true, "events ui-state-disabled", 'reserved'];

                } else {

                    return [true, '', ''];

                };
            }
        });
    </script>
@endsection