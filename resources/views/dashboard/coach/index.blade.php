@section('styles')

    <style>

        @media (max-width: 475.98px) {
            .boxes .col-sm-6 div#datatable_filter {
                float: none;
                text-align: center;
            }

            .boxes .col-sm-6 {
                float:  none;
                text-align: center;
                display:  inline-block;
                width:  10px;
            }
        }

        @media (min-width: 476px) and (max-width: 767.98px) {
            .boxes .col-sm-6 div#datatable_filter {
                float: right;
            }

            .boxes .col-sm-6 {
                float:  right;
                display:  inline-block;
                width:  50%;
            }
        }

    </style>
@endsection

@extends('dashboard.index')
@section('title')
    المدربات
@endsection
@section('content')

    <div class="row">

        {{--  <div class=" btn-group-justified m-b-10">
            <a href="#add" class="btn btn-success waves-effect btn-lg waves-light" data-animation="fadein" data-plugin="custommodal"
                data-overlaySpeed="100" data-overlayColor="#36404a">اضافة مدربة <i class="fa fa-plus"></i> </a>
            <a href="#deleteAll" class="btn btn-danger waves-effect btn-lg waves-light delete-all" data-animation="blur" data-plugin="custommodal"
                data-overlaySpeed="100" data-overlayColor="#36404a">حذف المحدد <i class="fa fa-trash"></i> </a>
            <a class="btn btn-primary waves-effect btn-lg waves-light" onclick="window.location.reload()" role="button">تحديث الصفحة <i class="fa fa-refresh"></i> </a>
        </div>  --}}

        <div class="col-sm-12">
            <div class="card-box table-responsive boxes">

                <table id="datatable" class="table table-bordered table-responsives">
                    <thead>
                    <tr>
                        {{--  <th>
                            تحديد
                            <input type="checkbox" id="checkedAll" style="margin-right: 10px">
                        </th>  --}}
                        <th> الصورة</th>
                        <th> الاسم</th>
                        <th> البريد الالكتروني</th>
                        <th>تاريخ التسجيل</th>
                        <th>التحكم</th>
                    </tr>
                    </thead>
                    <tbody class="text-center">
                    @foreach($data as $item)
                        <tr>
                            {{--  <td>
                                <input type="checkbox" class="form-check-label checkSingle" id="{{$item->id}}">
                            </td>  --}}

                            <td><img src="{{$item->Images->count() > 0 ? $item->Images->first()->image : ''}}" alt="user-img" width="100px" height="75px" title="Mat Helme" class="img-thumbnail img-responsive" style="height: 70px !important;"></td>
                            <td>{{ $item->name_ar }}</td>
                            <td>{{ $item->email }}</td>
                            <td>{{$item->created_at->diffForHumans()}}</td>
                            <td>
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <a href="{{route('editcoach',$item->id)}}" class="btn btn-success">
                                        <i class="fa fa-cogs"></i>
                                    </a>
                                    {{--  <a href="#delete" class="delete btn btn-danger" style="color: #c83338; font-weight: bold;" data-animation="blur" data-plugin="custommodal"
                                        data-overlaySpeed="100" data-overlayColor="#36404a"
                                        data-id = "{{$item->id}}"
                                    >
                                        <i class="fa fa-trash"></i>
                                    </a>  --}}
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div><!-- end col -->

    </div>

    <!-- add item modal -->
    <div id="add" class="modal-demo" >
        <button type="button" class="close" onclick="Custombox.close();" style="opacity: 1">
            <span>&times</span><span class="sr-only" style="color: #f7f7f7">Close</span>
        </button>
        <h4 class="custom-modal-title" style="background-color: #36404a">
            مدربة جديدة
        </h4>
        <form action="{{route('addcoach')}}" method="post" autocomplete="off" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="modal-body">
                <div class="row" style="margin-top: 15px;">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="field-1" class="col-sm-4 control-label">الأسم بالعربية</label>
                            <input type="text" autocomplete="nope" id="" name="name_ar" required class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="field-1" class="col-sm-4 control-label">الأسم بالانجليزية</label>
                            <input type="text" autocomplete="nope" id="" name="name_en" required class="form-control">
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="field-1" class="col-sm-4 control-label">البريد الالكتروني</label>
                            <input type="email" autocomplete="nope" id="" name="email" required class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="field-1" class="col-sm-6 control-label">الرقم التعريفي لسكاي بي</label>
                            <input type="text" autocomplete="nope" id="" name="skype_id" required class="form-control">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="col-md-12">
                                <label class="col-sm-4 control-label">التفاصيل بالعربية</label>
                                <textarea name="desc_ar" class="form-control" id="" cols="30" rows="10"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="col-md-12">
                                <label class="col-sm-4 control-label">التفاصيل بالانجليزية</label>
                                <textarea name="desc_en" class="form-control" id="" cols="30" rows="10"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="col-sm-4 control-label">الصورة </label>
                                <input type="file" name="image[]" class="dropify" data-max-file-size="30000M" multiple>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success waves-effect waves-light">اضافة</button>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal" onclick="Custombox.close();">رجوع</button>
            </div>
        </form>
    </div>

    <!-- edit item modal -->
    <div id="edit" class="modal-demo">
        <button type="button" class="close" onclick="Custombox.close();" style="opacity: 1">
            <span>&times</span><span class="sr-only" style="color: #f7f7f7">Close</span>
        </button>
        <h4 class="custom-modal-title" style="background-color: #36404a">
            تعديل <span id="itemname"></span>
        </h4>
        <form action="{{route('updatecoach')}}" method="post" autocomplete="off" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="id" value="">
            <div class="modal-body">
                <div class="row" style="margin-top: 15px;">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="field-1" class="col-sm-4 control-label">الأسم بالعربية</label>
                            <input type="text" autocomplete="nope" id="name_ar" name="name_ar" required class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="field-1" class="col-sm-4 control-label">الأسم بالانجليزية</label>
                            <input type="text" autocomplete="nope" id="name_en" name="name_en" required class="form-control">
                        </div>
                    </div>  
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="field-1" class="col-sm-4 control-label">البريد الالكتروني</label>
                            <input type="email" autocomplete="nope" id="email" name="email" required class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="field-1" class="col-sm-6 control-label">الرقم التعريفي لسكاي بي</label>
                            <input type="text" autocomplete="nope" id="skype_id" name="skype_id" required class="form-control">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="col-md-12">
                                <label class="col-sm-4 control-label">التفاصيل بالعربية</label>
                                <textarea name="desc_ar" id="desc_ar" class="form-control" id="" cols="30" rows="10"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="col-md-12">
                                <label class="col-sm-4 control-label">التفاصيل بالانجليزية</label>
                                <textarea name="desc_en" id="desc_en" class="form-control" id="" cols="30" rows="10"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="col-sm-4 control-label">الصورة </label>
                                <input type="file" name="image[]" class="dropify" data-max-file-size="30000M" multiple> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success waves-effect waves-light">تعديل</button>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal" onclick="Custombox.close();">رجوع</button>
            </div>
        </form>
    </div>

   <div id="delete" class="modal-demo" style="position:relative; right: 32%">
        <button type="button" class="close" onclick="Custombox.close();" style="opacity: 1">
            <span>&times</span><span class="sr-only" style="color: #f7f7f7">Close</span>
        </button>
        <h4 class="custom-modal-title">حذف رأي</h4>
        <div class="custombox-modal-container" style=" height: 160px;">
            <div class="row">
                <div class="col-sm-12">
                    <h3 style="margin-top: 35px">
                        هل تريد مواصلة عملية الحذف ؟
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <form action="{{route('deletecoach')}}" method="post">
                        {{csrf_field()}}
                        <input type="hidden" name="delete_id" value="">
                        <button style="margin-top: 35px" type="submit" class="btn btn-danger btn-rounded w-md waves-effect waves-light m-b-5 send-delete-all"  style="margin-top: 19px">حـذف</button>
                    </form>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div>

    <div id="deleteAll" class="modal-demo" style="position:relative; right: 32%">
        <button type="button" id="close-deleteAll" class="close" onclick="Custombox.close();" style="opacity: 1">
            <span>&times</span><span class="sr-only" style="color: #f7f7f7">Close</span>
        </button>
        <h4 class="custom-modal-title">حذف المحدد</h4>
        <div class="custombox-modal-container" style=" height: 160px;">
            <div class="row">
                <div class="col-sm-12">
                    <h3 style="margin-top: 35px">
                        هل تريد مواصلة عملية الحذف ؟
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <button style="margin-top: 35px" type="submit" class="btn btn-danger btn-rounded w-md waves-effect waves-light m-b-5 send-delete-all" style="margin-top: 19px">حـذف</button>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div>

@endsection

@section('script')

    <script>
        function changeChecked(id) {
            var tokenv  = "{{csrf_token()}}";
            $.ajax({
                type     : 'POST',
                url      : '{{ route('change-activation') }}' ,
                datatype : 'json' ,
                data     : {
                    'id'         :  id ,
                    '_token'     :  tokenv
                }, success   : function(msg){
                    //success here
                    if(msg == 0)
                        return false;
                }
            });
        }

        $('.edit').on('click',function(){
            //get valus
            let id           = $(this).data('id');
            let name_ar      = $(this).data('name_ar');
            let name_en      = $(this).data('name_en');
            let skype_id     = $(this).data('skype_id');
            let email        = $(this).data('email');
            let desc_ar      = $(this).data('desc_ar');
            let desc_en      = $(this).data('desc_en');

            $("input[name='id']").val(id);
            $("#name_ar").val(name_ar);
            $("#name_en").val(name_en);
            $("#skype_id").val(skype_id);
            $("#email").val(email);
            $("#desc_ar").html(desc_ar);
            $("#desc_en").html(desc_en);
        });

        $('.delete').on('click',function(){

            let id         = $(this).data('id');

            $("input[name='delete_id']").val(id);

        });

        $("#checkedAll").change(function(){
            if(this.checked){
                $(".checkSingle").each(function(){
                    this.checked=true;
                })
            }else{
                $(".checkSingle").each(function(){
                    this.checked=false;
                })
            }
        });

        $(".checkSingle").click(function () {
            if ($(this).is(":checked")){
                var isAllChecked = 0;
                $(".checkSingle").each(function(){
                    if(!this.checked)
                        isAllChecked = 1;
                })
                if(isAllChecked == 0){ $("#checkedAll").prop("checked", true); }
            }else {
                $("#checkedAll").prop("checked", false);
            }
        });

        $('.send-delete-all').on('click', function (e) {
            var itemsIds = [];
            $('.checkSingle:checked').each(function () {
                var id = $(this).attr('id');
                itemsIds.push({
                    id: id,
                });
            });
            var requestData = JSON.stringify(itemsIds);
            // console.log(requestData);
            if (itemsIds.length > 0) {
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: "{{route('deletecoachs')}}",
                    data: {data: requestData, _token: '{{csrf_token()}}'},
                    success: function( msg ) {
                        if (msg == 'success') {
                            location.reload();
                        }else{
                            $('#close-deleteAll').trigger('click');
                        }
                    }
                });
            }else{
                $('#close-deleteAll').trigger('click');
            }
        });
    </script>

@endsection