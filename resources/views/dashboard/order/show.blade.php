@section('styles')
    
    <style>

        .btn-group-justified {
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
            align-items: center;
            min-height: 550px;
        }

        .btn-group-justified > .btn, .btn-group-justified > .btn-group {
            display: inline-block;
            width: 30% !important;
            height: 150px;
            line-height: 130px;
            margin: 0 15px;
            font-size: 18px;
            border-radius: 5px;
        }

        a.btn.waves-effect.btn-lg.waves-light {
            border-color: rgba(0,0,0,.075);
        }

        .waves-light{
            color:white;
        }

        .waves-light:hover{
            color:white !important;
        }

        @media (max-width: 475.98px) {
            .boxes .col-sm-6 div#datatable_filter {
                float: none;
                text-align: center;
            }

            .boxes .col-sm-6 {
                float:  none;
                text-align: center;
                display:  inline-block;
                width:  10px;
            }
        }

        @media (min-width: 476px) and (max-width: 767.98px) {
            .boxes .col-sm-6 div#datatable_filter {
                float: right;
            }

            .boxes .col-sm-6 {
                float:  right;
                display:  inline-block;
                width:  50%;
            }
        }

        @media (max-width:991px){
            .circles::after{
                display: none !important;
            }
        }

        body {
            font-family: 'Cairo', sans-serif;
            direction: rtl
        }

        .modal-content {
            position: relative;
        }

        .modal-content .close {
            background: #13679b;
            color: #fff;
            opacity: 1;
            position: absolute;
            right: -43px;
            width: 43px;
            height: 43px;
            font-size: 35px;
            border-radius: 5px;
            line-height: 43px;
        }

        h3.blue {
            margin-bottom: 20px;
            font-weight: bold;
        }

        .blue {
            color: #13679b;
        }

        .circles {
            position: relative;
        }

        .circle {
            text-align: center;
            font-size: 16px;
            position: relative;
            color: #b0b0b0;
        }

        .circle span.active {
            border: 4px solid #13679b;
        }

        .circle span {
            display: block;
            margin: 0 auto 10px;
            width: 25px;
            height: 25px;
            border-radius: 50%;
            border: 4px solid #e9e9e9;
            background: #fff;
            position: relative;
            z-index: 1;
        }

        .circles::after {
            position: absolute;
            content: ''!important;
            width: 70%;
            height: 4px;
            top: 25%;
            z-index: 0;
            transform: translateY(-50%);
            background: #eee;
            left: 0;
            right: 0;
            margin: auto;
        }

        .order-dtail .title {
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        .order-dtail .title span,
        .del span:last-of-type {
            color: #b0b0b0;
        }

        .del {
            justify-content: space-around;
            text-align: right;
            padding: 10px;
        }

        .del span:first-of-type {
            font-weight: bold;
            display: inline-block;
            width: 500px;
            font-size: 16px;
            margin-left: 10px;
        }

        .del span:last-of-type {
            font-size: 14px;
            font-weight: bold;
        }

        .table-new,
        .table-new th,
        .table-new td {
            text-align: center;
        }

        .table-new.table>thead>tr>th,
        .table-new.table>tr>td,
        .table-new .table>tr>th,
        .table-new.table>tbody>tr>td,
        .table-new.table>tbody>tr>th,
        .table-new.table>tfoot>tr>td,
        .table-new.table>tfoot>tr>th,
        .table-new.table>thead>tr>td,
        .table-new.table>thead>tr>th {
            border: none
        }

        .gray {
            color: #b0b0b0;
        }

        ul {
            list-style: none;
        }

        .order-dtail ul li span {
            padding: 5px 15px;
            margin-bottom: 5px;
            display: inline-block;
        }

        .order-dtail ul li span:first-of-type {
            font-weight: bold;
            font-size: 14px;
        }

        .order-dtail ul li span:last-of-type {
            font-weight: bold;
            font-size: 14px;
            color: #b0b0b0;
        }

        .order-dtail {
            position: relative;
        }

        .map {
            position: absolute;
            top: 10px;
            border-radius: 50%;
            left: 10px;
        }

        .modal-header {
            pos
        }

    </style>
@endsection

@extends('dashboard.index')
@section('title')
    عرض الطلب
@endsection

@section('content')

    <div class="order-dtail panel">
        <div class="container">
            <h3 class="blue">تفاصيل الطلب</h3>
            <div class="circles clearfix">
                <div class="row">
                    <div class="col-md-4 col-xs-12 after">
                        <div class="circle">
                            <span class="{{ $data->status == 0 ? 'active' : '' }}"></span>
                            طلب جديد
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12 after">
                        <div class="circle">
                            <span class="{{ $data->status == 3 || $data->status == 4 ? 'active' : '' }}"></span>
                            قيد التسليم
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12 after">
                        <div class="circle">
                            <span class="{{ $data->status == 5 || $data->status == 6 ? 'active' : '' }}"></span>
                            نم التسليم
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="order-dtail panel">
        <div class="container">
            <div class="title">
                <h3 class="blue">الطلبات</h3>
                <span>{{ is_null($data->order_number) ? $data->id : $data->order_number }}</span>
            </div>
            <div class="table-responsive">
                <table class="table table-new">
                    <thead>
                        <tr>
                            <th scope="col">المنتج</th>
                            <th scope="col">التقطيع</th>
                            <th scope="col">العدد بالكيلو</th>
                            <th scope="col">اجمالي المنتج بالريال</th>
                            <th scope="col">اجمالي التقطيع بالريال</th>
                            <th scope="col">الاجمالي بالريال</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data->Order_options as $item)
                            <tr>
                                <td scope="col">{{ $item->Service->title_ar }}</td>
                                <td scope="col">{{ $item->Cut->title_ar }}</td>
                                <td scope="col">{{ $item->amount }}</td>
                                <td scope="col">{{ $item->price }}</td>
                                <td scope="col">{{ $item->cut_price }}</td>
                                <td scope="col">{{ $item->price + $item->cut_price }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="panel">
        <div class="del ">
            <div class="container">
                <span class="blue">خدمة التوصيل</span>
                <span>{{ $data->delivery }}</span>
            </div>
        </div>
        <div class="del ">
            <div class="container">
                <span class="blue">القيمة المضافة</span>
                <span>{{ $data->value_added }}</span>
            </div>
        </div>
        <div class="del ">
            <div class="container">
                <span class="blue">الأجمالى</span>
                <span>{{ $data->total_before_promo }}</span>
            </div>
        </div>
        @if($data->total_before_promo != $data->total_after_promo)
            <div class="del ">
                <div class="container">
                    <span class="blue">الأجمالى بعد الخصم</span>
                    <span>{{ $data->total_after_promo }}</span>
                </div>
            </div>
        @endif
    </div>
    <div class="order-dtail panel">
        <div class="container">
            <h3 class="blue">ملاحظات</h3>
            <p>
                {{ is_null($data->other_data) ? 'لا يوجد ملاحظات' : $data->other_data }}
            </p>
        </div>
    </div>
    <div class="order-dtail panel">
        <div class="container">
            <h3 class="blue">مكان التوصيل</h3>
            <ul>
                <li>
                    <span> المدينة</span>
                    <span> {{ $data->City->title_ar }}</span>
                </li>
                <li>
                    <span> الحي</span>
                    <span> {{ $data->Neighborhood->title_ar }}</span>
                </li>
            </ul>
            <!-- Button trigger modal -->
            {{--  <button type="button" class="btn blue map" data-toggle="modal" data-target="#exampleModal">
                <i class="fa fa-map-marker"></i>
            </button>  --}}
        </div>
    </div>
    <div class="order-dtail panel">
        <div class="container">
            <h3 class="blue">موعد التسليم</h3>
            <ul>
                <li>
                    <span> {{ $data->Day->title_ar }}</span>
                    <span> {{ date('Y-m-d' , strtotime($data->date)) }}</span>
                </li>
            </ul>
        </div>
    </div>
    <div class="order-dtail panel">
        <div class="container">
            <h3 class="blue">طريقة الدفع</h3>
            <p>
                {{ $data->payment_method == 0 ? 'الدفع عند الاستلام' : 'الدفع اون لاين' }}
            </p>
        </div>
    </div>
    <!-- Modal -->
    {{--  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="modal-body">
                    <div id="map" style="500px"></div>
                </div>
            </div>
        </div>
    </div>  --}}

@endsection

@section('script')

@endsection