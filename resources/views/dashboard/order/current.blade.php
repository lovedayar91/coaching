@section('styles')

    <style>

        @media (max-width: 475.98px) {
            .boxes .col-sm-6 div#datatable_filter {
                float: none;
                text-align: center;
            }

            .boxes .col-sm-6 {
                float:  none;
                text-align: center;
                display:  inline-block;
                width:  10px;
            }
        }

        @media (min-width: 476px) and (max-width: 767.98px) {
            .boxes .col-sm-6 div#datatable_filter {
                float: right;
            }

            .boxes .col-sm-6 {
                float:  right;
                display:  inline-block;
                width:  50%;
            }
        }

    </style>
@endsection

@extends('dashboard.index')
@section('title')
    الحجوزات الحالية
@endsection
@section('content')

    <div class="row">

        <div class="col-sm-12">
            <div class="card-box table-responsive boxes">

                <table id="datatable" class="table table-bordered table-responsives">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>أسم المدرب</th>
                        <th>أسم العميل</th>
                        <th>القسم</th>
                        <th>التاريخ</th>
                        <th>الوقت</th>
                    </tr>
                    </thead>
                    <tbody class="text-center">
                    @foreach($data as $i=>$item)
                        <tr>
                            <td>{{++$i}}</td>
                            <td>{{$item->Coach->name_ar}}</td>
                            <td style="{{is_null($item->User)||is_null($item->User->first_name)?'color:white;background:#f44336':''}}">{{is_null($item->User)||is_null($item->User->first_name)?'تم حذف العميل':$item->User->first_name . ' ' . $item->User->last_name}}</td>
                            <td style="{{is_null($item->Section)||is_null($item->Section->title_ar)?'color:white;background:#f44336':''}}">{{is_null($item->Section)||is_null($item->Section->title_ar)?'تم حذف القسم':$item->Section->title_ar}}</td>
                            <td>{{date('Y-m-d',strtotime($item->date))}}</td>
                            <td>{{$item->Time->title_ar}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div><!-- end col -->

    </div>


   <div id="delete" class="modal-demo" style="position:relative; right: 32%">
        <button type="button" class="close" onclick="Custombox.close();" style="opacity: 1">
            <span>&times</span><span class="sr-only" style="color: #f7f7f7">Close</span>
        </button>
        <h4 class="custom-modal-title">حذف طلب</h4>
        <div class="custombox-modal-container" style=" height: 175px;">
            <div class="row">
                <div class="col-sm-12">
                    <h3 style="margin-top: 35px">
                        هل تريد مواصلة عملية الحذف ؟
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <form action="{{route('deleteorder')}}" method="post">
                        {{csrf_field()}}
                        <input type="hidden" name="delete_id" value="">
                        <button style="margin-top: 35px" type="submit" class="btn btn-danger btn-rounded w-md waves-effect waves-light m-b-5 send-delete-all"  style="margin-top: 19px">حـذف</button>
                    </form>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div>

    <div id="deleteAll" class="modal-demo" style="position:relative; right: 32%">
        <button type="button" id="close-deleteAll" class="close" onclick="Custombox.close();" style="opacity: 1">
            <span>&times</span><span class="sr-only" style="color: #f7f7f7">Close</span>
        </button>
        <h4 class="custom-modal-title">حذف المحدد</h4>
        <div class="custombox-modal-container" style=" height: 160px;">
            <div class="row">
                <div class="col-sm-12">
                    <h3 style="margin-top: 35px">
                        هل تريد مواصلة عملية الحذف ؟
                    </h3>
                    {{--<span style="color: red">عند حذف قسم يتم حذف الخدمات التابعة له !!</span>--}}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <button style="margin-top: 35px" type="submit" class="btn btn-danger btn-rounded w-md waves-effect waves-light m-b-5 send-delete-all" style="margin-top: 19px">حـذف</button>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div>

@endsection

@section('script')

    <script>

        $(document).ready(function(){
            var table = $('.table').DataTable();
            var value = "{{ $search }}";
            table.search(value).draw();
        });

        $('.edit').on('click',function(){
            //get valus
            let id          = $(this).data('id');
            let title       = $(this).data('title');
            // let country_id  = $(this).data('country_id');

            $("input[name='id']").val(id);
            $("#title").val(title);
            // $("#country_id").val(country_id);
        });
        
        $('.show').on('click',function(){
            
        });

        $('.delete').on('click',function(){

            let id         = $(this).data('id');

            $("input[name='delete_id']").val(id);

        });

        $("#checkedAll").change(function(){
            if(this.checked){
                $(".checkSingle").each(function(){
                    this.checked=true;
                })
            }else{
                $(".checkSingle").each(function(){
                    this.checked=false;
                })
            }
        });

        $(".checkSingle").click(function () {
            if ($(this).is(":checked")){
                var isAllChecked = 0;
                $(".checkSingle").each(function(){
                    if(!this.checked)
                        isAllChecked = 1;
                })
                if(isAllChecked == 0){ $("#checkedAll").prop("checked", true); }
            }else {
                $("#checkedAll").prop("checked", false);
            }
        });
    </script>

@endsection