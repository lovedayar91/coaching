@extends('dashboard.index')
@section('title')
    الرئيسية
@endsection
@section('content')
    @php
        use Carbon\Carbon;    
    @endphp
    <section>
        <!-- new div-->
        <div class="new-title">
            <h5>
                احصائيات عامة
            </h5>
        </div>
        <div class="row statistic">
            <div class="col-lg-3 col-md-4 col-sm-6 text-center">
                <div class="block" id="counter">
                    <a  href="{{ route('admins') }}" class=""> عدد المشرفيين  </a>
                    <div class="number counter-value" data-count="{{ App\User::where('role','>',0)->count() }}">0</div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 text-center">
                <div class="block" id="counter">
                    <a  href="{{ route('users') }}" class=""> عدد المستخدمين  </a>
                    <div class="number counter-value" data-count="{{ App\User::where('role',0)->where('provider','0')->count() }}">0</div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 text-center">
                <div class="block" id="counter">
                    <a  href="{{ route('coachs') }}" class=""> عدد المدربات  </a>
                    <div class="number counter-value" data-count="{{ App\Models\Coach::count() }}">0</div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 text-center">
                <div class="block" id="counter">
                    <a  href="{{ route('sections') }}" class=""> عدد الأقسام  </a>
                    <div class="number counter-value" data-count="{{ App\Models\Section::count() }}">0</div>
                </div>
            </div>

            <div class="col-lg-3 col-md-4 col-sm-6 text-center">
                <div class="block" id="counter">
                    <a  href="{{ route('packages') }}" class=""> عدد الباقات  </a>
                    <div class="number counter-value" data-count="{{ App\Models\Package::count() }}">0</div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 text-center">
                <div class="block" id="counter">
                    <a  href="{{ route('countries') }}" class=""> عدد الدول  </a>
                    <div class="number counter-value" data-count="{{ App\Models\Country::count() }}">0</div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 text-center">
                <div class="block" id="counter">
                    <a  href="{{ route('cities') }}" class=""> عدد المدن  </a>
                    <div class="number counter-value" data-count="{{ App\Models\City::count() }}">0</div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 text-center">
                <div class="block" id="counter">
                    <a  href="{{ route('questions') }}" class=""> عدد الاسئلة الشائعة  </a>
                    <div class="number counter-value" data-count="{{ App\Models\Question::count() }}">0</div>
                </div>
            </div>

            <div class="col-lg-3 col-md-4 col-sm-6 text-center">
                <div class="block" id="counter">
                    <a  href="{{ route('our_customers') }}" class=""> عدد رأي عملائنا  </a>
                    <div class="number counter-value" data-count="{{ App\Models\Our_customer::count() }}">0</div>
                </div>
            </div>
            
            <div class="col-lg-3 col-md-4 col-sm-6 text-center">
                <div class="block" id="counter">
                    <a  href="{{ route('register_lows') }}" class=""> عدد قوانين التسجيل  </a>
                    <div class="number counter-value" data-count="{{ App\Models\Register_law::count() }}">0</div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 text-center">
                <div class="block" id="counter">
                    <a  href="{{ route('links') }}" class=""> عدد روابط تهمك  </a>
                    <div class="number counter-value" data-count="{{ App\Models\Link::count() }}">0</div>
                </div>
            </div>
        </div>
        <!-- end div-->

        <!-- new div-->
        @foreach (App\Models\Coach::get() as $coach)
            <div class="new-title">
                <h5>
                    احصائيات المدربة / {{$coach->name_ar}}
                </h5>
            </div>
            <div class="row statistic">
                <div class="col-lg-3 col-md-4 col-sm-6 text-center">
                    <div class="block" id="counter">
                        <a  href="{{ url('admin/today-order?search=' . $coach->name_ar) }}" class=""> عدد الجلسات اليوم  </a>
                        <div class="number counter-value" data-count="{{ App\Models\Order::where('coach_id',$coach->id)->whereDate('full_date', Carbon::now())->count() }}">0</div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 text-center">
                    <div class="block" id="counter">
                        <a  href="{{ url('admin/tomorrow-order?search=' . $coach->name_ar) }}" class=""> عدد الجلسات غدا  </a>
                        <div class="number counter-value" data-count="{{ App\Models\Order::where('coach_id',$coach->id)->whereDate('full_date', Carbon::now()->addDay())->count() }}">0</div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 text-center">
                    <div class="block" id="counter">
                        <a  href="{{ url('admin/current-order?search=' . $coach->name_ar) }}" class=""> عدد الجلسات الحالية  </a>
                        <div class="number counter-value" data-count="{{ App\Models\Order::where('coach_id',$coach->id)->whereDate('full_date', '>=', Carbon::now())->count() }}">0</div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 text-center">
                    <div class="block" id="counter">
                        <a  href="{{ url('admin/finish-order?search=' . $coach->name_ar) }}" class=""> عدد الجلسات المنتهية  </a>
                        <div class="number counter-value" data-count="{{ App\Models\Order::where('coach_id',$coach->id)->whereDate('full_date', '<', Carbon::now())->count() }}">0</div>
                    </div>
                </div>
            </div>
        @endforeach
        

    </section>
    <!-- End Statistics-->
@endsection
