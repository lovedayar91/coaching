@section('styles')

    <style>

        @media (max-width: 475.98px) {
            .boxes .col-sm-6 div#datatable_filter {
                float: none;
                text-align: center;
            }

            .boxes .col-sm-6 {
                float:  none;
                text-align: center;
                display:  inline-block;
                width:  10px;
            }
        }

        @media (min-width: 476px) and (max-width: 767.98px) {
            .boxes .col-sm-6 div#datatable_filter {
                float: right;
            }

            .boxes .col-sm-6 {
                float:  right;
                display:  inline-block;
                width:  50%;
            }
        }

        .inputs{
            margin-top: 10px;
            margin-bottom: 2px;
        }

    </style>
@endsection

@extends('dashboard.index')
@section('title')
    مركز الاشعارات
@endsection
@section('content')

    <div class="row">

        <div class="col-sm-12">
            <div class="card-box table-responsive boxes">

                <form action="{{route('sendnotifys')}}" id="notifys_form" method="post">
                    {{csrf_field()}}
                    <label for="" style="width: 100%">اختار التصنيف : </label>
                    <input type="radio" name="type" id="user" value="user" checked onchange="showDiv('users')">
                    <label for="user">الاعضاء</label>
                    <input type="radio" name="type" id="city" value="city" onchange="showDiv('citys')" style="margin-right: 20px">
                    <label for="city">المدن</label>
                    <div class="type-radio" id="users">
                        <label class="inputs" for="">اختار المرسل اليه</label>
                        <select name="to" id="to" class="form-control inputs">
                            <optgroup label="عام">
                                <option value="all">الكل</option>
                                <option value="doctors">كل الأطباء</option>
                                <option value="users">كل الاعضاء</option>
                            </optgroup>
                            <optgroup label="الأطباء">
                                @foreach($doctors as $item)
                                    <option value="{{$item->id}}">{{$item->name_ar}}</option>
                                @endforeach
                            </optgroup>
                            <optgroup label="الأعضاء">
                                @foreach($users as $item)
                                    <option value="{{$item->id}}">{{$item->name_ar}}</option>
                                @endforeach
                            </optgroup>
                        </select>
                    </div>
                    <div class="type-radio" id="citys" style="display: none">
                        <label class="inputs" for="">اختار المدينة</label>
                        <select name="city_id" id="city_id" class="form-control inputs">
                            <optgroup label="عام">
                                <option value="all">الكل</option>
                            </optgroup>
                            <optgroup label="المدن">
                                @foreach(App\Models\City::orderBy('title_ar','asc')->get() as $item)
                                    <option value="{{$item->id}}">{{$item->title_ar}}</option>
                                @endforeach
                            </optgroup>
                        </select>
                    </div>

                    <label class="inputs" for="">اكتب رسالتك</label>
                    <textarea name="desc" id="desc" placeholder="اكتب رسالتك" cols="30" rows="10" class="form-control inputs"></textarea>

                    <button type="submit" class="form-control inputs btn-success">ارسال</button>
                </form>
            </div>
        </div><!-- end col -->

    </div>

@endsection

@section('script')

    <script>
        $("#to").select2();
        $("#city_id").select2();
        function showDiv(id){
            $('.type-radio').hide();
            $('#'+id).show();
        }
    </script>

@endsection