@section('styles')

    <style>

        @media (max-width: 475.98px) {
            .boxes .col-sm-6 div#datatable_filter {
                float: none;
                text-align: center;
            }

            .boxes .col-sm-6 {
                float:  none;
                text-align: center;
                display:  inline-block;
                width:  10px;
            }
        }

        @media (min-width: 476px) and (max-width: 767.98px) {
            .boxes .col-sm-6 div#datatable_filter {
                float: right;
            }

            .boxes .col-sm-6 {
                float:  right;
                display:  inline-block;
                width:  50%;
            }
        }

    </style>
@endsection

@extends('dashboard.index')
@section('title')
    اشتراكات الباقات
@endsection
@section('content')

    <div class="row">

        <div class="col-sm-12">
            <div class="card-box table-responsive boxes">

                <table id="datatable" class="table table-bordered table-responsives">
                    <thead>
                    <tr>
                        <th>
                            #
                        </th>
                        <th> اسم العضو</th>
                        <th> عنوان الباقة</th>
                        <th> عنوان القسم</th>
                        <th> سعر الباقة</th>
                        <th> عدد الجلسات المتبقية</th>
                        <th>تاريخ التسجيل</th>
                    </tr>
                    </thead>
                    <tbody class="text-center">
                    @foreach($data as $i=>$item)
                        <tr>
                            <td>
                                {{++$i}}
                            </td>

                            <td>{{ is_null($item->User)    ? 'تم حذف العضو' : $item->User->first_name . ' ' . $item->User->last_name }}</td>
                            <td>{{ is_null($item->Package) ? 'تم حذف الباقة' : $item->Package->title_ar }}</td>
                            <td>{{ is_null($item->Package) && !is_null($item->Package->Section) ? 'تم حذف الباقة' : $item->Package->Section->title_ar}} </td>
                            <td>{{ is_null($item->Package) ? 'تم حذف الباقة' : $item->Package->price }} ريال </td>
                            <td>{{$item->session_count == 0 ? 'تم الانتهاء من الجلسات' : $item->session_count}}</td>
                            <td>{{$item->created_at->diffForHumans()}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div><!-- end col -->

    </div>

@endsection