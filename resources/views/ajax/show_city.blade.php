@php 
    $lang  = Session::has('language') ? Session('language') : 'ar';
    $title = 'title_' . $lang;
@endphp

@foreach ($data as $item)
    <option value="{{$item->id}}" @if(Auth::check() && $item->id == Auth::user()->city_id) selected @endif>
        {{$item->$title}}
    </option>
@endforeach