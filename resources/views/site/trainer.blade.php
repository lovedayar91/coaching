@extends('site.master')
@php 
    $lang  = App::getLocale();
    $name  = 'name_' . $lang;
    $desc  = 'desc_' . $lang;
@endphp
@section('title') {{$data->$name}} @endsection
@section('style')
@endsection

@section('content')
    <div class="wrapper">
        <!------------- banner ------------>
        <section class="banner single-page">
            <img src="{{site_path()}}/images/banner.png" alt="">
            <div class="banner-over">
            </div>
        </section>
        <!------------- end banner ------------>
        <!------------- about ------------->
        <section class="about dark-txt">
            <div class="container">
                <h2 class="section-title">{{$data->$name}}</h2>
                <!-- end slider -->
                @if ($data->Images->count() > 0)
                    <div id="owl-main" class="owl-carousel owl-theme">
                        @foreach ($data->Images as $item)
                            <div class="item">
                                <img src="{{$item->image}}" alt="slider">
                            </div>
                        @endforeach
                    </div>
                @endif
                <!-- end slider -->
                <div class="row">
                    <div class="col-12">
                        <h4 class="brown-txt">{{ trans('site.about') }} {{$data->$name}} </h4>
                        <p class="gray-txt">
                            {{$data->$desc}}
                        </p>
                    </div>
                </div>
                <hr>
                <h4 class="brown-txt">
                    {{-- يمكنك التواصل عبر الأيميل --}}
                    {{ trans('site.contact_via_email') }}
                </h4>
                <h4 class="text-center">
                    <a class="dark-txt" target="_blank" href="mailto:{{$data->email}}">
                        {{$data->email}}
                    </a>
                </h4>
            </div>
        </section>
        <!------------- end about ------------->
    </div>
@endsection

@section('script')
@endsection