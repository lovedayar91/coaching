@extends('site.master')
@section('title') {{ trans('site.login') }} @endsection
@section('style')
@endsection

@section('content')
    <div class="wrapper">
        <!------------- banner ------------>
        <section class="banner single-page">
            <img src="{{site_path()}}/images/banner.png" alt="">
            <div class="banner-over">
            </div>
        </section>
        <!------------- end banner ------------>
        <!------------- about ------------->
        <section class="about dark-txt">
            <div class="container">
                <h2 class="section-title">{{ trans('site.login') }}</h2>
                <form class="login-form" action="{{route('site_post_login')}}" method="POST" autocomplete="off">
                    @csrf
                    <div class="form-group dark-txt">
                        <input type="tel" class="form-control empty phone" name="phone" value="" required>
                        <label for="">
                            {{--  رقم الجوال  --}}
                            {{ trans('site.Mobile') }}
                        </label>
                    </div>
                    <div class="form-group dark-txt">
                        <input type="password" class="form-control empty" name="password" value="" required>
                        <label for="">
                            {{--  كلمة المرور  --}}
                            {{ trans('site.password') }}
                        </label>
                    </div>
                    <a  class="brown-txt text-center d-block" href="{{route('site_forget_password')}}">
                        {{--  هل نسيت كلمة المرور  --}}
                        {{ trans('site.Are_you_forget_your_password') }}
                    </a>
                    <button type="submit" class="btn btn-site brown-bg d-block">
                        {{--  دخول  --}}
                        {{ trans('site.login_now') }}
                    </button>
                    <p class="dark-txt text-center">
                        {{--  ليس لديك حساب ؟  --}}
                        {{ trans('site.Are_you_have_not_account') }}
                        <a href="{{route('site_register')}}" class="brown-txt">
                            {{--  تسجيل جديد  --}}
                            {{ trans('site.register') }}
                        </a>
                    </p>
                </form>
            </div>
        </section>
        <!------------- end about ------------->
    </div>
@endsection

@section('script')
@endsection