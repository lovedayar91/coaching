@extends('site.master')
@section('title') {{ trans('site.Register_Lows') }} @endsection
@section('style')
@endsection

@section('content')
    <div class="wrapper">
        <!------------- banner ------------>
        <section class="banner single-page">
            <img src="{{site_path()}}/images/banner.png" alt="">
            <div class="banner-over">
            </div>
        </section>
        <!------------- end banner ------------>
        @php 
            $lang  = App::getLocale();
            $title = 'title_' . $lang;
            $desc  = 'desc_' . $lang;
        @endphp
        <!------------- about ------------->
        <section class="about dark-txt">
            <div class="container">
                <h2 class="section-title">
                    {{--  قوانين التسجيل  --}}
                    {{ trans('site.Register_Lows') }}
                </h2>
                <div class="row">
                    <div class="col-12">
                        <div class="condation-item">
                            @foreach ($data as $item)
                                <h6 class="brown-txt"> {{$item->$title}}</h6>
                                <p class="m-b gray-txt">
                                   {{$item->$desc}}
                                </p>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!------------- end about ------------->
    </div>
@endsection

@section('script')
@endsection