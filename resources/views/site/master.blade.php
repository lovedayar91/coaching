<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="{{settings('description')}}">
    <meta name="keywords" content="{{settings('key_words')}}">
    <meta name="author" content="Abdelrahman">
    <meta name="robots" content="index/follow">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1,, shrink-to-fit=no, maximum-scale=1, user-scalable=no">
    <meta name="HandheldFriendly" content="true">
    <link rel="shortcut icon" type="image/png" href="{{site_path()}}/images/favicon.jpg" />
    <title>{{settings('site_name')}} | @yield('title')</title>

    <link rel="stylesheet" href="{{site_path()}}/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{site_path()}}/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{site_path()}}/css/owl.carousel.css">
    <link rel="stylesheet" href="{{site_path()}}/css/jquery-ui.css">
    <link rel="stylesheet" href="{{site_path()}}/css/style.css">
    @if (!App::isLocale('ar'))
        <link rel="stylesheet" href="{{site_path()}}/css/style_en.css">
    @endif
    <style>
        /* remove increase and decrease arrows from number input */
        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        .wrapper{
            min-height: 632px;
        }
    </style>
    @yield('style')
</head>

<body>
    @php 
        $lang  = App::getLocale();
        $name  = 'name_' . $lang;
        $title = 'title_' . $lang;
        $desc  = 'desc_' . $lang;
        $address = 'address_' . $lang;
    @endphp
    @include('site.msg')
    <div class="overlay"></div>
    <div id="preloader_6">
        <div class="preloader-contant">
            <img src="{{site_path()}}/images/logo-w.png" alt="">
        </div>
    </div>
    <!------------- header ------------>
    <header>
        <div class="top-header dark-bg">
            <div class="container">
                <div class="row">
                    @if(!Auth::check())
                        <div class="col-md-6 col-12">
                            <div class="login">
                                <a href="{{route('site_login')}}">
                                    {{--  دخول  --}}
                                    {{trans('site.login')}} /
                                </a>
                                <span>
                                    <a href="{{route('site_register')}}" style="color:#C49728"> 
                                        {{--  تسجيل جديد  --}}
                                        {{trans('site.signUp')}} 
                                    </a>
                                </span>
                            </div>
                        </div>
                    @else 
                        <div class="col-md-6 col-12">
                            <div class="lang-phone justify-content-start">
                                <div class="dropdown">
                                    <button class="dropdown-toggle" type="button" id="dropdownMenuButton"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {{--  اهلا وسهلا بك  --}}
                                        {{ trans('site.welcome') }}
                                        &nbsp;&nbsp;
                                        <span class="brown-txt">
                                            {{Auth::user()->first_name}} {{Auth::user()->last_name}}
                                        </span>
                                    </button>
                                    <div class="dropdown-menu user-drop" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="{{route('site_profile')}}">
                                            <span class="lang-name">
                                                {{--  حسابي  --}}
                                                {{ trans('site.Profile') }}
                                            </span>
                                        </a>
                                        <a class="dropdown-item" href="{{route('site_orders')}}">
                                            <span class="lang-name">
                                                {{--  حجزاتي  --}}
                                                {{ trans('site.My_Orders') }}
                                            </span>
                                        </a>
                                        <a class="dropdown-item" href="{{ route('site_logout') }}">
                                            <span class="lang-name">
                                                {{--  تسجيل الخروج  --}}
                                                {{ trans('site.Logout') }}
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="col-md-6 col-12">
                        <ul class="lang-phone">
                            <li>
                                <a href="tel:{{settings('phone')}}">
                                    <i class="fas fa-phone"></i>
                                    {{settings('phone')}}
                                </a>
                            </li>
                            <li>
                                @if (App::isLocale('ar'))
                                    <div class="dropdown">
                                        <button class="dropdown-toggle" type="button" id="dropdownMenuButton"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span>اللغة :</span>
                                            <img class="lang-img" src="{{site_path()}}/images/ar.png" alt="">
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="{{url('change-lang/en')}}">
                                                <span class="lang-name">English</span>
                                                <img class="lang-img" src="{{site_path()}}/images/en.png">
                                            </a>
                                        </div>
                                    </div>
                                @else
                                    <div class="dropdown">
                                        <button class="dropdown-toggle" type="button" id="dropdownMenuButton"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span>Language : </span>
                                            <img class="lang-img" src="{{site_path()}}/images/en.png" alt="">
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="{{url('change-lang/ar')}}">
                                                <span class="lang-name">Arabic</span>
                                                <img class="lang-img" src="{{site_path()}}/images/ar.png">
                                            </a>
                                        </div>
                                    </div>
                                @endif
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <nav>
            <div class="container">
                <div class="row">
                    <div class="logo">
                        <a href="{{url('/')}}">
                            <img src="{{settings('site_logo')}}" alt="">
                        </a>
                    </div>
                    <div class="mob-collaps">
                        <span class=""></span>
                        <span class=""></span>
                        <span class=""></span>
                    </div>
                    <ul class="d-flex site-nav">
                        <li>
                            <a href="{{url('/')}}" @if(Route::currentRouteName() == 'url') class="active" @endif @if(Route::currentRouteName() == 'site_index') class="active" @endif>
                                {{--  الرئيسية  --}}
                                {{ trans('site.home') }}
                            </a>
                        </li>
                        <li>
                            <a href="{{route('site_about_train')}}" @if(Route::currentRouteName() == 'site_about_train') class="active" @endif>
                                {{--  عن التدريب  --}}
                                {{trans('site.aboutTraining')}}
                            </a>
                        </li>
                        <li>
                            <a href="{{route('site_about_us')}}" @if(Route::currentRouteName() == 'site_about_us') class="active" @endif>
                                {{--  من نحن  --}}
                                {{trans('site.whoUs')}}
                            </a>
                        </li>
                        <li>
                            <div class="dropdown-click">
                                <a class="default" href="">
                                    {{--  العروض  --}}
                                    {{trans('site.offer')}}
                                    <i class="left-icon fas fa-chevron-down"></i>
                                </a>
                                <div class="dropdown-content">
                                    <ul>
                                        @foreach (getSection() as $item)
                                            <li>
                                                <div class="sub-dropdown">
                                                    <a class="default" href="">
                                                        <span>{{$item->$title}}</span>
                                                        <i class="fas fa-angle-left"></i>
                                                    </a>
                                                    <div class="dropdown">
                                                        <ul>
                                                            @foreach ($item->Packages as $package)
                                                                <li><a href="{{route('site_packages_checkout_info' , $package->id)}}">{{$package->$title}}</a></li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li>
                            <a href="{{url('/')}}#client">
                                {{--  أراء عملاءنا  --}}
                                {{ trans('site.Opinion_of_our_customers') }}
                            </a>
                        </li>
                        <li>
                            <a href="{{route('site_contact_us')}}" @if(Route::currentRouteName() == 'site_contact_us') class="active" @endif>
                                {{--  تواصل معنا  --}}
                                {{ trans('site.Contact_Us') }}
                            </a>
                        </li>
                        <li class="important-link">
                            <a href="{{route('site_links')}}" @if(Route::currentRouteName() == 'site_links') class="active" @endif>
                                {{--  روابط تهمك  --}}
                                {{ trans('site.Important_Links') }}
                            </a>
                        </li>
                        <li class="custom-link">
                            <a href="{{route('site_conditions')}}" @if(Route::currentRouteName() == 'site_conditions') class="active" @endif>
                                {{--  قوانين التسجيل  --}}
                                {{ trans('site.Register_Lows') }}
                            </a>
                        </li>
                        <li class="custom-link">
                            <a href="{{route('site_questions')}}" @if(Route::currentRouteName() == 'site_questions') class="active" @endif>
                                {{--  الأسئلة الشائعة  --}}
                                {{ trans('site.Common_Questions') }}
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <!------------- end header ------------>
    @yield('content')
    <!------------- footer ------------>
    <footer class="dark-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-12 d-flex align-items-center">
                    <img src="{{site_path()}}/images/logo-w.png" class="img-fluid" alt="">
                </div>
                <div class="col-md-6 col-12">
                    <div class="footer-widget">
                        <ul class="custom-ul">
                            <li class="footer-title">
                                {{--  روابط تهمك  --}}
                                {{ trans('site.Important_Links') }}
                            </li>
                            <li class="footer-title">{{settings('site_name')}}</li>
                            @foreach (getAllLinks() as $item)
                                <li>
                                    <a href="{{$item->url}}">
                                        {{$item->$title}}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-12">
                    <div class="footer-widget ">
                        <h4 class="footer-title">
                            {{--  أتصل بنا  --}}
                            {{ trans('site.Call_Us') }}
                        </h4>
                        <ul>
                            <li>
                                {{--  العنوان  --}}
                                {{ trans('site.Address') }}
                                 :
                                <span>{{settings($address)}}</span>
                            </li>
                            <li>
                                {{--  رقم الجوال   --}}
                                {{ trans('site.Mobile') }}
                                 :
                                <span> {{settings('mobile')}}</span>
                            </li>
                            <li>
                                {{--  البريد الألكتروني  --}}
                                {{ trans('site.Email') }}
                                 :
                                <span>{{settings('email')}}</span>
                            </li>
                            <li class="contacts">
                                <a target="_blanck" href="https://api.whatsapp.com/send?phone={{settings('whatsapp')}}">
                                    <i class="fab fa-whatsapp"></i>
                                </a>
                                <a target="_blanck" href="https://www.snapchat.com/add/{{settings('snapchat')}}">
                                    <i class="fab fa-snapchat-ghost"></i>
                                </a>
                                <a target="_blanck" href="{{social('instagram')}}">
                                    <i class="fab fa-instagram"></i>
                                </a>
                                <a target="_blanck" href="{{social('twitter')}}">
                                    <i class="fab fa-twitter"></i>
                                </a>
                                <a target="_blanck" href="{{social('facebook')}}">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div class="footer-bottom brown-bg">
        <div class="container">
            <div class="row justify-content-between">
                <p>
                    {{--  جميع الحقوق محفوظة لـ  --}}
                    {{ trans('site.All_rights_are_save_to') }}
                    <a href="{{url('/')}}">
                        {{settings('site_name')}}
                    </a>
                </p>
                <p>
                    {{--  تصميم وتنفيذ مؤسسة  --}}
                    {{ trans('site.Design_and_implementation_of_an_institution') }}
                    <a href="https://www.aait.sa">
                        {{--  أوامر الشبكة  --}}
                        {{ trans('site.Aait') }}
                    </a>
                </p>
            </div>
        </div>
    </div>
    <!------------- end footer ------------>
    <!------------- fixedfooter ------------>
    <div class="fixed-footer dark-bg">
        <ul>
            <li>
                <a href="{{url('/')}}" class="active">
                    <i class="fas fa-home"></i>
                    <span>
                        {{--  الرئيسية  --}}
                        {{trans('site.home')}}
                    </span>
                </a>
            </li>
            <li>
                <a href="{{route('site_contact_us')}}">
                    <i class="fas fa-phone"></i>
                    <span>
                        {{--  تواصل معنا  --}}
                        {{ trans('site.Contact_Us') }}
                    </span>
                </a>
            </li>
            <li>
                <a href="{{route('site_about_us')}}">
                    <i class="fas fa-info-circle"></i>
                    <span>
                        {{--  من نحن  --}}
                        {{trans('site.whoUs')}}
                    </span>
                </a>
            </li>
            <li>
                @if(!Auth::check())
                    <a href="{{ route('site_login') }}">
                        <i class="fas fa-sign-in-alt"></i>
                        <span>
                            {{--  تسجيل دخول  --}}
                            {{trans('site.login')}}
                        </span>
                    </a>
                @else
                    <a href="{{route('site_profile')}}">
                        <i class="fas fa-user"></i>
                        <span>
                            {{--  حسابي  --}}
                            {{ trans('site.Profile') }}
                        </span>
                    </a>
                @endif
            </li>
        </ul>
    </div>
    <!------------- end fixedfooter ------------>

    <script src="{{site_path()}}/js/jquery-3.2.1.min.js"></script>
    <script src="{{site_path()}}/js/popper.min.js"></script>
    <script src="{{site_path()}}/js/bootstrap.min.js"></script>
    <script src="{{site_path()}}/js/jquery-ui.js"></script>
    <script src="{{site_path()}}/js/owl.carousel.min.js"></script>
    <script src="{{site_path()}}/js/main.js"></script>
    <script src="{{appPath()}}/public/notify.js"></script>
    <script>
        $(document).ready(function() {
            // notify
            var type = $('#alertType').val();
            if (type != '0') {
                var theme = $('#alertTheme').val();
                var message = $('#alertMessage').val();
                $.notify(message, theme);
            }

            // allow number only in inputs has class phone
            $(".phone").keydown(function(e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });

            // empty inputs has class empty
            $(".empty").val('');
        });
    </script>
    @yield('script')
</body>

</html>