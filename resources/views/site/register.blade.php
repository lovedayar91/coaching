@extends('site.master')
@section('title') {{ trans('site.register') }} @endsection
@section('style')
@endsection

@section('content')
    <div class="wrapper">
        <!------------- banner ------------>
        <section class="banner single-page">
            <img src="{{site_path()}}/images/banner.png" alt="">
            <div class="banner-over">
            </div>
        </section>
        <!------------- end banner ------------>
        @php 
            $lang  = App::getLocale();
            $title = 'title_' . $lang;
        @endphp
        <!------------- about ------------->
        <section class="about dark-txt">
            <div class="container">
                <h2 class="section-title">
                    {{--  تسجيل حساب جديد  --}}
                    {{ trans('site.register') }}
                </h2>
                <form class="login-form" action="{{route('site_post_register')}}" method="POST">
                    @csrf
                    <div class="form-group dark-txt">
                        <input type="text" class="form-control empty" name="first_name" value="{{old('first_name')}}" required>
                        <label for="first_name">
                            {{--  الأسم الأول  --}}
                            {{ trans('site.First_name') }}
                        </label>
                    </div>
                    <div class="form-group dark-txt">
                        <input type="text" class="form-control empty" name="last_name" value="{{old('last_name')}}" required>
                        <label for="last_name">
                            {{--  أسم العائلة  --}}
                            {{ trans('site.Last_name') }}
                        </label>
                    </div>
                    <div class="form-group dark-txt">
                        <input type="tel" class="form-control empty phone" name="phone" value="{{old('phone')}}" required>
                        <label for="phone">
                            {{--  رقم الجوال  --}}
                            {{ trans('site.Mobile') }}
                        </label>
                    </div>
                    <div class="form-group dark-txt">
                        <input type="email" class="form-control empty" name="email" value="{{old('email')}}" required>
                        <label for="email">
                            {{--  البريد الألكتروني  --}}
                            {{ trans('site.Email') }}
                        </label>
                    </div>
                    <div class="form-group dark-txt">
                        <input type="text"  class="form-control datepicker1" name="birthdate" value="{{old('birthdate')}}" required>
                        <label for="birthdate">
                            {{--  تاريخ الميلاد  --}}
                            {{ trans('site.Birthdate') }}
                        </label>
                        <i class="far fa-calendar-alt brown-txt"></i>
                    </div>
                    <div class="form-group dark-txt">
                        <select name="gender" id="" class="form-control" required>
                            <option value="male">
                                {{--  ذكر  --}}
                                {{ trans('site.Male') }}
                            </option>
                            <option value="female">
                                {{--  انثى  --}}
                                {{ trans('site.Female') }}
                            </option>
                        </select>
                        <label for="">
                            {{--  الجنس  --}}
                            {{ trans('site.gender') }}
                        </label>
                        <i class="fas fa-angle-down brown-txt"></i>
                    </div>
                    <div class="form-group dark-txt">
                        <select name="specialist_id" id="" class="form-control" required>
                            @foreach (getSpecialist() as $item)
                                <option value="{{$item->id}}">
                                    {{$item->$title}}
                                </option>
                            @endforeach
                        </select>
                        <label for="">
                            {{--  الحالة الأجتماعية  --}}
                            {{ trans('site.Social_status') }}
                        </label>
                        <i class="fas fa-angle-down brown-txt"></i>
                    </div>
                    <div class="form-group dark-txt">
                        <select name="country_id" id="country_id" class="form-control" onchange="showCity()" required>
                            @foreach (getCountry() as $item)
                                <option value="{{$item->id}}" @if($item->id == 187) selected @endif>{{$item->$title}}</option>
                            @endforeach
                        </select>
                        <label for="">
                            {{--  الدولة  --}}
                            {{ trans('site.country') }}
                        </label>
                        <i class="fas fa-angle-down brown-txt"></i>
                    </div>
                    <div class="form-group dark-txt">
                        <select name="city_id" id="city_id" class="form-control" required>
                            @foreach (getSoudiCity() as $item)
                                <option value="{{$item->id}}">{{$item->$title}}</option>
                            @endforeach
                        </select>
                        <label for="">
                            {{--  المدينة  --}}
                            {{ trans('site.city') }}
                        </label>
                        <i class="fas fa-angle-down brown-txt"></i>
                    </div>
                    <div class="form-group dark-txt">
                        <input type="text" class="form-control" name="job" value="{{old('job')}}" required>
                        <label for="">
                            {{--  الوظيفة  --}}
                            {{ trans('site.Job') }}
                        </label>
                    </div>
                    <div class="form-group dark-txt">
                        <input type="password" class="form-control" name="password" required>
                        <label for="">
                            {{--  كلمة المرور  --}}
                            {{ trans('site.password') }}
                        </label>
                    </div>
                    <div class="form-group dark-txt">
                        <input type="password" class="form-control" name="password_confirmation" required>
                        <label for="">
                            {{--  تأكيد كلمة المرور  --}}
                            {{ trans('site.confirmPassword') }}
                        </label>
                    </div>
                    <div class="form-group border-none">
                        <div class="custom-check">
                            <span>
                                {{--  أقر أن المعلومات عني هي صحيحة  --}}
                                {{ trans('site.correct_information') }}
                            </span>
                            <input type="checkbox" checked="checked" name="agree_info" disabled>
                            <span class="checkmark"></span>
                        </div>
                    </div>
                    <div class="form-group border-none">
                        <div class="custom-check">
                            <span>
                                <a href="{{route('site_conditions')}}" target="_blank" class="dark-txt">
                                    {{--  اقر انني قرأت القوانين ونظام التسجيل وموافق عليها  --}}
                                    {{ trans('site.read_lows') }}
                                </a>
                            </span>
                            <input type="checkbox" name="read_conditions" id="read_conditions" onchange="checkAgree()">
                            <span class="checkmark"></span>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-site brown-bg d-block" id="register_submit" disabled="">
                        {{--  تسجيل  --}}
                        {{ trans('site.register_now') }}
                    </button>
                    <p class="dark-txt text-center">
                        {{--  هل لديك حساب ؟  --}}
                        {{ trans('site.Are_you_have_account') }}
                        <a href="{{route('site_login')}}" class="brown-txt">
                            {{--  تسجيل الدخول  --}}
                            {{ trans('site.login') }}
                        </a>
                    </p>
                </form>
            </div>
        </section>
        <!------------- end about ------------->
    </div>
@endsection

@section('script')
    <script>
        function showCity() {
            let country_id = $('#country_id').val();
            $.ajax({
                type: "POST",
                url: "{{route('showCity')}}",
                data: {country_id: country_id, _token: '{{csrf_token()}}'},
                success: function( msg ) {
                    $('#city_id').html(msg);
                }
            });
        }

        function checkAgree() {
            if($('#read_conditions').is(':checked')){
                $('#register_submit').removeAttr('disabled');
            }
            else{
                $('#register_submit').attr('disabled','');
            }
        }
    </script>
@endsection