@extends('site.master')
@section('title') {{ trans('site.changePassword') }} @endsection
@section('style')
@endsection

@section('content')
    <div class="wrapper">
        <!------------- banner ------------>
        <section class="banner single-page">
            <img src="{{site_path()}}/images/banner.png" alt="">
            <div class="banner-over">
            </div>
        </section>
        <!------------- end banner ------------>
        @php 
            $lang  = App::getLocale();
            $title = 'title_' . $lang;
        @endphp
        <!------------- about ------------->
        <section class="about dark-txt">
            <div class="container">
                <h2 class="section-title">
                    {{--  تسجيل حساب جديد  --}}
                    {{ trans('site.changePassword') }}
                </h2>
                <form class="login-form" action="{{route('site_post_reset_password')}}" method="POST">
                    @csrf
                    <input type="hidden" name="id" value="{{$user->id}}">
                    <div class="form-group dark-txt">
                        <input type="tel" class="form-control empty phone" name="code" value="{{old('code')}}" required>
                        <label for="code">
                            {{--  الكود  --}}
                            {{ trans('site.code') }}
                        </label>
                    </div>
                    <div class="form-group dark-txt">
                        <input type="password" class="form-control" name="password" required>
                        <label for="">
                            {{--  كلمة المرور  --}}
                            {{ trans('site.password') }}
                        </label>
                    </div>
                    <div class="form-group dark-txt">
                        <input type="password" class="form-control" name="password_confirmation" required>
                        <label for="">
                            {{--  تأكيد كلمة المرور  --}}
                            {{ trans('site.confirmPassword') }}
                        </label>
                    </div>
                    <button type="submit" class="btn btn-site brown-bg d-block" id="register_submit">
                        {{--  حفظ  --}}
                        {{ trans('site.save') }}
                    </button>
                </form>
            </div>
        </section>
        <!------------- end about ------------->
    </div>
@endsection

@section('script')
    <script>
        function showCity() {
            let country_id = $('#country_id').val();
            $.ajax({
                type: "POST",
                url: "{{route('showCity')}}",
                data: {country_id: country_id, _token: '{{csrf_token()}}'},
                success: function( msg ) {
                    $('#city_id').html(msg);
                }
            });
        }

        function checkAgree() {
            if($('#read_conditions').is(':checked')){
                $('#register_submit').removeAttr('disabled');
            }
            else{
                $('#register_submit').attr('disabled','');
            }
        }
    </script>
@endsection