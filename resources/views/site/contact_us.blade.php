@extends('site.master')
@section('title') {{ trans('site.Common_Questions') }} @endsection
@section('style')
@endsection

@section('content')
    <div class="wrapper">
        <!------------- banner ------------>
        <section class="banner single-page">
            <img src="{{site_path()}}/images/banner.png" alt="">
            <div class="banner-over">
            </div>
        </section>
        <!------------- end banner ------------>
        @php 
            $lang  = App::getLocale();
            $title    = 'title_' . $lang;
            $address  = 'address_' . $lang;
            $data = [
                'name'  => $lang == 'en' ? 'Name'    : 'الأسم',
                'email' => $lang == 'en' ? 'Email'   : 'البريد الألكتروني',
                'phone' => $lang == 'en' ? 'Phone'   : 'رقم الهاتف',
                'msg'   => $lang == 'en' ? 'Message' : 'الرسالة',
            ];
        @endphp
        <!------------- about ------------->
        <section class="about dark-txt">
            <div class="container">
                <h2 class="section-title">
                    {{--  تواصل معنا  --}}
                    {{ trans('site.Contact_Us') }}
                </h2>
                <div class="row justify-content-center">
                    <div class="col-lg-4 col-md-6 col-12">
                        <div class="contact-item text-center">
                            <div class="icon brown-bg">
                                <i class="far fa-envelope"></i>
                            </div>
                            <div class="details ">
                                <h5>
                                    {{-- للمراسلة المباشرة --}}
                                    {{ trans('site.for_dirict_contact') }}
                                </h5>
                                <a href="mailto:{{settings('email')}}" class="dark-txt">
                                    {{settings('email')}}
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-12">
                        <div class="contact-item text-center">
                            <div class="icon brown-bg">
                                <i class="fas fa-home"></i>
                            </div>
                            <div class="details ">
                                <h5>
                                    {{--  العنوان  --}}
                                    {{ trans('site.Address') }}
                                </h5>
                                <p> {{settings($address)}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-12">
                        <div class="contact-item text-center">
                            <div class="icon brown-bg">
                                <i class="fas fa-phone"></i>
                            </div>
                            <div class="details ">
                                <h5>
                                    {{-- للمراسلة المباشرة --}}
                                    {{ trans('site.for_dirict_contact') }}
                                </h5>
                                <a href="tel:{{settings('mobile')}}" class="dark-txt">
                                    {{settings('mobile')}}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <form class="contact-form" action="{{route('site_post_contact_us')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="{{$data['name']}}" name="name" required>
                    </div>
                    <div class="form-group">
                        <input type="tel" class="form-control phone" placeholder="{{$data['phone']}}" name="phone" required>
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" placeholder="{{$data['email']}}" name="email" required>
                    </div>
                    <div class="form-group">
                        <textarea name="message" id="" class="form-control" cols="30" rows="7" placeholder="{{$data['msg']}}" required></textarea>
                    </div>
                    <button type="submit" class="btn btn-site brown-bg d-block">
                        {{-- ارسال --}}
                        {{ trans('site.send') }}
                    </button>
                </form>
            </div>
        </section>
        <!------------- end about ------------->
    </div>
@endsection

@section('script')
@endsection