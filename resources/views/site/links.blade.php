@extends('site.master')
@section('title') {{ trans('site.Important_Links') }} @endsection
@section('style')
@endsection

@section('content')
    <div class="wrapper">
        <!------------- banner ------------>
        <section class="banner single-page">
            <img src="{{site_path()}}/images/banner.png" alt="">
            <div class="banner-over">
            </div>
        </section>
        <!------------- end banner ------------>
        @php 
            $lang  = App::getLocale();
            $title = 'title_' . $lang;
        @endphp
        <!------------- about ------------->
        <section class="about dark-txt fqa">
            <div class="container">
                <h2 class="section-title">
                    {{--  روابط تهمك  --}}
                    {{ trans('site.Important_Links') }}
                </h2>
                <div class="row">
                    <div class="col-12">
                        <ul class="links-page">
                            @foreach ($data as $item)
                                <li>
                                    <a href="{{$item->url}}">
                                        {{$item->$title}}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!------------- end about ------------->
    </div>
@endsection

@section('script')
@endsection