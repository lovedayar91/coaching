@extends('site.master')
@section('title') {{ trans('site.update_password') }} @endsection
@section('style')
@endsection

@section('content')
    <div class="wrapper">
        <!------------- banner ------------>
        <section class="banner single-page">
            <img src="{{site_path()}}/images/banner.png" alt="">
            <div class="banner-over">
            </div>
        </section>
        <!------------- end banner ------------>
        @php 
            $lang  = App::getLocale();
            $title = 'title_' . $lang;
            $desc  = 'desc_' . $lang;
        @endphp
        <!------------- about ------------->
        <section class="about dark-txt">
            <div class="container">
                <h2 class="section-title">
                    {{-- تعديل بيانات --}}
                    {{ trans('site.update_password') }}
                </h2>
                <form class="login-form" action="{{route('site_post_update_password')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group dark-txt">
                        <input type="password" class="form-control" name="old_password">
                        <label for="">
                            {{ trans('site.old_password') }}
                        </label>
                    </div>
                    <div class="form-group dark-txt">
                        <input type="password" class="form-control" name="password">
                        <label for="">
                            {{ trans('site.password') }}
                        </label>
                    </div>
                    <div class="form-group dark-txt">
                        <input type="password" class="form-control" name="password_confirmation">
                        <label for="">
                            {{ trans('site.confirmPassword') }}
                        </label>
                    </div>
                    <button type="submit" class="btn btn-site brown-bg d-block">
                        تعديل
                    </button>
                </form>
            </div>
        </section>
        <!------------- end about ------------->
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function(){
            showCity();
        });

        function showCity() {
            let country_id = $('#country_id').val();
            $.ajax({
                type: "POST",
                url: "{{route('showCity')}}",
                data: {country_id: country_id, _token: '{{csrf_token()}}'},
                success: function( msg ) {
                    $('#city_id').html(msg);
                }
            });
        }

        function checkAgree() {
            if($('#read_conditions').is(':checked')){
                $('#register_submit').removeAttr('disabled');
            }
            else{
                $('#register_submit').attr('disabled','');
            }
        }
    </script>
@endsection