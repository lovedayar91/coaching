@extends('site.master')
@section('title') {{trans('site.check_out')}} @endsection
@section('style')
@endsection

@section('content')
    <div class="wrapper">
        <!------------- banner ------------>
        <section class="banner single-page">
            <img src="{{site_path()}}/images/banner.png" alt="">
            <div class="banner-over">
            </div>
        </section>
        <!------------- end banner ------------>
        @php 
            $lang  = App::getLocale();
            $title = 'title_' . $lang;
            $desc  = 'desc_' . $lang;
        @endphp
        <!------------- about ------------->
        <section class="about dark-txt">
            <div class="container">
                <h2 class="section-title">
                    {{-- االدفع --}}
                    {{ trans('site.pay') }}
                </h2>
                <img class="img-fluid pay-join" src="{{site_path()}}/images/pay-join.png" alt="">
                <div class="text-center">
                    <h6>
                        <form action="{{route('site_post_packages_checkout')}}" method="post">
                            @csrf
                            <input type="hidden" name="user_id" value="{{Auth::id()}}">
                            <input type="hidden" name="package_id" value="{{$data->id}}">
                            <input type="hidden" name="section_id" value="{{$data->Section->id}}">
                            <input type="hidden" name="session_count" value="{{$data->session_count}}">
                            <input type="text" name="price" value="{{$data->price}}" style="padding: 10px" readonly>
                            <button class="btn-success" style="padding: 10px">
                                {{-- أدفع الأن --}}
                                {{ trans('site.check_out_now') }}
                            </button>
                        </form>
                    </h6>
                </div>
            </div>
        </section>
        <!------------- end about ------------->
    </div>
@endsection

@section('script')
@endsection