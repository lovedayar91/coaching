@extends('site.master')
@section('title') {{ trans('site.profile') }} @endsection
@section('style')
@endsection

@section('content')
    <div class="wrapper">
        <!------------- banner ------------>
        <section class="banner single-page">
            <img src="{{site_path()}}/images/banner.png" alt="">
            <div class="banner-over">
            </div>
        </section>
        <!------------- end banner ------------>
        @php 
            $lang  = App::getLocale();
            $title = 'title_' . $lang;
            $desc  = 'desc_' . $lang;
        @endphp
        <!------------- about ------------->
        <section class="about dark-txt">
            <div class="container">
                <h2 class="section-title">
                    {{-- تعديل بيانات --}}
                    {{ trans('site.Edit_data') }}
                </h2>
                <form class="login-form" action="{{route('site_post_profile')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group border-none">
                        <div class="img-block">
                            <div class="upload-img">
                                <div class="upload-icon">
                                    <i class="fas fa-camera-retro"></i>
                                </div>
                                <input type="file" id="gallery-photo-add" name="avatar">
                            </div>
                            <div class="gallery">
                                <div class="images">
                                    <img src="{{appPath()}}/public/images/users/{{Auth::user()->avatar}}" alt="">
                                    <input name="avatar" type="hidden">
                                    <button class="close">
                                        <i class="fa fa-times-circle"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group dark-txt">
                        <input type="text" class="form-control" name="first_name" value="{{Auth::user()->first_name}}" required>
                        <label for="first_name">
                            {{--  الأسم الأول  --}}
                            {{ trans('site.First_name') }}
                        </label>
                    </div>
                    <div class="form-group dark-txt">
                        <input type="text" class="form-control" name="last_name" value="{{Auth::user()->last_name}}" required>
                        <label for="last_name">
                            {{--  أسم العائلة  --}}
                            {{ trans('site.Last_name') }}
                        </label>
                    </div>
                    <div class="form-group dark-txt">
                        <input type="tel" class="form-control phone" name="phone" value="{{Auth::user()->phone}}" required>
                        <label for="phone">
                            {{--  رقم الجوال  --}}
                            {{ trans('site.Mobile') }}
                        </label>
                    </div>
                    <div class="form-group dark-txt">
                        <input type="email" class="form-control" name="email" value="{{Auth::user()->email}}" required>
                        <label for="email">
                            {{--  البريد الألكتروني  --}}
                            {{ trans('site.Email') }}
                        </label>
                    </div>
                    <div class="form-group dark-txt">
                        <input type="text"  class="form-control datepicker1" name="birthdate" value="{{Auth::user()->birthdate}}" required>
                        <label for="birthdate">
                            {{--  تاريخ الميلاد  --}}
                            {{ trans('site.Birthdate') }}
                        </label>
                        <i class="far fa-calendar-alt brown-txt"></i>
                    </div>
                    <div class="form-group dark-txt">
                        <select name="gender" id="" class="form-control" required>
                            <option value="male" @if(Auth::user()->gender == 'male') selected @endif>
                                {{--  ذكر  --}}
                                {{ trans('site.Male') }}
                            </option>
                            <option value="female" @if(Auth::user()->gender == 'female') selected @endif>
                                {{--  انثى  --}}
                                {{ trans('site.Female') }}
                            </option>
                        </select>
                        <label for="">
                            {{--  الجنس  --}}
                            {{ trans('site.gender') }}
                        </label>
                        <i class="fas fa-angle-down brown-txt"></i>
                    </div>
                    <div class="form-group dark-txt">
                        <select name="specialist_id" id="" class="form-control" required>
                            @foreach (getSpecialist() as $item)
                                <option value="{{$item->id}}" @if(Auth::user()->specialist_id == $item->id) selected @endif>
                                    {{$item->$title}}
                                </option>
                            @endforeach
                        </select>
                        <label for="">
                            {{--  الحالة الأجتماعية  --}}
                            {{ trans('site.Social_status') }}
                        </label>
                        <i class="fas fa-angle-down brown-txt"></i>
                    </div>
                    <div class="form-group dark-txt">
                        <select name="country_id" id="country_id" class="form-control" onchange="showCity()" required>
                            @foreach (getCountry() as $item)
                                <option value="{{$item->id}}" @if($item->id == Auth::user()->country_id) selected @endif>{{$item->$title}}</option>
                            @endforeach
                        </select>
                        <label for="">
                            {{--  الدولة  --}}
                            {{ trans('site.country') }}
                        </label>
                        <i class="fas fa-angle-down brown-txt"></i>
                    </div>
                    <div class="form-group dark-txt">
                        <select name="city_id" id="city_id" class="form-control" required>
                        </select>
                        <label for="">
                            {{--  المدينة  --}}
                            {{ trans('site.city') }}
                        </label>
                        <i class="fas fa-angle-down brown-txt"></i>
                    </div>
                    <div class="form-group dark-txt">
                        <input type="text" class="form-control" name="job" value="{{Auth::user()->job}}" required>
                        <label for="">
                            {{--  الوظيفة  --}}
                            {{ trans('site.Job') }}
                        </label>
                    </div>
                    {{-- <div class="form-group dark-txt">
                        <input type="password" class="form-control" name="password">
                        <label for="">
                            {{ trans('site.password') }}
                        </label>
                    </div>
                    <div class="form-group dark-txt">
                        <input type="password" class="form-control" name="password_confirmation">
                        <label for="">
                            {{ trans('site.confirmPassword') }}
                        </label>
                    </div> --}}
                    <p class="text-center">
                        <a href="{{route('site_update_password')}}" class="dark-txt">
                            {{-- تغير كلمة المرور --}}
                            {{ trans('site.update_password') }}
                        </a>
                    </p>
                    <button type="submit" class="btn btn-site brown-bg d-block">
                        تعديل
                    </button>
                </form>
            </div>
        </section>
        <!------------- end about ------------->
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function(){
            showCity();
        });

        function showCity() {
            let country_id = $('#country_id').val();
            $.ajax({
                type: "POST",
                url: "{{route('showCity')}}",
                data: {country_id: country_id, _token: '{{csrf_token()}}'},
                success: function( msg ) {
                    $('#city_id').html(msg);
                }
            });
        }

        function checkAgree() {
            if($('#read_conditions').is(':checked')){
                $('#register_submit').removeAttr('disabled');
            }
            else{
                $('#register_submit').attr('disabled','');
            }
        }
    </script>
@endsection