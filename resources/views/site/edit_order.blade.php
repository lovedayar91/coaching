@extends('site.master')
@section('title') {{trans('site.orders')}} @endsection
@section('style')
@endsection

@section('content')
    <div class="wrapper">
        <!------------- banner ------------>
        <section class="banner single-page">
            <img src="{{site_path()}}/images/banner.png" alt="">
            <div class="banner-over">
            </div>
        </section>
        <!------------- end banner ------------>
        @php 
            $lang  = App::getLocale();
            $name  = 'name_' . $lang;
            $title = 'title_' . $lang;
            $desc  = 'desc_' . $lang;
        @endphp
        <!------------- about ------------->
        <section class="about dark-txt">
            <div class="container">
                <h2 class="section-title">
                    {{-- تعرف أكثر علي المدربات --}}
                    {{ trans('site.know_more_about_trainers') }}
                </h2>
                <form action="{{route('site_post_edit_order')}}" class="login-form" method="POST">
                    @csrf
                    <div class="row">
                        @foreach (getCoachs() as $item)
                            <div class="col-md-6 col-12">
                                <div class="tainer">
                                    <input type="radio" name="coachs_id" value="{{$item->id}}" @if($order->coach_id == $item->id) checked @endif  onclick="check_avaliable_date({{$item->id}})">
                                    <div class="tainer-img">
                                        <img src="{{$item->Images->count() > 0 ? $item->Images->first()->image : site_path() . '/images/trainer.png'}}" alt="">
                                    </div>
                                    <h5 class="brown-txt text-center">
                                        {{$item->$name}}
                                    </h5>
                                </div>
                            </div>
                        @endforeach
                    </div>

                    {{--  Date Data  --}}
                    <input type="hidden" name="id" id="order_id" value="{{$order->id}}">
                    <input type="hidden" name="coach_id" id="coach_id" value="{{$order->coach_id}}">
                    <input type="hidden" name="max_count" id="max_count"  value="1">
                    <input type="hidden" name="session_count" id="allow_count"  value="0">

                    <div class="row remove-check">
                        <div class="col-12">
                            <div class="form-group dark-txt">
                                <input type="text" name="date" id="date" value="{{$order->date}}" class="form-control datepicker" readonly required 
                                onclick="emptyData()" onchange="check_avaliable_time()">
                                <label for="">
                                    {{-- التاريخ --}}
                                    {{ trans('site.date') }}
                                </label>
                                <i class="far fa-calendar-alt brown-txt"></i>
                            </div>
                            <div class="form-group dark-txt">
                                <input type="text" class="form-control clock" readonly>
                                <label for="">
                                    {{-- الوقت --}}
                                    {{ trans('site.time') }}
                                </label>
                                <i class="far fa-clock  brown-txt"></i>
                            </div>
                        </div>
                        <div class="time-content">
                            <div class="time-check">
                                @foreach (getTimes() as $item)
                                    <div class="times">
                                        {{-- <input type="checkbox" class="booking" disabled> --}}
                                        <input type="checkbox" name="time_id" @if($item->id == $order->time_id) checked @endif class="times" id="time{{$item->id}}" value="{{$item->id}}" 
                                        onclick="check_session_count({{$item->id}})" onchange="change_allow_count()">
                                        <span>
                                            {{$item->$title}}
                                        </span>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <button type="submit" class="btn-site brown-bg" onclick="check_validation()">
                            {{-- حجز --}}
                            {{ trans('site.reserve') }}
                        </button>
                    </div>
                </form>
            </div>
        </section>
        <!------------- end about ------------->
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function () { 
            check_avaliable_date_in_edit();
            check_avaliable_time_in_edit();
        });

        $('.clock').on('click', function () {
            if ($('#date').val() != null && $('#date').val() != '' && $('#date').val() != undefined) {
                $('.time-content').slideDown();
            } else {
                $('.clock').notify(
                    "{{trans('site.choose_date_first')}}", {
                        position: "bottom"
                    }
                );
            }
        });

        var eventHolidays = {};
        var eventDates = {};

        var holidays = '';
        var dates    = '';

        for (var i = 0; i < dates.length; i++) {
            eventDates[new Date(dates[i])] = new Date(dates[i]);
        }
        for (var i = 0; i < holidays.length; i++) {
            eventHolidays[new Date(holidays[i])] = new Date(holidays[i]);
        }

        $(".datepicker").datepicker({
            dateFormat: "yy-mm-dd", //format
            minDate: 0, // disabled old dates
            beforeShowDay: function (date) {

                var highlight = eventDates[date];
                var value1    = eventHolidays[date];

                if (value1) {

                    return [true, "ui-state-disabled", 'holiday'];

                } else if (highlight) {

                    return [true, "events ui-state-disabled", 'reserved'];

                } else {

                    return [true, '', ''];

                };
            }
        });

        function emptyData(){
            $('.time-content').slideUp();
            $('.times').prop('checked', false);
        }

        function check_validation(){
            var now_count = parseInt($('input:checkbox:checked').length);
            if(now_count == 0){
                event.preventDefault();
                $('.clock').notify(
                    "{{trans('site.choose_time')}}", {
                        position: "bottom"
                    }
                );
            }
        }

        function check_avaliable_date_in_edit() {
            var coach_id = {{$order->coach_id}};
            $('#coach_id').val(coach_id);
            eventHolidays   = {};
            eventDates      = {};
            holidays        = '';
            dates           = '';

            for (var i = 0; i < dates.length; i++) {
                eventDates[new Date(dates[i])] = new Date(dates[i]);
            }
            for (var i = 0; i < holidays.length; i++) {
                eventHolidays[new Date(holidays[i])] = new Date(holidays[i]);
            }

            $.ajax({
                type: "POST",
                url: "{{route('check_avaliable_date')}}",
                data: {coach_id: coach_id, _token: '{{csrf_token()}}'},
                success: function( msg ) {
                    holidays = msg.holidays;
                    dates    = msg.dates;
                    for (var i = 0; i < dates.length; i++) {
                        eventDates[new Date(dates[i])] = new Date(dates[i]);
                    }
                    for (var i = 0; i < holidays.length; i++) {
                        eventHolidays[new Date(holidays[i])] = new Date(holidays[i]);
                    }

                    $('.remove-check').slideDown();
                    $('.time-content').slideDown();
                }
            });    
        }

        function check_avaliable_date(coach_id) {
            change_allow_count();
            $('.time-content').slideUp();
            $('#date').val('');
            $('#coach_id').val(coach_id);
            eventHolidays   = {};
            eventDates      = {};
            holidays        = '';
            dates           = '';

            for (var i = 0; i < dates.length; i++) {
                eventDates[new Date(dates[i])] = new Date(dates[i]);
            }
            for (var i = 0; i < holidays.length; i++) {
                eventHolidays[new Date(holidays[i])] = new Date(holidays[i]);
            }

            $.ajax({
                type: "POST",
                url: "{{route('check_avaliable_date')}}",
                data: {coach_id: coach_id, _token: '{{csrf_token()}}'},
                success: function( msg ) {
                    holidays = msg.holidays;
                    dates    = msg.dates;
                    for (var i = 0; i < dates.length; i++) {
                        eventDates[new Date(dates[i])] = new Date(dates[i]);
                    }
                    for (var i = 0; i < holidays.length; i++) {
                        eventHolidays[new Date(holidays[i])] = new Date(holidays[i]);
                    }
                }
            });    
        }

        function check_avaliable_time_in_edit() {
            var coach_id = $('#coach_id').val();
            var date     = $('#date').val();
            var time_id  = {{$order->time_id}};
            $.ajax({
                type: "POST",
                url: "{{route('check_avaliable_time')}}",
                data: {coach_id: coach_id ,date: date, _token: '{{csrf_token()}}'},
                success: function( msg ) {
                    $('.times').removeClass('booking');
                    $('.times').removeAttr('disabled','');
                    msg.time_ids.forEach(id => {
                        if(id != time_id){
                            $('#time'+id).addClass('booking');
                            $('#time'+id).attr('disabled','');
                        }
                    });
                    $('.time-content').slideDown();
                }
            });    
        }

        function check_avaliable_time(coach_id) {
            change_allow_count();
            var coach_id = $('#coach_id').val();
            var date     = $('#date').val();
            var time_id  = {{$order->time_id}};
            $.ajax({
                type: "POST",
                url: "{{route('check_avaliable_time')}}",
                data: {coach_id: coach_id ,date: date, _token: '{{csrf_token()}}'},
                success: function( msg ) {
                    $('.times').removeClass('booking');
                    $('.times').removeAttr('disabled','');
                    msg.time_ids.forEach(id => {
                        if(id != time_id){
                            $('#time'+id).addClass('booking');
                            $('#time'+id).attr('disabled','');
                        }
                    });
                    $('.time-content').slideDown();
                }
            });    
        }

        function check_session_count(id) {
            var max_count = parseInt($('#max_count').val());
            var allow_count = parseInt($('#allow_count').val());
            if(allow_count == 0 && $('#time'+id).is(':checked')){
                event.preventDefault();
                $('.clock').notify(
                    "{{trans('site.finish_your_session_count')}}" + " " + max_count + " " + "{{trans('site.sessions')}}", {
                        position: "bottom"
                    }
                );
            }
        }

        function change_allow_count() {
            var max_count = parseInt($('#max_count').val());
            var now_count = parseInt($('input:checkbox:checked').length);
            var allow_count = max_count- now_count;
            $('#allow_count').val(allow_count);
        }
    </script>
@endsection