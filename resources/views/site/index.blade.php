@extends('site.master')
@section('title') {{ trans('site.home') }} @endsection
@section('style')
@endsection

@section('content')
    <div class="wrapper">
        <!------------- banner ------------>
        @php
            $banner  = App\Models\Slider::first();  
            $lang    = App::getLocale();
            $name    = 'name_' . $lang;
            $title   = 'title_' . $lang;
            $desc    = 'desc_' . $lang;
            $address = 'address_' . $lang;
            $who_us_vission    = 'who_us_vission_' . $lang;
            $who_us_message    = 'who_us_message_' . $lang;
            $who_us_short_desc = 'who_us_short_desc_' . $lang;
            $about_training_short_desc = 'about_training_short_desc_' . $lang;
        @endphp
        <section class="banner">
            <img src="{{$banner->image}}" alt="">
            <div class="banner-over">
                <div class="caption">
                    <p>
                      {{$banner->$desc}}
                    </p>
                    <a href="{{$banner->url}}" class="dark-txt">
                        {{--  تواصل معنا  --}}
                        {{ trans('site.Contact_Us') }}
                    </a>
                </div>
            </div>
        </section>
        <!------------- end banner ------------>
        <!------------- about ------------->
        <section class="about dark-txt">
            <div class="container">
                <h2 class="section-title">
                    {{--  عن الموقع  --}}
                    {{trans('site.about_site')}}
                </h2>
                <div class="row">
                    <div class="col-12">
                        <p class="m-b gray-txt">
                            {!! settings($who_us_short_desc) !!}
                        </p>
                    </div>

                    <div class="col-sm-6 col-12">
                        <div class="mission">
                            <i class="far fa-eye brown-txt"></i>
                            <h5>
                                {{--  رؤيتنا المستقبلية  --}}
                                {{trans('site.Our_Vission')}}
                            </h5>
                            <p class="gray-txt">
                                {!! settings($who_us_vission) !!}
                            </p>
                        </div>
                    </div>

                    <div class="col-sm-6 col-12">
                        <div class="mission">
                            <i class="fas fa-envelope brown-txt"></i>
                            <h5>
                                {{--  رسالتنا  --}}
                                {{trans('site.Our_Message')}}
                            </h5>
                            <p class="gray-txt">
                                {!! settings($who_us_message) !!}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!------------- end about ------------->
        <!------------- about-train ------------->
        <section class="about-train">
            <div class="over"></div>
            <div class="container">
                <h2 class="section-title">{{ trans('site.aboutTraining') }}</h2>
                <p class="m-b">
                    {!! settings($about_training_short_desc) !!}
                </p>
                <a href="{{route('site_about_train')}}">
                    {{--  المزيد  --}}
                    {{ trans('site.more') }}
                </a>
            </div>
        </section>
        <!------------- end about-train ------------->
        <!------------- offers ------------->
        <section class="offers">
            <div class="container">
                <h2 class="section-title">{{ trans('site.offer') }}</h2>
                <div class="row justify-content-center">
                    @foreach (getSection() as $item)
                        <div class="col-lg-4 col-md-6 col-12">
                            <div class="offers-item">
                                <img src="{{$item->image}}" alt="">
                                <div class="details">
                                    <h6>{{$item->$title}}</h6>
                                    <div class="links">
                                        <a href="{{route('site_packages' , $item->id)}}" class="btn dark-bg">
                                            {{-- المزيد --}}
                                            {{ trans('site.more') }}
                                        </a>
                                        <a href="{{route('site_add_order' , $item->id)}}" class="btn brown-bg">
                                            {{-- أشتراك --}}
                                            {{ trans('site.subscripe') }}
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
        <!------------- end offers ------------->
        <!------------- clients ------------->
        <section class="clients" id="client">
            <h2 class="section-title">{{ trans('site.Opinion_of_our_customers') }}</h2>
            <div class="container">
                <div class="clients-slider owl-carousel" id="owl-clients">
                    @foreach (getOurCustomers() as $item)
                        <div class="item">
                            <img src="{{$item->image}}" class="img-responsive center-block" />
                            <h3>{{$item->$name}}</h3>
                            <p>
                                {{$item->$desc}}
                            </p>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
        <!------------- end clients ------------->
    </div>
@endsection

@section('script')
@endsection