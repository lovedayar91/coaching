@extends('site.master')
@section('title') {{ trans('site.whoUs') }} @endsection
@section('style')
@endsection

@section('content')
    <div class="wrapper">
        <!------------- banner ------------>
        <section class="banner single-page">
            <img src="{{site_path()}}/images/banner.png" alt="">
            <div class="banner-over">
            </div>
        </section>
        <!------------- end banner ------------>
        @php 
            $lang  = App::getLocale();
            $name  = 'name_' . $lang;
            $title = 'title_' . $lang;
            $desc  = 'desc_' . $lang;
            $who_us_desc       = 'who_us_desc_' . $lang;
            $who_us_vission    = 'who_us_vission_' . $lang;
            $who_us_message    = 'who_us_message_' . $lang;
        @endphp
        <!------------- about ------------->
        <section class="about dark-txt">
            <div class="container">
                <h2 class="section-title">
                    {{--  من نحن  --}}
                    {{trans('site.whoUs')}}
                </h2>
                <img src="{{settings('who_us_image')}}" class="img-fluid img-about" alt="">
                <div class="row">
                    <div class="col-12">
                        <p class="m-b gray-txt">
                            {{settings($who_us_desc)}}
                        </p>
                    </div>

                    <div class="col-sm-6 col-12">
                        <div class="mission">
                            <i class="far fa-eye brown-txt"></i>
                            <h5>
                                {{--  رؤيتنا المستقبلية  --}}
                                {{trans('site.Our_Vission')}}
                            </h5>
                            <p class="gray-txt">
                                {{settings($who_us_vission)}}
                            </p>
                        </div>
                    </div>

                    <div class="col-sm-6 col-12">
                        <div class="mission">
                            <i class="fas fa-envelope brown-txt"></i>
                            <h5>
                                {{--  رسالتنا  --}}
                                {{trans('site.Our_Message')}}
                            </h5>
                            <p class="gray-txt">
                                {{settings($who_us_message)}}
                            </p>
                        </div>
                    </div>
                </div>
                <hr>
                <h2 class="section-title">
                    {{-- تعرف أكثر علي المدربات --}}
                    {{ trans('site.know_more_about_trainers') }}
                </h2>
                <div class="row">
                    @foreach (getCoachs() as $item)
                        <div class="col-md-6 col-12">
                            <div class="tainer">
                                <a href="{{route('site_trainer', $item->id)}}">
                                    <div class="tainer-img">
                                        <img src="{{$item->Images->count() > 0 ? $item->Images->first()->image : site_path().'/images/trainer.png'}}" alt="">
                                    </div>
                                    <h5 class="brown-txt text-center">
                                        {{$item->$name}}
                                    </h5>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
        <!------------- end about ------------->
    </div>
@endsection

@section('script')
@endsection