@extends('site.master')
@section('title') {{ trans('site.forgetPassword') }} @endsection
@section('style')
@endsection

@section('content')
    <div class="wrapper">
        <!------------- banner ------------>
        <section class="banner single-page">
            <img src="{{site_path()}}/images/banner.png" alt="">
            <div class="banner-over">
            </div>
        </section>
        <!------------- end banner ------------>
        @php 
            $lang  = App::getLocale();
            $title = 'title_' . $lang;
        @endphp
        <!------------- about ------------->
        <section class="about dark-txt">
            <div class="container">
                <h2 class="section-title">
                    {{--  نسيت كلمة المرور  --}}
                    {{ trans('site.forgetPassword') }}
                </h2>
                <form class="login-form" action="{{route('site_post_forget_password')}}" method="POST">
                    @csrf
                    <div class="form-group dark-txt">
                        <input type="tel" class="form-control empty phone" name="phone" value="{{old('phone')}}" required>
                        <label for="phone">
                            {{--  رقم الجوال  --}}
                            {{ trans('site.Mobile') }}
                        </label>
                    </div>
                    <div class="form-group dark-txt">
                        <select name="country_id" id="country_id" class="form-control" required>
                            @foreach (getCountry() as $item)
                                <option value="{{$item->id}}" @if($item->id == 187) selected @endif>{{$item->$title}}</option>
                            @endforeach
                        </select>
                        <label for="">
                            {{--  الدولة  --}}
                            {{ trans('site.country') }}
                        </label>
                        <i class="fas fa-angle-down brown-txt"></i>
                    </div>
                    <button type="submit" class="btn btn-site brown-bg d-block" id="register_submit">
                        {{--  ارسال  --}}
                        {{ trans('site.send') }}
                    </button>
                </form>
            </div>
        </section>
        <!------------- end about ------------->
    </div>
@endsection

@section('script')
@endsection