@extends('site.master')
@section('title') {{trans('site.aboutTraining')}} @endsection
@section('style')
@endsection

@section('content')
    <div class="wrapper">
        <!------------- banner ------------>
        <section class="banner single-page">
            <img src="{{site_path()}}/images/banner.png" alt="">
            <div class="banner-over">
            </div>
        </section>
        <!------------- end banner ------------>
        @php 
            $lang  = App::getLocale();
            $name  = 'name_' . $lang;
            $title = 'title_' . $lang;
            $desc  = 'desc_' . $lang;
            $about_training_desc   = 'about_training_desc_' . $lang;
        @endphp
        <!------------- about ------------->
        <section class="about dark-txt">
            <div class="container">
                <h2 class="section-title">
                    {{-- ما هو التدريب الحياتي ؟ --}}
                    {{ trans('site.about_personal_train') }}
                </h2>
                <div class="row">
                    <div class="col-12">
                        <p class="m-b gray-txt">
                            {{settings($about_training_desc)}}
                        </p>
                    </div>
                </div>
                <hr>
                <div class="about-video">
                    <h2 class="section-title">
                        {{-- فيديو للتعريف بالتدريب الحياتي --}}
                        {{ trans('site.video_life_training') }}
                    </h2>
                    <div class="row">
                        <div class="col-12">
                            <p class="m-b gray-txt">
                                
                            </p>
                        </div>
                        <div class="col-12">
                            <iframe width="100%" height="450" src="{{getEmbedLink(settings('about_training_video'))}}"
                                frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture"
                                allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!------------- end about ------------->
    </div>
@endsection

@section('script')
@endsection