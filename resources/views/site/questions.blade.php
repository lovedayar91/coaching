@extends('site.master')
@section('title') {{ trans('site.Common_Questions') }} @endsection
@section('style')
@endsection

@section('content')
    <div class="wrapper">
        <!------------- banner ------------>
        <section class="banner single-page">
            <img src="{{site_path()}}/images/banner.png" alt="">
            <div class="banner-over">
            </div>
        </section>
        <!------------- end banner ------------>
        @php 
            $lang  = App::getLocale();
            $title = 'title_' . $lang;
            $desc  = 'desc_' . $lang;
        @endphp
        <!------------- about ------------->
        <section class="about dark-txt fqa">
            <div class="container">
                <h2 class="section-title">
                    {{--  الأسئلة الشائعة  --}}
                    {{ trans('site.Common_Questions') }}
                </h2>
                <div class="row">
                    <div class="col-12">
                        <div id="accordion" class="accordion">
                            @foreach ($data as $item)
                                <div class="card">
                                    <div class="card-header collapsed" data-toggle="collapse" href="#collapseOne{{$item->id}}">
                                        <a class="card-title">
                                            {{$item->$title}}
                                        </a>
                                    </div>
                                    <div id="collapseOne{{$item->id}}" class="card-body collapse" data-parent="#accordion">
                                        <p class="brown-txt">
                                            {{$item->$desc}}
                                        </p>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!------------- end about ------------->
    </div>
@endsection

@section('script')
@endsection