@extends('site.master')
@section('title') {{trans('site.check_out')}} @endsection
@section('style')
@endsection

@section('content')
    <div class="wrapper">
        <!------------- banner ------------>
        <section class="banner single-page">
            <img src="{{site_path()}}/images/banner.png" alt="">
            <div class="banner-over">
            </div>
        </section>
        <!------------- end banner ------------>
        @php 
            $lang  = App::getLocale();
            $title = 'title_' . $lang;
            $desc  = 'desc_' . $lang;
        @endphp
        <!------------- about ------------->
        <section class="about dark-txt">
            <div class="container">
                <h2 class="section-title">
                    {{-- الأشتراك والدفع --}}
                    {{ trans('site.subscripe_pay') }}
                </h2>

                <p class="gray-txt">
                    {{$data->$title}}
                    <span>
                        {{$data->session_count}}
                    </span>
                    {{-- جلسات بسعر --}}
                    {{ trans('site.sessions_for') }}
                    <span>
                        {{$data->price}}
                    </span>
                    {{-- ريال سعودي --}}
                    {{ trans('site.SR') }}
                </p>

                <img class="img-fluid pay-join" src="{{site_path()}}/images/pay-join.png" alt="">
                <div class="text-center">
                    <h5 class="brown-txt">
                        {{-- تنبية --}}
                        {{ trans('site.Attention') }}
                    </h5>
                    <h6>
                        <a href="{{route('site_packages_checkout' , $data->id)}}" class="dark-txt">
                            {{-- سيتم تحويلك الأن الى صحفة الدفع الألكتروني --}}
                            {{ trans('site.redirect_to_check_out_page') }}
                        </a>
                    </h6>
                </div>
            </div>
        </section>
        <!------------- end about ------------->
    </div>
@endsection

@section('script')
@endsection