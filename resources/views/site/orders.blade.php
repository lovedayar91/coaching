@extends('site.master')
@section('title') {{ trans('site.forgetPassword') }} @endsection
@section('style')
@endsection

@section('content')
    <div class="wrapper">
        <!------------- banner ------------>
        <section class="banner single-page">
            <img src="{{site_path()}}/images/banner.png" alt="">
            <div class="banner-over">
            </div>
        </section>
        <!------------- end banner ------------>
        @php 
            use Carbon\Carbon;
            $lang  = App::getLocale();
            $name  = 'name_' . $lang;
            $title = 'title_' . $lang;
            $desc  = 'desc_' . $lang;
        @endphp
        <!------------- about ------------->
        <section class="about dark-txt fqa">
            <div class="container">
                <h2 class="section-title">
                    {{--  حجوزاتي  --}}
                    {{ trans('site.my_orders') }}
                </h2>
                <p class="gray-txt">
                    {{-- متبقي لك  --}}
                    {{ trans('site.you_have') }}
                    <span class="brown-txt">
                        <span id="session_count">{{all_sessions_count()}}</span>
                        {{-- جلسات --}}
                        {{ trans('site.sessions') }}
                    </span>
                </p>
                <div class="row">
                    <div class="col-12">
                        <div class="hgozat-tabs">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active dark-txt" id="next-tab" data-toggle="tab" href="#next" role="tab"
                                        aria-controls="next" aria-selected="true">
                                        {{--  الحجوزات القادمة  --}}
                                        {{ trans('site.comming_orders') }}
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link dark-txt" id="last-tab" data-toggle="tab" href="#last" role="tab"
                                        aria-controls="last" aria-selected="false">
                                        {{--  الحجوزات السابقة  --}}
                                        {{ trans('site.finish_orders') }}
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="next" role="tabpanel" aria-labelledby="next-tab">
                                    <div class="row">
                                        @foreach ($current_orders as $item)
                                            <div class="col-md-6 col-12 delete-item">
                                                <div class="hgozat-item">
                                                    <img class="img-fluid" src="{{$item->Coach->Images->count() > 0 ? $item->Coach->Images->first()->image : site_path() . '/images/trainer.png'}}" alt="">
                                                    <div class="details">
                                                        <h5 class="brown-txt">{{$item->Coach->$name}} </h5>
                                                        <p>
                                                            <i class="far fa-calendar-alt brown-txt"></i>
                                                            <span>{{$item->date}}</span>
                                                        </p>
                                                        <p>
                                                            <i class="far fa-clock brown-txt"></i>
                                                            <span>{{$item->Time->$title}}</span>
                                                        </p>
                                                    </div>
                                                    @if(!Carbon::parse($item->full_date)->subHours(settings('hours_unallow_count'))->isPast())
                                                        <div class="edit-hgozat">
                                                            <div class="dropdown show">
                                                                <button class="dropdown-toggle" type="button"
                                                                    id="dropdownMenuButton" data-toggle="dropdown"
                                                                    aria-haspopup="true" aria-expanded="true">
                                                                    <i class="fas fa-ellipsis-v"></i>
                                                                </button>
                                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton"
                                                                    x-placement="bottom-start"
                                                                    style="position: absolute; transform: translate3d(0px, 22px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                                    <a class="dropdown-item" href="{{route('site_edit_order',$item->id)}}">
                                                                        {{--  تعديل  --}}
                                                                        {{ trans('site.edit') }}
                                                                    </a>
                                                                    <a class="dropdown-item delete-hgozat" href="#" onclick="delete_order({{$item->id}})">
                                                                        {{--  حذف  --}}
                                                                        {{ trans('site.delete') }}
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                    @if(Carbon::parse($item->full_date)->subMinute()->isPast() && Carbon::parse($item->full_date)->addMinutes(59)->greaterThan(Carbon::now()))
                                                        <a href="skype:{{$item->Coach->skype_id}}?call" class="skype-call" target="_blank">
                                                            <i class="fab fa-skype"></i>
                                                            {{--  أبدء الجلسة  --}}
                                                            {{ trans('site.start_session') }}
                                                        </a>
                                                    @endif
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="last" role="tabpanel" aria-labelledby="last-tab">
                                    <div class="row">
                                        @foreach ($finish_orders as $item)
                                            <div class="col-md-6 col-12 delete-item">
                                                <div class="hgozat-item">
                                                    <img class="img-fluid" src="{{$item->Coach->Images->count() > 0 ? $item->Coach->Images->first()->image : site_path() . '/images/trainer.png'}}" alt="">
                                                    <div class="details">
                                                        <h5 class="brown-txt">{{$item->Coach->$name}} </h5>
                                                        <p>
                                                            <i class="far fa-calendar-alt brown-txt"></i>
                                                            <span>{{$item->date}}</span>
                                                        </p>
                                                        <p>
                                                            <i class="far fa-clock brown-txt"></i>
                                                            <span>{{$item->Time->$title}}</span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!------------- end about ------------->
    </div>
@endsection

@section('script')
    <script>
        function delete_order(id) { 
            $.ajax({
                type: "POST",
                url: "{{route('site_delete_order')}}",
                data: {id: id, _token: '{{csrf_token()}}'},
                success: function( msg ) {
                    var count = parseInt($('#session_count').val()) + 1;
                    $('#session_count').val(count);
                }
            }); 
        }
    </script>
@endsection