@extends('site.master')
@php 
    $lang  = App::getLocale();
    $title = 'title_' . $lang;
    $desc  = 'desc_' . $lang;
@endphp
@section('title') {{ $data->$title }} @endsection
@section('style')
@endsection

@section('content')
    <div class="wrapper">
        <!------------- banner ------------>
        <section class="banner single-page">
            <img src="{{site_path()}}/images/banner.png" alt="">
            <div class="banner-over">
            </div>
        </section>
        <!------------- end banner ------------>
        <!------------- about ------------->
        <section class="about dark-txt fqa">
            <div class="container">
                <h2 class="section-title">{{$data->$title}}</h2>
                <p class="gray-txt">
                    {{$data->$desc}}
                </p>
                <div class="row justify-content-center">
                    @foreach ($data->Packages as $item)
                        <div class="col-lg-4 col-md-6 col-12">
                            <div class="packageBlock">
                                <div class="packageTitle dark-bg">
                                    <h4>{{$item->$title}}</h4>
                                </div>
                                <div class="packageBody">
                                    <h6 class="text-center">
                                        {{-- مميزات الباقة --}}
                                        {{ trans('site.Package_Features') }}
                                    </h6>
                                    <ul>
                                        <li>{{$item->$desc}}</li>
                                    </ul>
                                </div>
                                <div class="packageButton">
                                    <a class="btn brown-bg text-center" href="{{route('site_packages_checkout_info' , $item->id)}}">
                                        {{-- الاشتراك --}}
                                        {{ trans('site.subscripe') }}
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
        <!------------- end about ------------->
    </div>
@endsection

@section('script')
@endsection