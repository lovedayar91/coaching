@extends('site.master')
@section('title') {{trans('site.check_out')}} @endsection
@section('style')
@endsection

@section('content')
    <div class="wrapper">
        <!------------- banner ------------>
        <section class="banner single-page">
            <img src="{{site_path()}}/images/banner.png" alt="">
            <div class="banner-over">
            </div>
        </section>
        <!------------- end banner ------------>
        @php 
            $lang  = App::getLocale();
            $title = 'title_' . $lang;
            $desc  = 'desc_' . $lang;
        @endphp
        <!------------- about ------------->
        <section class="about dark-txt">
            <div class="container">
                <h2 class="section-title">
                    {{-- تهانينا --}}
                    {{ trans('site.Congratulations') }}
                </h2>

                <img class="img-fluid pay-join pay-complate" src="{{site_path()}}/images/CE.png" alt="">

                <div class="text-center">
                    <h5 class="brown-txt">
                        {{-- تمت العملية بنجاح --}}
                        {{ trans('site.Operation_accomplished_successfully') }}
                    </h5>
                    <h6>
                        {{-- تم حجز مقعدك في  --}}
                        {{ trans('site.reservation_done_in') }}
                        <span class="brown-txt">
                            {{$data->Section->$title}}
                        </span>
                            {{-- متبقي لك  --}}
                            {{ trans('site.you_have') }}
                        <span class="brown-txt">
                            {{count_sessions($data->Section->id)}}
                            {{-- جلسات --}}
                            {{ trans('site.sessions') }}
                        </span>
                        {{-- ستصلك رسالة في بريدك الألكتروني عن تفاصيل الباقة ونتمني لك التوفيق والنجاح --}}
                        {{ trans('site.will_recive_email') }}
                    </h6>
                    <div>
                        <a href="{{route('site_add_order' , $data->Section->id)}}" class="btn brown-bg">
                            {{-- أشتراك --}}
                            {{ trans('site.subscripe') }}
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <!------------- end about ------------->
    </div>
@endsection

@section('script')
@endsection