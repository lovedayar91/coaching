<?php

use Illuminate\Support\Facades\Route;
// use function GuzzleHttp\json_encode;

//Genrate bcrypt 123456
// Route::get('/hash', function () {
//     return fillData();
// });                                          



//test some code
// Route::get('/test-code', function () {
//     return showDatesDesc(92);
//     $data = [
//         [
//             'title' => 'test title 1',
//             'price' => '20'
//         ], [
//             'title' => 'test title 2',
//             'price' => '15'
//         ]
//     ];
//     $data = json_encode($data);
//     return $data;
// });


//WebSite
#change Lang
Route::get('change-lang/{lang}', 'SiteController@language')->name('site_language');

//Lang check
Route::middleware('Lang')->group(function () {
    #Ajax
    Route::post('show-city', 'ajaxController@show_city')->name('showCity');
    Route::post('check-avaliable-date', 'ajaxController@check_avaliable_date')->name('check_avaliable_date');
    Route::post('check-avaliable-date-dashboard', 'ajaxController@check_avaliable_date_dashboard')->name('check_avaliable_date_dashboard');
    Route::post('check-avaliable-time', 'ajaxController@check_avaliable_time')->name('check_avaliable_time');

    #Home Page
    Route::get('/', 'SiteController@index')->name('site_index');
    #Contact Page
    Route::get('contact-us', 'SiteController@contact')->name('site_contact_us');
    Route::post('contact-us', 'SiteController@post_contact')->name('site_post_contact_us');
    #conditions Page
    Route::get('conditions', 'SiteController@conditions')->name('site_conditions');
    #about_us Page
    Route::get('about-us', 'SiteController@about_us')->name('site_about_us');
    #links Page
    Route::get('links', 'SiteController@links')->name('site_links');
    #questions Page
    Route::get('questions', 'SiteController@questions')->name('site_questions');
    #about_train Page
    Route::get('about-train', 'SiteController@about_train')->name('site_about_train');
    #trainer Page
    Route::get('trainer/{id}', 'SiteController@trainer')->name('site_trainer');

    #siteAuth Start
    Route::get('logout', 'siteAuthController@logout')->name('site_logout');

    Route::get('login', 'siteAuthController@login')->name('site_login');
    Route::post('post_login', 'siteAuthController@post_login')->name('site_post_login');

    Route::get('register', 'siteAuthController@register')->name('site_register');
    Route::post('post_register', 'siteAuthController@post_register')->name('site_post_register');

    Route::get('forget_password', 'siteAuthController@forget_password')->name('site_forget_password');
    Route::post('post_forget_password', 'siteAuthController@post_forget_password')->name('site_post_forget_password');

    Route::get('reset_password/{id}', 'siteAuthController@reset_password')->name('site_reset_password');
    Route::post('post_reset_password', 'siteAuthController@post_reset_password')->name('site_post_reset_password');

    Route::get('profile', 'siteAuthController@profile')->name('site_profile');
    Route::post('post_profile', 'siteAuthController@post_profile')->name('site_post_profile');

    Route::get('update_password', 'siteAuthController@update_password')->name('site_update_password');
    Route::post('post_update_password', 'siteAuthController@post_update_password')->name('site_post_update_password');
    #siteAuth End

    //Auth users check
    Route::middleware('siteAuth')->group(function () {
        #profile Page
        Route::get('profile', 'siteAuthController@profile')->name('site_profile');

        ##########################Packages & check-out##########################
        #Packages page
        Route::get('packages/{id}', 'SiteController@packages')->name('site_packages');
        #packages_checkout_info page
        Route::get('packages-checkout-info/{id}', 'SiteController@packages_checkout_info')->name('site_packages_checkout_info');
        #packages_checkout page
        Route::get('packages-checkout/{id}', 'SiteController@packages_checkout')->name('site_packages_checkout');
        Route::post('packages-checkout', 'SiteController@post_packages_checkout')->name('site_post_packages_checkout');
        #packages_checkout_success page
        Route::get('packages-checkout-success/{id}', 'SiteController@packages_checkout_success')->name('site_packages_checkout_success');

        ##########################orders##########################
        #my orders Page
        Route::get('orders', 'SiteController@orders')->name('site_orders');
        #add_order page
        Route::get('add-order/{id}', 'SiteController@add_order')->name('site_add_order');
        Route::post('add-order', 'SiteController@post_add_order')->name('site_post_add_order');
        #edit_order page
        Route::get('edit-order/{id}', 'SiteController@edit_order')->name('site_edit_order');
        Route::post('edit-order', 'SiteController@post_edit_order')->name('site_post_edit_order');
        #delete_order page
        Route::post('delete-order', 'SiteController@delete_order')->name('site_delete_order');
    });
});


###################################################################################################################################################
###################################################################################################################################################

//Dashboard
Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {

    Route::get('/login', 'AuthController@loginForm')->name('loginForm');

    Route::post('/login', 'AuthController@login')->name('login');

    /**** ajax routes ****/
    Route::post('change-checked', 'UsersController@change_checked')->name('change-checked');
    Route::post('change-offer-checked', 'offerController@change_checked')->name('change-offer-checked');
    Route::post('change-special', 'sectionController@change_special')->name('change-special');
    Route::post('change-addService', 'providersController@change_addService')->name('change-addService');
    Route::post('change-addService-notfiy', 'providersController@change_addService_notfiy')->name('change-addService-notfiy');
    Route::post('change-activation', 'sliderController@change_activation')->name('change-activation');
    Route::post('change-ad-activation', 'adController@change_ad_activation')->name('change-ad-activation');
    Route::post('change-active', 'sectionController@change_active')->name('change-active');
    Route::post('showProviderDetails', 'providersController@showProviderDetails')->name('showProviderDetails');
    Route::post('showProviderBill', 'providersController@showProviderBill')->name('showProviderBill');
    Route::post('rmvImage', 'coachController@rmvImage')->name('rmvImage');
    Route::post('addData', 'coachController@addData')->name('addData');
    Route::post('rmvData', 'coachController@rmvData')->name('rmvData');
    Route::post('change-special-service', 'serviceController@change_special_service')->name('change-special-service');
    Route::post('change-new-service', 'serviceController@change_new_service')->name('change-new-service');
    Route::post('showOrderPayDetails', 'finicial_accountController@showOrderPayDetails')->name('showOrderPayDetails');

    // Auth user only
    Route::group(['middleware' => ['admin', 'check_role']], function () {

        Route::get('/', [
            'uses' => 'AuthController@dashboard',
            'as' => 'dashboard',
            'icon' => '<i class="fa fa-dashboard"></i>',
            'title' => 'الرئيسيه'
        ]);

        // ============= Permission ==============
        Route::get('permissions-list', [
            'uses' => 'PermissionController@index',
            'as' => 'permissionslist',
            'title' => 'قائمة الصلاحيات',
            'icon' => '<i class="fa fa-eye"></i>',
            'child' => [
                'addpermissionspage',
                'addpermission',
                'editpermissionpage',
                'updatepermission',
                'deletepermission',

            ]
        ]);

        Route::get('permissions', [
            'uses' => 'PermissionController@create',
            'as' => 'addpermissionspage',
            'title' => 'اضافة صلاحيه',
        ]);

        Route::post('add-permission', [
            'uses' => 'PermissionController@store',
            'as' => 'addpermission',
            'title' => 'تمكين اضافة صلاحيه'
        ]);

        #edit permissions page
        Route::get('edit-permissions/{id}', [
            'uses' => 'PermissionController@edit',
            'as' => 'editpermissionpage',
            'title' => 'تعديل صلاحيه'
        ]);

        #update permission
        Route::post('update-permission', [
            'uses' => 'PermissionController@update',
            'as' => 'updatepermission',
            'title' => 'تمكين تعديل صلاحيه'
        ]);

        #delete permission
        Route::post('delete-permission', [
            'uses' => 'PermissionController@destroy',
            'as' => 'deletepermission',
            'title' => 'حذف صلاحيه'
        ]);

        // ========== Settings

        Route::get('settings', [
            'uses' => 'SettingController@index',
            'as' => 'settings',
            'title' => 'الاعدادات',
            'icon' => '<i class="fa fa-cogs"></i>',
            'child' => [
                'sitesetting',
                'add-social',
                'update-social',
                'delete-social',
                'intro',
                'pages',
                'SEO',
            ]
        ]);

        // General Settings
        Route::post('/update-settings', [
            'uses' => 'SettingController@updateSiteInfo',
            'as' => 'sitesetting',
            'title' => 'تعديل بيانات الموقع'
        ]);

        Route::post('/pages', [
            'uses' => 'SettingController@pages',
            'as' => 'pages',
            'title' => 'تعديل من نحن'
        ]);

        Route::post('/app_links', [
            'uses' => 'SettingController@app_links',
            'as' => 'app_links',
            'title' => 'تعديل روابط التطبيق'
        ]);

        Route::post('/intro', [
            'uses' => 'SettingController@intro',
            'as' => 'intro',
            'title' => 'تعديل عن التدريب'
        ]);

        // Social Sites
        Route::post('/add-social', [
            'uses' => 'SettingController@storeSocial',
            'as' => 'add-social',
            'title' => 'اضافة مواقع التواصل'
        ]);

        Route::post('/update-social', [
            'uses' => 'SettingController@updateSocial',
            'as' => 'update-social',
            'title' => 'تعديل مواقع التواصل'
        ]);

        Route::get('/delete-social/{id}', [
            'uses' => 'SettingController@deleteSocial',
            'as' => 'delete-social',
            'title' => 'حذف مواقع التواصل'
        ]);

        Route::post('/seo', [
            'uses' => 'SettingController@SEO',
            'as' => 'SEO',
            'title' => 'تعديل بيانات ال SEO'
        ]);

        #admins
        Route::get('/admins', [
            'uses' => 'AdminController@index',
            'as' => 'admins',
            'title' => 'المشرفين',
            'icon' => '<i class="fa fa-user-circle"></i>',
            'child' => [
                'addadmin',
                'updateadmin',
                'deleteadmin',
                'deleteadmins',
            ]
        ]);

        Route::post('/add-admin', [
            'uses' => 'AdminController@store',
            'as' => 'addadmin',
            'title' => 'اضافة مشرف'
        ]);

        // Update User
        Route::post('/update-admin', [
            'uses' => 'AdminController@update',
            'as' => 'updateadmin',
            'title' => 'تعديل مشرف'
        ]);

        // Delete User
        Route::post('/delete-admin', [
            'uses' => 'AdminController@delete',
            'as' => 'deleteadmin',
            'title' => 'حذف مشرف'
        ]);

        // Delete Users
        Route::post('/delete-admins', [
            'uses' => 'AdminController@deleteAllAdmins',
            'as' => 'deleteadmins',
            'title' => 'حذف اكتر من مشرف'
        ]);

        #users
        Route::get('/users', [
            'uses' => 'UsersController@index',
            'as' => 'users',
            'title' => 'الاعضاء ',
            'icon' => '<i class="fa fa-users"></i>',
            // 'child' => [
            //     'deleteuser',
            //     'deleteusers',
            // ]
        ]);

        // Add User
        Route::post('/add-user', [
            'uses' => 'UsersController@store',
            'as' => 'adduser',
            'title' => 'اضافة عضو'
        ]);

        // Update User
        Route::post('/update-user', [
            'uses' => 'UsersController@update',
            'as' => 'updateuser',
            'title' => 'تعديل عضو'
        ]);

        // Delete User
        Route::post('/delete-user', [
            'uses' => 'UsersController@delete',
            'as' => 'deleteuser',
            'title' => 'حذف عضو'
        ]);

        // Delete Users
        Route::post('/delete-users', [
            'uses' => 'UsersController@deleteAll',
            'as' => 'deleteusers',
            'title' => 'حذف اكتر من عضو'
        ]);

        // Send Notify
        Route::post('/send-notify', [
            'uses' => 'UsersController@sendNotify',
            'as' => 'send-fcm',
            'title' => 'ارسال اشعارات'
        ]);


        #coach
        Route::get('/coachs', [
            'uses' => 'coachController@index',
            'as' => 'coachs',
            'title' => 'المدربات ',
            'icon' => '<i class="fa fa-user"></i>',
            'child' => [
                // 'addcoach',
                'editcoach',
                'updatecoach',
                // 'deletecoach',
                // 'deletecoachs',
            ]
        ]);

        // Add coach
        Route::post('/add-coach', [
            'uses' => 'coachController@store',
            'as' => 'addcoach',
            'title' => 'اضافة مدرب'
        ]);

        // edit coach
        Route::get('/edit-coach/{id}', [
            'uses' => 'coachController@edit',
            'as' => 'editcoach',
            'title' => 'صفحة تعديل مدرب'
        ]);

        // Update coach
        Route::post('/update-coach', [
            'uses' => 'coachController@update',
            'as' => 'updatecoach',
            'title' => 'تعديل مدرب'
        ]);

        // Delete coach
        Route::post('/delete-coach', [
            'uses' => 'coachController@delete',
            'as' => 'deletecoach',
            'title' => 'حذف مدرب'
        ]);

        // Delete coachs
        Route::post('/delete-coachs', [
            'uses' => 'coachController@deleteAll',
            'as' => 'deletecoachs',
            'title' => 'حذف اكتر من مدرب'
        ]);

        #section
        Route::get('/sections', [
            'uses' => 'sectionController@index',
            'as' => 'sections',
            'title' => 'الأقسام',
            'icon' => '<i class="fa fa-align-justify"></i>',
            'child' => [
                'addsection',
                'updatesection',
                'deletesection',
                'deletesections',
            ]
        ]);

        Route::post('/add-section', [
            'uses' => 'sectionController@store',
            'as' => 'addsection',
            'title' => 'اضافة قسم'
        ]);

        Route::post('/update-section', [
            'uses' => 'sectionController@update',
            'as' => 'updatesection',
            'title' => 'تعديل قسم'
        ]);

        Route::post('/delete-section', [
            'uses' => 'sectionController@delete',
            'as' => 'deletesection',
            'title' => 'حذف قسم'
        ]);

        Route::post('/delete-sections', [
            'uses' => 'sectionController@deleteAll',
            'as' => 'deletesections',
            'title' => 'حذف اكتر من قسم'
        ]);

        #package
        Route::get('/packages', [
            'uses' => 'packageController@index',
            'as' => 'packages',
            'title' => 'الباقات ',
            'icon' => '<i class="fa fa-dollar"></i>',
            'child' => [
                'addpackage',
                'updatepackage',
                'deletepackage',
                'deletepackages',
            ]
        ]);

        // Add package
        Route::post('/add-package', [
            'uses' => 'packageController@store',
            'as' => 'addpackage',
            'title' => 'اضافة باقة'
        ]);

        // Update package
        Route::post('/update-package', [
            'uses' => 'packageController@update',
            'as' => 'updatepackage',
            'title' => 'تعديل باقة'
        ]);

        // Delete package
        Route::post('/delete-package', [
            'uses' => 'packageController@delete',
            'as' => 'deletepackage',
            'title' => 'حذف باقة'
        ]);

        // Delete packages
        Route::post('/delete-packages', [
            'uses' => 'packageController@deleteAll',
            'as' => 'deletepackages',
            'title' => 'حذف اكتر من باقة'
        ]);

        #user_packages
        Route::get('/user-packages', [
            'uses' => 'packageController@user_packages',
            'as' => 'user_packages',
            'title' => 'اشتراكات الباقات ',
            'icon' => '<i class="fa fa-dollar"></i>',
        ]);

        // ======== order
        Route::get('all-order', [
            'uses' => 'orderController@index',
            'as' => 'orders',
            'title' => 'الحجوزات',
            'icon' => '<i class="fa fa-cart-plus"></i>',
            'child' => [
                'currentorder',
                'finishorder',
                'todayorder',
                'tomorroworder',
                // 'deleteorder',
            ]
        ]);

        Route::get('/current-order', [
            'uses' => 'orderController@current',
            'as' => 'currentorder',
            'title' => 'الحجوزات الحالية'
        ]);

        Route::get('/today-order', [
            'uses' => 'orderController@today',
            'as' => 'todayorder',
            'title' => 'حجوزات اليوم'
        ]);

        Route::get('/tomorrow-order', [
            'uses' => 'orderController@tomorrow',
            'as' => 'tomorroworder',
            'title' => 'حجوزات الغد'
        ]);

        Route::get('/finish-order', [
            'uses' => 'orderController@finish',
            'as' => 'finishorder',
            'title' => 'الحجوزات المنتهية'
        ]);

        Route::post('/delete-order', [
            'uses' => 'orderController@delete',
            'as' => 'deleteorder',
            'title' => 'حذف طلب'
        ]);


        #question
        Route::get('/questions', [
            'uses' => 'questionController@index',
            'as' => 'questions',
            'title' => 'الاسئلة الشائعة ',
            'icon' => '<i class="fa fa-question"></i>',
            'child' => [
                'addquestion',
                'updatequestion',
                'deletequestion',
                'deletequestions',
            ]
        ]);

        // Add question
        Route::post('/add-question', [
            'uses' => 'questionController@store',
            'as' => 'addquestion',
            'title' => 'اضافة سؤال'
        ]);

        // Update question
        Route::post('/update-question', [
            'uses' => 'questionController@update',
            'as' => 'updatequestion',
            'title' => 'تعديل سؤال'
        ]);

        // Delete question
        Route::post('/delete-question', [
            'uses' => 'questionController@delete',
            'as' => 'deletequestion',
            'title' => 'حذف سؤال'
        ]);

        // Delete questions
        Route::post('/delete-questions', [
            'uses' => 'questionController@deleteAll',
            'as' => 'deletequestions',
            'title' => 'حذف اكتر من سؤال'
        ]);


        #register_low
        Route::get('/register_lows', [
            'uses' => 'register_lowController@index',
            'as' => 'register_lows',
            'title' => 'قوانين التسجيل',
            'icon' => '<i class="fa fa-question"></i>',
            'child' => [
                'addregister_low',
                'updateregister_low',
                'deleteregister_low',
                'deleteregister_lows',
            ]
        ]);

        // Add register_low
        Route::post('/add-register_low', [
            'uses' => 'register_lowController@store',
            'as' => 'addregister_low',
            'title' => 'اضافة قانون تسجيل'
        ]);

        // Update register_low
        Route::post('/update-register_low', [
            'uses' => 'register_lowController@update',
            'as' => 'updateregister_low',
            'title' => 'تعديل قانون تسجيل'
        ]);

        // Delete register_low
        Route::post('/delete-register_low', [
            'uses' => 'register_lowController@delete',
            'as' => 'deleteregister_low',
            'title' => 'حذف قانون تسجيل'
        ]);

        // Delete register_lows
        Route::post('/delete-register_lows', [
            'uses' => 'register_lowController@deleteAll',
            'as' => 'deleteregister_lows',
            'title' => 'حذف اكتر من قانون تسجيل'
        ]);

        #link
        Route::get('/links', [
            'uses' => 'linkController@index',
            'as' => 'links',
            'title' => 'روابط تهمك',
            'icon' => '<i class="fa fa-link"></i>',
            'child' => [
                'addlink',
                'updatelink',
                'deletelink',
                'deletelinks',
            ]
        ]);

        // Add link
        Route::post('/add-link', [
            'uses' => 'linkController@store',
            'as' => 'addlink',
            'title' => 'اضافة رابط'
        ]);

        // Update link
        Route::post('/update-link', [
            'uses' => 'linkController@update',
            'as' => 'updatelink',
            'title' => 'تعديل رابط'
        ]);

        // Delete link
        Route::post('/delete-link', [
            'uses' => 'linkController@delete',
            'as' => 'deletelink',
            'title' => 'حذف رابط'
        ]);

        // Delete links
        Route::post('/delete-links', [
            'uses' => 'linkController@deleteAll',
            'as' => 'deletelinks',
            'title' => 'حذف اكتر من رابط'
        ]);

        #our_customer
        Route::get('/our_customers', [
            'uses' => 'our_customerController@index',
            'as' => 'our_customers',
            'title' => 'رأي عملائنا',
            'icon' => '<i class="fa fa-users"></i>',
            'child' => [
                'addour_customer',
                'updateour_customer',
                'deleteour_customer',
                'deleteour_customers',
            ]
        ]);

        // Add our_customer
        Route::post('/add-our_customer', [
            'uses' => 'our_customerController@store',
            'as' => 'addour_customer',
            'title' => 'اضافة رأي'
        ]);

        // Update our_customer
        Route::post('/update-our_customer', [
            'uses' => 'our_customerController@update',
            'as' => 'updateour_customer',
            'title' => 'تعديل رأي'
        ]);

        // Delete our_customer
        Route::post('/delete-our_customer', [
            'uses' => 'our_customerController@delete',
            'as' => 'deleteour_customer',
            'title' => 'حذف رأي'
        ]);

        // Delete our_customers
        Route::post('/delete-our_customers', [
            'uses' => 'our_customerController@deleteAll',
            'as' => 'deleteour_customers',
            'title' => 'حذف اكتر من رأي'
        ]);

        #slider
        Route::get('/sliders', [
            'uses' => 'sliderController@index',
            'as' => 'sliders',
            'title' => 'لوحة العرض',
            'icon' => '<i class="fa fa-image"></i>',
            'child' => [
                'updateslider',
                // 'addslider',
                // 'deleteslider',
                // 'deletesliders',
            ]
        ]);

        Route::post('/add-slider', [
            'uses' => 'sliderController@store',
            'as' => 'addslider',
            'title' => 'اضافة صورة'
        ]);

        Route::post('/update-slider', [
            'uses' => 'sliderController@update',
            'as' => 'updateslider',
            'title' => 'تعديل صورة'
        ]);

        Route::post('/delete-slider', [
            'uses' => 'sliderController@delete',
            'as' => 'deleteslider',
            'title' => 'حذف صورة'
        ]);

        Route::post('/delete-sliders', [
            'uses' => 'sliderController@deleteAll',
            'as' => 'deletesliders',
            'title' => 'حذف اكتر من صورة'
        ]);

        #country
        Route::get('/countries', [
            'uses' => 'countryController@index',
            'as' => 'countries',
            'title' => 'الدول',
            'icon' => '<i class="fa fa-globe"></i>',
            'child' => [
                'addcountry',
                'updatecountry',
                'deletecountry',
                'deletecountries',
            ]
        ]);

        Route::post('/add-country', [
            'uses' => 'countryController@store',
            'as' => 'addcountry',
            'title' => 'اضافة دولة'
        ]);

        Route::post('/update-country', [
            'uses' => 'countryController@update',
            'as' => 'updatecountry',
            'title' => 'تعديل دولة'
        ]);

        Route::post('/delete-country', [
            'uses' => 'countryController@delete',
            'as' => 'deletecountry',
            'title' => 'حذف دولة'
        ]);

        Route::post('/delete-countries', [
            'uses' => 'countryController@deleteAll',
            'as' => 'deletecountries',
            'title' => 'حذف اكتر من دولة'
        ]);


        #city
        Route::get('/cities', [
            'uses' => 'cityController@index',
            'as' => 'cities',
            'title' => 'المدن',
            'icon' => '<i class="fa fa-globe"></i>',
            'child' => [
                'addcity',
                'updatecity',
                'deletecity',
                'deletecities',
            ]
        ]);

        Route::post('/add-city', [
            'uses' => 'cityController@store',
            'as' => 'addcity',
            'title' => 'اضافة مدينة'
        ]);

        Route::post('/update-city', [
            'uses' => 'cityController@update',
            'as' => 'updatecity',
            'title' => 'تعديل مدينة'
        ]);

        Route::post('/delete-city', [
            'uses' => 'cityController@delete',
            'as' => 'deletecity',
            'title' => 'حذف مدينة'
        ]);

        Route::post('/delete-cities', [
            'uses' => 'cityController@deleteAll',
            'as' => 'deletecities',
            'title' => 'حذف اكتر من مدينة'
        ]);

        #neighborhood
        // Route::get('/neighborhoods', [
        //     'uses' => 'neighborhoodController@index',
        //     'as' => 'neighborhoods',
        //     'title' => 'الحي',
        //     'icon' => '<i class="fa fa-globe"></i>',
        //     'child' => [
        //         'addneighborhood',
        //         'updateneighborhood',
        //         'deleteneighborhood',
        //         'deleteneighborhoods',
        //     ]
        // ]);

        // Route::post('/add-neighborhood', [
        //     'uses' => 'neighborhoodController@store',
        //     'as' => 'addneighborhood',
        //     'title' => 'اضافة الحي'
        // ]);

        // Route::post('/update-neighborhood', [
        //     'uses' => 'neighborhoodController@update',
        //     'as' => 'updateneighborhood',
        //     'title' => 'تعديل الحي'
        // ]);

        // Route::post('/delete-neighborhood', [
        //     'uses' => 'neighborhoodController@delete',
        //     'as' => 'deleteneighborhood',
        //     'title' => 'حذف الحي'
        // ]);

        // Route::post('/delete-neighborhoods', [
        //     'uses' => 'neighborhoodController@deleteAll',
        //     'as' => 'deleteneighborhoods',
        //     'title' => 'حذف اكتر من الحي'
        // ]);

        #contact
        Route::get('/contacts', [
            'uses' => 'contactController@index',
            'as' => 'contacts',
            'title' => 'أتصل بنا',
            'icon' => '<i class="fa fa-envelope-open"></i>',
            'child' => [
                'showcontact',
                'sendcontact',
                'deletecontact',
                'deletecontacts',
            ]
        ]);

        Route::get('/show-contact/{id}', [
            'uses' => 'contactController@show',
            'as' => 'showcontact',
            'title' => 'عرض رسالة'
        ]);

        Route::post('/send-contact', [
            'uses' => 'contactController@send',
            'as' => 'sendcontact',
            'title' => 'رد على رسالة'
        ]);

        Route::post('/delete-contact', [
            'uses' => 'contactController@delete',
            'as' => 'deletecontact',
            'title' => 'حذف رسالة'
        ]);

        Route::post('/delete-contacts', [
            'uses' => 'contactController@deleteAll',
            'as' => 'deletecontacts',
            'title' => 'حذف اكتر من رسالة'
        ]);


        // ======== Reports
        Route::get('all-reports', [
            'uses' => 'ReportController@index',
            'as' => 'reports',
            'title' => 'التقارير',
            'icon' => '<i class="fa fa-flag"></i>',
            'child' => [
                'deletereports',
            ]
        ]);

        Route::get('/delete-reports', [
            'uses' => 'ReportController@delete',
            'as' => 'deletereports',
            'title' => 'حذف التقارير'
        ]);
    });
    Route::any('/logout', 'AuthController@logout')->name('logout');
});
