$(document).ready(function () {
    //upload images
    $(function () {
        var imagesPreview = function (input, placeToInsertImagePreview) {
            $('.gallery').empty();
            if (input.files) {
                var filesAmount = input.files.length;
                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();
                    reader.onload = function (event) {
                        var image = document.createElement('img');
                        image.setAttribute('src', event.target.result);
                        var body = document.createElement('div');
                        var button = document.createElement('button');
                        var input = document.createElement('input');
                        input.setAttribute('name', 'images[]');
                        input.setAttribute('type', 'hidden');
                        button.setAttribute('class', 'close');
                        button.innerHTML = '<i class="fa fa-times-circle"></i>';
                        body.appendChild(image);
                        body.appendChild(input);
                        body.appendChild(button);
                        body.setAttribute('class', 'images');
                        console.log(body);
                        $('.gallery').append(body);
                        ($($.parseHTML(body)).appendTo(placeToInsertImagePreview));
                    }
                    reader.readAsDataURL(input.files[i]);
                }
            }
        };
        $(document).on('click', '.close', function (event) {
            event.preventDefault();
            $(this).parent().remove();
        });
        $('#gallery-photo-add').on('change', function () {
            imagesPreview(this, 'div.gallery');
        });
    });
    // delete-hgozat
    $('.delete-hgozat').on('click', function () {
        $(this).parents('.delete-item').remove();
    });
    // scroll nice 
    $('nav ul li a[href*="#"]:not([href="#"])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
    // Start Navbar 

    $('.overlay').fadeOut();

    $(".mob-collaps").click(function () {
        $(this).parents('nav').find("ul.site-nav").toggleClass('nav-open');
        $('.overlay').fadeToggle();
        $(this).find("span:first-child").toggleClass('rotate');
        $(this).find("span:nth-child(2)").toggleClass('none');
        $(this).find("span:nth-child(3)").toggleClass('rotate2');
    });

    $(".overlay").click(function () {
        $("nav ul.site-nav").removeClass('nav-open');
        $(this).fadeOut();
        $(".mob-collaps span:first-child").removeClass('rotate');
        $(".mob-collaps span:nth-child(2)").removeClass('none');
        $(".mob-collaps span:nth-child(3)").removeClass('rotate2');
    });
    //dropdown
    $(".dropdown-click").on("click", function (e) {
        e.stopPropagation();
        $(this).parent().toggleClass("active");
        $(this).find(".dropdown-content").toggleClass("open");
        $(this).find(".dropdown").removeClass("open");
    });
    $("body").on("click", function () {
        $(".dropdown-click").parent().removeClass("active");
        $(".dropdown-content").removeClass("open");
    });
    $(".sub-dropdown").on("click", function (e) {
        e.stopPropagation();
        $(this).find(".dropdown").toggleClass("open").parents("li").siblings().find(".dropdown").removeClass("open");
    })
    $(".default").on("click", function (e) {
        e.preventDefault();
    })
    $("body").on("click", function () {
        $(".dropdown").removeClass("open");
        $(".sub-dropdown").find(".left-icon").removeClass("fa-times");
    });

    //boooking form
    $('form .tainer input').on('change', function () {
        $('.remove-check').slideDown();
    });
    // $('.clock').on('click', function () {
    //     if ($('#date').val() != null && $('#date').val() != '' && $('#date').val() != undefined) {
    //         $('.time-content').slideDown();
    //     } else {
    //         $('.clock').notify(
    //             "Choose date first - اختار التاريخ اولا", {
    //                 position: "bottom"
    //             }
    //         );
    //     }
    // });
    // Start Testamonial
    $('#owl-clients').owlCarousel({
        center: true,
        items: 3,
        loop: true,
        margin: 20,
        navText: ['<i class="fas fa-arrow-left"></i>', '<i class="fas fa-arrow-right"></i>'],
        responsive: {
            320: {
                items: 1
            },

            768: {
                items: 3
            }
        }

    });
    //about slider
    $("#owl-main").owlCarousel({
        navigation: false,
        slideSpeed: 300,
        paginationSpeed: 400,
        dots: true,
        items: 1,
        loop: true,
        autoplay: true,
        nav: false,
        autoplayTimeout: 3000,
        itemsDesktop: !1,
        itemsDesktopSmall: !1,
        itemsTablet: !1,
        itemsMobile: !1
    });
    //datepicker
    $(".datepicker1").datepicker({
        dateFormat: "yy-mm-dd",
        changeMonth: true,
        changeYear: true,
        yearRange: "1950:new Date()"
    });
    var eventDates = {};
    var eventHolidays = {};
    var holidays = [
        ["2019/11/28"],
        ["2019/11/29"],
        ["2019/11/16"]
    ];
    var dates = [
        ["2019/11/30"],
        ["2019/11/25"],
        ["2018/08/16"],
        ["2018/06/25"],
    ];

    for (var i = 0; i < dates.length; i++) {
        eventDates[new Date(dates[i])] = new Date(dates[i]);
    }
    for (var i = 0; i < holidays.length; i++) {
        eventHolidays[new Date(holidays[i])] = new Date(holidays[i]);
    }

    // $(".datepicker").datepicker({
    //     dateFormat: "yy-mm-dd",
    //     minDate: 0,
    //     beforeShowDay: function (date) {

    //         var highlight = eventDates[date];
    //         var value1 = eventHolidays[date];

    //         if (value1) {

    //             return [true, "ui-state-disabled", 'اجازة'];

    //         } else if (highlight) {

    //             return [true, "events ui-state-disabled", 'محجوز'];

    //         } else {

    //             return [true, '', ''];

    //         };
    //     }
    // });
});
//loader
$(window).on('load', function () {
    $("#preloader_6").fadeOut(2000, function () {
        $("body").fadeIn(1000)
    })
})