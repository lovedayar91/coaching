-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 23, 2019 at 06:01 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `happy`
--

-- --------------------------------------------------------

--
-- Table structure for table `app_settings`
--

CREATE TABLE `app_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `app_settings`
--

INSERT INTO `app_settings` (`id`, `key`, `value`, `created_at`, `updated_at`) VALUES
(1, 'site_name', 'Happy', '2019-03-17 17:12:52', '2019-03-28 23:25:51'),
(2, 'address', '', NULL, '2019-04-18 14:20:29'),
(3, 'phone', '966546589050', NULL, '2019-03-19 00:54:35'),
(4, 'email', 'happy@info.com', NULL, '2019-03-19 00:54:35'),
(5, 'about_us_ar', '<p>&nbsp; &nbsp; يعتبر التطبيق واحد من التطبيقات الرائدة في مجال الخدمات المقدمة من خلال الطلب من اكثر من مقدم خدمة نعمل في هذا التطبيق علي تلبية&nbsp;إحتياجات&nbsp;العملاء بمختلف شرائحهم بحيث نقدم لعملائنا ما يتوافق مع تطلعاتهم و يحقق أهدافهم من خلال مجموعة من المحترفين من مقدمين الخدمات و موظفين مختصين لخدمة العملاء ليلا نهارا.</p>', NULL, '2019-04-18 14:40:33'),
(6, 'description', 'خدمات , services', NULL, '2019-04-18 14:37:59'),
(7, 'key_words', 'خدمات , services', NULL, '2019-04-18 14:37:59'),
(8, 'lat', '21.485811', NULL, '2019-04-18 14:17:57'),
(9, 'lng', '39.19250480000005', NULL, '2019-04-18 14:17:57'),
(10, 'about_us_en', '<p>&nbsp; &nbsp; يعتبر التطبيق واحد من التطبيقات الرائدة في مجال الخدمات المقدمة من خلال الطلب من اكثر من مقدم خدمة نعمل في هذا التطبيق علي تلبية&nbsp;إحتياجات&nbsp;العملاء بمختلف شرائحهم بحيث نقدم لعملائنا ما يتوافق مع تطلعاتهم و يحقق أهدافهم من خلال مجموعة من المحترفين من مقدمين الخدمات و موظفين مختصين لخدمة العملاء ليلا نهارا.</p>', NULL, '2019-04-18 14:40:33'),
(11, 'site_logo', 'logo.png', '2019-03-28 23:25:26', '2019-03-28 23:25:26'),
(12, 'who_us_ar', '<p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.<br />\r\nإذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع.<br />\r\nومن هنا وجب على المصمم أن يضع نصوصا مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور مولد النص العربى أن يوفر على المصمم عناء البحث عن نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم فيظهر بشكل لا يليق.<br />\r\nهذا النص يمكن أن يتم تركيبه على أي تصميم دون مشكلة فلن يبدو وكأنه نص منسوخ، غير منظم، غير منسق، أو حتى غير مفهوم. لأنه مازال نصاً بديلاً ومؤقتاً.</p>', NULL, '2019-04-18 14:40:33'),
(13, 'who_us_en', '<p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.<br />\r\nإذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع.<br />\r\nومن هنا وجب على المصمم أن يضع نصوصا مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور مولد النص العربى أن يوفر على المصمم عناء البحث عن نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم فيظهر بشكل لا يليق.<br />\r\nهذا النص يمكن أن يتم تركيبه على أي تصميم دون مشكلة فلن يبدو وكأنه نص منسوخ، غير منظم، غير منسق، أو حتى غير مفهوم. لأنه مازال نصاً بديلاً ومؤقتاً.</p>', NULL, '2019-04-18 14:40:33'),
(14, 'why_us_ar', '<p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.<br />\r\nإذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع.<br />\r\nومن هنا وجب على المصمم أن يضع نصوصا مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور مولد النص العربى أن يوفر على المصمم عناء البحث عن نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم فيظهر بشكل لا يليق.<br />\r\nهذا النص يمكن أن يتم تركيبه على أي تصميم دون مشكلة فلن يبدو وكأنه نص منسوخ، غير منظم، غير منسق، أو حتى غير مفهوم. لأنه مازال نصاً بديلاً ومؤقتاً.</p>', NULL, '2019-04-18 14:40:33'),
(15, 'why_us_en', '<p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.<br />\r\nإذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع.<br />\r\nومن هنا وجب على المصمم أن يضع نصوصا مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور مولد النص العربى أن يوفر على المصمم عناء البحث عن نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم فيظهر بشكل لا يليق.<br />\r\nهذا النص يمكن أن يتم تركيبه على أي تصميم دون مشكلة فلن يبدو وكأنه نص منسوخ، غير منظم، غير منسق، أو حتى غير مفهوم. لأنه مازال نصاً بديلاً ومؤقتاً.</p>', NULL, '2019-04-18 14:40:33'),
(16, 'condition_ar', '<p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.<br />\r\nإذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع.<br />\r\nومن هنا وجب على المصمم أن يضع نصوصا مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور مولد النص العربى أن يوفر على المصمم عناء البحث عن نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم فيظهر بشكل لا يليق.<br />\r\nهذا النص يمكن أن يتم تركيبه على أي تصميم دون مشكلة فلن يبدو وكأنه نص منسوخ، غير منظم، غير منسق، أو حتى غير مفهوم. لأنه مازال نصاً بديلاً ومؤقتاً.</p>', NULL, '2019-04-18 14:40:33'),
(17, 'condition_en', '<p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.<br />\r\nإذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع.<br />\r\nومن هنا وجب على المصمم أن يضع نصوصا مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور مولد النص العربى أن يوفر على المصمم عناء البحث عن نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم فيظهر بشكل لا يليق.<br />\r\nهذا النص يمكن أن يتم تركيبه على أي تصميم دون مشكلة فلن يبدو وكأنه نص منسوخ، غير منظم، غير منسق، أو حتى غير مفهوم. لأنه مازال نصاً بديلاً ومؤقتاً.</p>', NULL, '2019-04-18 14:40:33'),
(18, 'max_service', '2', NULL, '2019-04-18 14:17:57'),
(19, 'profit_percent', '10', NULL, '2019-04-18 14:17:57'),
(20, 'client_android_url', 'http://www.android.com/client', NULL, '2019-04-21 07:07:36'),
(21, 'provider_android_url', 'http://www.android.com/provider', NULL, '2019-04-21 07:07:36'),
(22, 'client_ios_url', 'http://www.ios.com/client', NULL, '2019-04-21 07:07:36'),
(23, 'provider_ios_url', 'http://www.ios.com/provider', NULL, '2019-04-21 07:07:36');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(11) NOT NULL,
  `title_ar` varchar(255) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `country_id` int(11) DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `title_ar`, `title_en`, `country_id`, `created_at`, `updated_at`) VALUES
(1, 'الرياض', 'reyad', 1, '2019-04-17 12:03:21', '2019-04-17 10:03:21'),
(2, 'جدة', 'geda', 1, '2019-04-17 12:03:50', '2019-04-17 10:03:50'),
(3, 'القاهرة', 'cairo', 2, '2019-04-17 12:03:38', '2019-04-17 10:03:38');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `message` text,
  `file` varchar(255) DEFAULT NULL,
  `seen` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `phone`, `title`, `message`, `file`, `seen`, `created_at`, `updated_at`) VALUES
(1, 'test name', 'test@info.com', '966123456789', 'test title', 'test the first msg from DB to adminPanel and i hope its be secure and success !!', NULL, 1, '2019-03-20 22:00:00', '2019-03-21 14:23:50'),
(2, 'abdelrahman101', 'ar@info.com', '96698765432101', NULL, 'test send msg from post man to admin panel', NULL, 1, '2019-04-21 11:16:01', '2019-04-21 11:17:10');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `title_ar` varchar(255) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `flag` varchar(255) DEFAULT NULL,
  `code` varchar(100) DEFAULT '966',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `title_ar`, `title_en`, `flag`, `code`, `created_at`, `updated_at`) VALUES
(1, 'السعودية', 'saudi arabia', NULL, '966', '2019-03-20 16:50:32', '2019-04-17 10:00:44'),
(2, 'مصر', 'Egypt', NULL, '02', '2019-03-20 17:16:39', '2019-04-17 10:00:23');

-- --------------------------------------------------------

--
-- Table structure for table `favourites`
--

CREATE TABLE `favourites` (
  `id` int(11) NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_06_26_110013_create_roles_table', 1),
(4, '2018_06_26_110120_create_permissions_table', 1),
(5, '2018_07_01_104552_create_reports_table', 1),
(6, '2018_07_01_123905_create_app_seetings_table', 1),
(7, '2018_07_02_074616_create_socials_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `neighborhoods`
--

CREATE TABLE `neighborhoods` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `neighborhoods`
--

INSERT INTO `neighborhoods` (`id`, `title`, `country_id`, `city_id`, `created_at`, `updated_at`) VALUES
(1, 'حي العليا', 1, 1, '2019-03-20 18:24:58', '2019-03-20 18:24:58'),
(2, 'الشرق', 1, 1, '2019-03-20 18:25:17', '2019-03-20 18:25:17'),
(3, 'النور', 1, 1, '2019-03-20 18:25:28', '2019-03-20 18:25:28'),
(4, 'الروابي', 1, 2, '2019-03-20 18:25:53', '2019-03-20 18:25:53'),
(5, 'حي العضيان', 1, 2, '2019-03-20 18:26:10', '2019-03-20 18:26:10'),
(6, 'جاردن سيتي', 2, 3, '2019-03-20 18:27:03', '2019-03-20 18:27:03');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `from_id` int(10) UNSIGNED DEFAULT NULL,
  `to_id` int(10) UNSIGNED DEFAULT NULL,
  `message_ar` text,
  `message_en` text,
  `order_id` int(11) DEFAULT NULL,
  `seen` tinyint(1) NOT NULL DEFAULT '0',
  `confirm` tinyint(1) NOT NULL DEFAULT '0',
  `type` varchar(100) NOT NULL DEFAULT 'order',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `from_id`, `to_id`, `message_ar`, `message_en`, `order_id`, `seen`, `confirm`, `type`, `created_at`, `updated_at`) VALUES
(1, 1, 18, 'قام admin بحجز خدمة 2', 'admin has booked service updated 2', 4, 0, 0, 'order', '2019-04-23 09:51:58', '2019-04-23 09:51:58'),
(2, 18, NULL, 'طلب اضافة خدمة جديدة فوق العدد المسموح به', 'Request for store new order', NULL, 0, 0, 'admin', '2019-04-23 11:44:24', '2019-04-23 11:44:24'),
(3, 1, 18, 'قام admin بحجز خدمة 2', 'admin has booked service updated 2', 5, 0, 0, 'order', '2019-04-23 12:56:06', '2019-04-23 12:56:06'),
(4, 1, 18, 'قام admin بحجز خدمة 2', 'admin has booked service updated 2', 6, 0, 0, 'order', '2019-04-23 13:44:47', '2019-04-23 13:44:47');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `service_id` int(11) DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `provider_id` int(10) UNSIGNED DEFAULT NULL,
  `price` varchar(255) DEFAULT '0',
  `lat` varchar(100) DEFAULT NULL,
  `lng` varchar(100) DEFAULT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `days_count` varchar(255) DEFAULT NULL,
  `payment_method` tinyint(4) NOT NULL DEFAULT '0',
  `status` int(1) DEFAULT '0' COMMENT '0=new , 1=agree , 2=refused , 3=deletedByClient , 4=deletedByProvider , 5=finish',
  `cancel` tinyint(1) NOT NULL DEFAULT '0',
  `user_seen` tinyint(1) NOT NULL DEFAULT '1',
  `provider_seen` tinyint(1) NOT NULL DEFAULT '1',
  `admin_seen` tinyint(1) NOT NULL DEFAULT '1',
  `user_finish` tinyint(1) DEFAULT '0',
  `provider_finish` tinyint(1) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `service_id`, `user_id`, `provider_id`, `price`, `lat`, `lng`, `start_date`, `end_date`, `days_count`, `payment_method`, `status`, `cancel`, `user_seen`, `provider_seen`, `admin_seen`, `user_finish`, `provider_finish`, `created_at`, `updated_at`) VALUES
(1, 3, 1, 18, '240', NULL, NULL, '2019-04-30 22:00:00', '2019-05-02 22:00:00', NULL, 0, 0, 0, 1, 1, 1, 0, 0, '2019-04-22 13:36:25', '2019-04-22 13:36:25'),
(2, 3, 1, 18, '40', NULL, NULL, '2019-05-04 22:00:00', '2019-05-05 22:00:00', NULL, 0, 0, 0, 1, 1, 1, 0, 0, '2019-04-22 13:41:34', '2019-04-22 13:41:34'),
(3, 3, 1, 18, '40', NULL, NULL, '2019-05-06 22:00:00', '2019-05-08 22:00:00', NULL, 0, 1, 0, 1, 1, 1, 0, 0, '2019-04-22 13:42:57', '2019-04-23 08:52:39'),
(4, 3, 1, 18, '200', NULL, NULL, '2019-05-15 22:00:00', '2019-05-16 22:00:00', NULL, 0, 1, 0, 1, 1, 1, 0, 0, '2019-04-23 09:51:58', '2019-04-23 09:56:37'),
(5, 3, 1, 18, '240', NULL, NULL, '2019-04-30 22:00:00', '2019-05-02 22:00:00', NULL, 0, 0, 0, 1, 1, 1, 0, 0, '2019-04-23 12:56:06', '2019-04-23 12:56:06'),
(6, 3, 1, 18, '240', NULL, NULL, '2019-04-30 22:00:00', '2019-05-02 22:00:00', NULL, 0, 0, 0, 1, 1, 1, 0, 0, '2019-04-23 13:44:47', '2019-04-23 13:44:47');

-- --------------------------------------------------------

--
-- Table structure for table `order_options`
--

CREATE TABLE `order_options` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `permissions` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `permissions`, `role_id`, `created_at`, `updated_at`) VALUES
(722, 'dashboard', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(723, 'permissionslist', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(724, 'addpermissionspage', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(725, 'addpermission', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(726, 'editpermissionpage', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(727, 'updatepermission', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(728, 'deletepermission', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(729, 'admins', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(730, 'addadmin', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(731, 'updateadmin', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(732, 'deleteadmin', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(733, 'deleteadmins', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(734, 'users', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(735, 'adduser', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(736, 'updateuser', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(737, 'deleteuser', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(738, 'deleteusers', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(739, 'providers', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(740, 'addprovider', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(741, 'updateprovider', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(742, 'deleteprovider', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(743, 'deleteproviders', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(744, 'settings', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(745, 'sitesetting', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(746, 'add-social', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(747, 'update-social', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(748, 'delete-social', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(749, 'app_links', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(750, 'pages', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(751, 'SEO', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(752, 'sections', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(753, 'addsection', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(754, 'updatesection', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(755, 'deletesection', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(756, 'deletesections', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(757, 'showservices', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(758, 'countries', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(759, 'addcountry', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(760, 'updatecountry', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(761, 'deletecountry', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(762, 'deletecountries', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(763, 'cities', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(764, 'addcity', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(765, 'updatecity', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(766, 'deletecity', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(767, 'deletecities', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(768, 'orders', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(769, 'neworder', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(770, 'refusedorder', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(771, 'finishorder', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(772, 'deleteorder', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(773, 'contacts', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(774, 'showcontact', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(775, 'sendcontact', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(776, 'deletecontact', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(777, 'deletecontacts', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(778, 'reports', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(779, 'deletereports', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13');

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE `reports` (
  `id` int(10) UNSIGNED NOT NULL,
  `event` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `supervisor` int(11) NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `area` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reports`
--

INSERT INTO `reports` (`id`, `event`, `supervisor`, `ip`, `country`, `city`, `area`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'قام admin بتحديث روابط التطبيق', 1, '::1', '', '', '', 1, '2019-04-21 07:07:37', '2019-04-21 07:07:37'),
(2, 'قام admin بالسماح لعضو بأضافة خدمات', 1, '::1', '', '', '', 1, '2019-04-22 08:01:22', '2019-04-22 08:01:22'),
(3, 'قام admin بمنع مزود بأضافة خدمات', 1, '::1', '', '', '', 1, '2019-04-22 08:01:22', '2019-04-22 08:01:22');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role`, `created_at`, `updated_at`) VALUES
(1, 'مدير عام', '2019-03-17 17:12:51', '2019-03-17 17:12:51');

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `id` int(11) NOT NULL,
  `title_ar` varchar(255) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`id`, `title_ar`, `title_en`, `image`, `active`, `created_at`, `updated_at`) VALUES
(3, 'ألعاب', 'Games', '/public/images/section/18-04-1915555979731094596823.gif', 1, '2019-04-18 12:32:53', '2019-04-18 12:32:53');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `title_ar` varchar(255) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `desc_en` text,
  `desc_ar` text,
  `price` double DEFAULT '0',
  `payment_method` tinyint(1) NOT NULL DEFAULT '0',
  `rate` varchar(100) NOT NULL DEFAULT '0',
  `quantity` varchar(100) DEFAULT '0',
  `quantity_in_stock` varchar(255) DEFAULT NULL,
  `down_payment` varchar(100) DEFAULT '0',
  `address_ar` varchar(255) DEFAULT NULL,
  `address_en` varchar(255) DEFAULT NULL,
  `lat` varchar(100) DEFAULT NULL,
  `lng` varchar(100) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `main_phone` varchar(255) DEFAULT NULL,
  `rent_count` int(11) NOT NULL DEFAULT '0',
  `country_id` int(11) DEFAULT NULL,
  `user_id` int(11) UNSIGNED DEFAULT NULL,
  `section_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `title_ar`, `title_en`, `desc_en`, `desc_ar`, `price`, `payment_method`, `rate`, `quantity`, `quantity_in_stock`, `down_payment`, `address_ar`, `address_en`, `lat`, `lng`, `phone`, `main_phone`, `rent_count`, `country_id`, `user_id`, `section_id`, `city_id`, `created_at`, `updated_at`) VALUES
(1, 'خدمة 1', 'service 1', 'test desc by english', 'تجربة التفاصيل بالعربية', 100, 0, '0', '5', NULL, '0', NULL, NULL, '24.36522', '26.32541', '9665623521005', '5623521005', 0, 1, 18, 3, NULL, '2019-04-21 14:01:25', '2019-04-21 14:01:25'),
(3, 'خدمة 2', 'service updated 2', 'test update desc by english', 'تجربة التفاصيل بالعربية', 200, 0, '0', '3', NULL, '0', NULL, NULL, '24.36522', '26.32541', '9665623521008', '5623521008', 0, 1, 18, 3, NULL, '2019-04-22 08:43:41', '2019-04-22 09:30:44');

-- --------------------------------------------------------

--
-- Table structure for table `service_images`
--

CREATE TABLE `service_images` (
  `id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `service_images`
--

INSERT INTO `service_images` (`id`, `image`, `service_id`, `created_at`, `updated_at`) VALUES
(1, '/public/images/services/21-04-191555862485462549117.png', 1, '2019-04-21 14:01:25', '2019-04-21 14:01:25'),
(2, '/public/images/services/21-04-1915558624851556719374.png', 1, '2019-04-21 14:01:25', '2019-04-21 14:01:25'),
(8, '/public/images/services/22-04-191555929821858090280.png', 3, '2019-04-22 08:43:41', '2019-04-22 08:43:41'),
(9, '/public/images/services/22-04-191555929821831744056.png', 3, '2019-04-22 08:43:41', '2019-04-22 08:43:41'),
(10, '/public/images/services/22-04-1915559298211910556054.png', 3, '2019-04-22 08:43:41', '2019-04-22 08:43:41'),
(11, '/public/images/services/22-04-1915559326441306822390.png', 3, '2019-04-22 09:30:44', '2019-04-22 09:30:44'),
(12, '/public/images/services/22-04-191555932644244872113.png', 3, '2019-04-22 09:30:44', '2019-04-22 09:30:44'),
(13, '/public/images/services/22-04-191555932644837043253.png', 3, '2019-04-22 09:30:44', '2019-04-22 09:30:44');

-- --------------------------------------------------------

--
-- Table structure for table `service_options`
--

CREATE TABLE `service_options` (
  `id` int(11) NOT NULL,
  `title_ar` varchar(255) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `service_prices`
--

CREATE TABLE `service_prices` (
  `id` int(11) NOT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `price` varchar(255) NOT NULL DEFAULT '0',
  `service_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `service_prices`
--

INSERT INTO `service_prices` (`id`, `start_date`, `end_date`, `price`, `service_id`, `created_at`, `updated_at`) VALUES
(13, '2019-05-01 22:00:00', '2019-05-04 22:00:00', '20', 3, '2019-04-22 09:30:44', '2019-04-22 09:30:44'),
(14, '2019-05-10 22:00:00', '2019-05-14 22:00:00', '30', 3, '2019-04-22 09:30:44', '2019-04-22 09:30:44');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `image`, `active`, `created_at`, `updated_at`) VALUES
(1, '/public/images/Slider/30-03-191553949278796835127.png', 1, '2019-03-30 08:35:00', '2019-03-30 10:34:38'),
(2, '/public/images/slider/30-03-1915539492471627446225.png', 1, '2019-03-30 10:34:07', '2019-03-30 10:34:07');

-- --------------------------------------------------------

--
-- Table structure for table `socials`
--

CREATE TABLE `socials` (
  `id` int(10) UNSIGNED NOT NULL,
  `site_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `socials`
--

INSERT INTO `socials` (`id`, `site_name`, `url`, `icon`, `created_at`, `updated_at`) VALUES
(2, 'facebook', 'http://www.facebook.com', 'facebook', '2019-03-17 18:24:15', '2019-03-17 18:24:15'),
(3, 'twitter', 'http://www.twitter.com', 'twitter', '2019-03-17 18:24:54', '2019-03-17 18:24:54'),
(4, 'instagram', 'http://www.instagram.com', 'instagram', '2019-03-17 18:25:17', '2019-03-17 18:26:47'),
(5, 'whatsapp', 'http://www.whatsapp.com', 'whatsapp', '2019-04-18 12:51:24', '2019-04-18 12:51:24');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `main_phone` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default.png',
  `provider` tinyint(1) NOT NULL DEFAULT '0',
  `identity_num` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `checked` int(11) NOT NULL DEFAULT '0',
  `add_service` tinyint(1) NOT NULL DEFAULT '1',
  `role` int(11) NOT NULL DEFAULT '0',
  `lat` decimal(16,14) DEFAULT NULL,
  `lng` decimal(16,14) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'android',
  `lang` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ar',
  `total_sales` double NOT NULL DEFAULT '0',
  `pay_done` double NOT NULL DEFAULT '0',
  `total_profit` double NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `first_name`, `last_name`, `email`, `password`, `phone`, `main_phone`, `code`, `avatar`, `provider`, `identity_num`, `device_id`, `active`, `checked`, `add_service`, `role`, `lat`, `lng`, `country_id`, `address`, `device_type`, `lang`, `total_sales`, `pay_done`, `total_profit`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', NULL, NULL, 'happy@info.com', '$2y$10$nQ3urUipUw7pT6LFBhd1peWg4XIygCMY.ihro7CnJ/wVAZZlY9O92', '96612345678', NULL, '4347', 'default.png', 0, NULL, '11111', 1, 1, 1, 1, NULL, NULL, NULL, NULL, 'android', 'ar', 0, 0, 0, 'L3jUCOuLBnbdemWmJBvNLe80abpeqG1lO9CkBoQYqVt67wNgylwXbmrfqUFe', '2019-03-16 22:00:00', '2019-03-30 06:44:51'),
(8, 'no3man mahmmoud', 'no3man', 'mahmmoud', 'no3man@info.com', '$2y$10$ZMSNFN6HWLbGalGh/qFILu1meaBJwb7l/iJqhKSnkpqoTG2D7kKwC', '966546589050', '546589050', NULL, '5cb87f9c14d0b-1555595164-oIGAE4iLnF.png', 1, NULL, NULL, 0, 1, 1, 0, NULL, NULL, 1, NULL, 'android', 'ar', 0, 0, 0, NULL, '2019-04-18 11:46:04', '2019-04-18 12:35:13'),
(9, 'اوامر الشبكة', 'اوامر', 'الشبكة', 'awamr@info.com', '$2y$10$e3A3V6dF.grlBhRrGTZWEuyTkzhTVCZ7RqQm86okCxQkU9EFRyapS', '966541867992', '541867992', NULL, '5cb885a53d475-1555596709-dYyLGkJ3eY.png', 0, NULL, NULL, 0, 1, 0, 0, NULL, NULL, 1, NULL, 'android', 'ar', 0, 0, 0, NULL, '2019-04-18 12:11:49', '2019-04-18 12:11:49'),
(10, 'your first name your last name', 'your first name', 'your last name', NULL, '$2y$10$ygimqdGqA3ilw6qb.f.tretoYnDHaZnWUly21SyNk1EDmLN2Un7Wa', '9661111156789', '1111156789', '28472', 'default.png', 0, NULL, '123456', 0, 0, 0, 0, NULL, NULL, 1, NULL, 'ios', 'ar', 0, 0, 0, NULL, '2019-04-21 07:42:06', '2019-04-21 07:42:06'),
(12, 'your first name your last name', 'your first name', 'your last name', NULL, '$2y$10$F/yUvK0L2cLupFQWHk.Lg.R6/zCWHHVaO3R/NkKVrH5GDjudkVJ52', '96695126210', '95126210', '77672', 'default.png', 0, NULL, '123456', 0, 0, 0, 0, NULL, NULL, 1, NULL, 'ios', 'ar', 0, 0, 0, NULL, '2019-04-21 07:45:12', '2019-04-21 07:45:12'),
(13, 'your first name your last name', 'your first name', 'your last name', NULL, '$2y$10$JqpHPyxoRd7zaT1nf4pUxuoXOIv0yjRwgrgHDQTuLtNt759QRsMYy', '966951262100', '951262100', '73507', 'default.png', 0, NULL, '123456', 0, 0, 0, 0, NULL, NULL, 1, NULL, 'ios', 'ar', 0, 0, 0, NULL, '2019-04-21 07:46:02', '2019-04-21 07:46:02'),
(14, 'abdelrahman mohammed', 'abdelrahman', 'mohammed', NULL, '$2y$10$gtQDhBN2sHeMBtffzWju/.J8F6Z1XvQ46A/m.QlpYDE0oIdcm4mS6', '9669512621000', '9512621000', '23176', 'default.png', 0, NULL, '123456', 0, 0, 0, 0, NULL, NULL, 1, NULL, 'ios', 'ar', 0, 0, 0, NULL, '2019-04-21 08:04:37', '2019-04-21 08:04:37'),
(15, 'abdelrahman mohammed', 'abdelrahman', 'mohammed', NULL, '$2y$10$kvx3x.Jppec3gr.BHBmuzOhcIW80J.HRPy4pb4lEb51UMF3.Papo2', '9669512621001', '9512621001', '93394', 'default.png', 0, NULL, '123456', 0, 0, 0, 0, NULL, NULL, 1, NULL, 'ios', 'ar', 0, 0, 0, NULL, '2019-04-21 08:05:41', '2019-04-21 08:05:41'),
(16, 'abdelrahman mohammed', 'abdelrahman', 'mohammed', NULL, '$2y$10$xxofxtgaSCNmlsHY8p0sA.xUms015QC5i0bgp2wSYMoZgJk/AMeSe', '9669512621002', '9512621002', '25809', 'default.png', 0, NULL, '123456', 0, 0, 0, 0, NULL, NULL, 1, NULL, 'ios', 'ar', 0, 0, 0, NULL, '2019-04-21 08:11:50', '2019-04-21 08:11:50'),
(17, 'abdelrahman showqy', 'abdelrahman', 'showqy', 'ar@user.com', '$2y$10$C4p8G5gUSwpSi1l5WdJ4leZGw3uJ.p4/AGT6gnCPuNo8PWtMyvu72', '96654126320001', '54126320001', '', 'default.png', 0, NULL, '11111', 0, 1, 0, 0, NULL, NULL, 1, 'saudia', 'ios', 'en', 0, 0, 0, NULL, '2019-04-21 08:12:58', '2019-04-21 08:44:21'),
(18, 'abdelrahman mohammed', 'abdelrahman', 'mohammed', NULL, '$2y$10$OCT.DynHBAbJQccWuMt4.OtU1/kcXBJUcsjQ6j9XWnXCdfIsoCzH2', '9669512621005', '9512621005', '97508', 'default.png', 1, NULL, '123456', 0, 1, 0, 0, NULL, NULL, 1, NULL, 'ios', 'ar', 0, 0, 0, NULL, '2019-04-21 13:56:22', '2019-04-22 08:01:22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `app_settings`
--
ALTER TABLE `app_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `country_id` (`country_id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favourites`
--
ALTER TABLE `favourites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `service_id` (`service_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `neighborhoods`
--
ALTER TABLE `neighborhoods`
  ADD PRIMARY KEY (`id`),
  ADD KEY `city_id` (`city_id`),
  ADD KEY `country_id` (`country_id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `from_id` (`from_id`),
  ADD KEY `to_id` (`to_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `provider_id` (`provider_id`),
  ADD KEY `service_id` (`service_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `order_options`
--
ALTER TABLE `order_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `option_id` (`option_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reports_user_id_foreign` (`user_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `city_id` (`city_id`),
  ADD KEY `section_id` (`section_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `country_id` (`country_id`);

--
-- Indexes for table `service_images`
--
ALTER TABLE `service_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_id` (`service_id`);

--
-- Indexes for table `service_options`
--
ALTER TABLE `service_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_id` (`service_id`);

--
-- Indexes for table `service_prices`
--
ALTER TABLE `service_prices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_id` (`service_id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `socials`
--
ALTER TABLE `socials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `country_id` (`country_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `app_settings`
--
ALTER TABLE `app_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `favourites`
--
ALTER TABLE `favourites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `neighborhoods`
--
ALTER TABLE `neighborhoods`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `order_options`
--
ALTER TABLE `order_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=780;

--
-- AUTO_INCREMENT for table `reports`
--
ALTER TABLE `reports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `service_images`
--
ALTER TABLE `service_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `service_options`
--
ALTER TABLE `service_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `service_prices`
--
ALTER TABLE `service_prices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `socials`
--
ALTER TABLE `socials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cities`
--
ALTER TABLE `cities`
  ADD CONSTRAINT `cities_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `favourites`
--
ALTER TABLE `favourites`
  ADD CONSTRAINT `favourites_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `favourites_ibfk_2` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `neighborhoods`
--
ALTER TABLE `neighborhoods`
  ADD CONSTRAINT `neighborhoods_ibfk_1` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `neighborhoods_ibfk_2` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notifications_ibfk_1` FOREIGN KEY (`from_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `notifications_ibfk_2` FOREIGN KEY (`to_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `notifications_ibfk_3` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`provider_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `orders_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_options`
--
ALTER TABLE `order_options`
  ADD CONSTRAINT `order_options_ibfk_1` FOREIGN KEY (`option_id`) REFERENCES `service_options` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_options_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permissions`
--
ALTER TABLE `permissions`
  ADD CONSTRAINT `permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `reports`
--
ALTER TABLE `reports`
  ADD CONSTRAINT `reports_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `services`
--
ALTER TABLE `services`
  ADD CONSTRAINT `services_ibfk_1` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `services_ibfk_2` FOREIGN KEY (`section_id`) REFERENCES `sections` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `services_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `services_ibfk_4` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `service_images`
--
ALTER TABLE `service_images`
  ADD CONSTRAINT `service_images_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `service_options`
--
ALTER TABLE `service_options`
  ADD CONSTRAINT `service_options_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `service_prices`
--
ALTER TABLE `service_prices`
  ADD CONSTRAINT `service_prices_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE SET NULL;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
