<?php

return [
    'driver' => env('FCM_PROTOCOL', 'http'),
    'log_enabled' => false,

    'http' => [
        'server_key' => env('FCM_SERVER_KEY', 'AAAA6eBju3k:APA91bEbRFidmkyuxmlXf84CHFZxpRDMvDAeeIIl0wrtTRHflsOiN4DW2d7m1ndkri6gWmfuFjAgRomjGVNvYdxzuqel607pJHioAj5p0aO8cR7xW351MFczukI9aYRG_4Njue0keAPQ'),
        'sender_id' => env('FCM_SENDER_ID', '1004492012409'),
        'server_send_url' => 'https://fcm.googleapis.com/fcm/send',
        'server_group_url' => 'https://android.googleapis.com/gcm/notification',
        'timeout' => 30.0, // in second
    ],
];
