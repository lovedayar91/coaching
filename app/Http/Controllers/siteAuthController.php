<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Mail\forgetMail;
use App\Models\Country;
use Mail;
use Illuminate\Support\Facades\Hash;

class siteAuthController extends Controller
{
    public function __construct(Request $request)
    { }

    /////////////logout
    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }

    /////////////login
    public function login()
    {
        return view('site.login');
    }

    public function post_login(Request $request)
    {
        $this->validate($request, [
            'phone'      => 'required',
            'password'   => 'required|min:6',
        ]);

        #login success
        if (Auth::attempt(['phone' => $request->phone, 'password' => $request->password])) {
            $user = User::find(Auth::id());

            /** If Login Success But User's account has block **/
            if ($user->checked == '0') {
                Auth::logout();
                Session::flash('danger', trans('api.hasBlock'));
                return redirect('contact-us');
            }

            return redirect('/');
        }

        #login faild
        Session::flash('danger', trans('site.wrongLogin'));
        return back();
    }

    ////////////////register
    public function register()
    {
        return view('site.register');
    }

    public function post_register(Request $request)
    {
        $this->validate($request, [
            'first_name'     => 'required|min:2|max:255',
            'last_name'      => 'required|min:2|max:255',
            'phone'          => 'required|min:10|max:15|unique:users,phone',
            'email'          => 'required|email|max:255|unique:users,email',
            'birthdate'      => 'required|max:255',
            'gender'         => 'required|max:255',
            'country_id'     => 'required|max:255',
            'city_id'        => 'required|max:255',
            'specialist_id'  => 'required|max:255',
            'job'            => 'required|max:255',
            'password'       => 'required|min:8|max:255|confirmed',
        ]);

        /** Save data to users **/
        $user     = new User;
        $user->first_name       = $request->first_name;
        $user->last_name        = $request->last_name;
        $user->email            = $request->email;
        $user->phone            = convert2english($request->phone);
        $user->code             = Country::whereId($request->country_id)->first()->code;
        $user->birthdate        = $request->birthdate;
        $user->gender           = $request->gender;
        $user->country_id       = $request->country_id;
        $user->city_id          = $request->city_id;
        $user->specialist_id    = $request->specialist_id;
        $user->job              = $request->job;
        $user->password         = bcrypt($request->password);
        $user->code             = rand(1111, 9999);
        $user->activation       = 1; // account not need activation
        $user->checked          = 1; // active account
        $user->save();

        Auth::login($user);

        Session::flash('success', trans('site.doneRegister'));
        return redirect('/');
    }

    ////////////////forget
    public function forget_password()
    {
        return view('site.forget_password');
    }

    public function post_forget_password(Request $request)
    {
        $this->validate($request, [
            'country_id' => 'required|min:2|max:255',
            'phone'      => 'required|min:2|max:255',
        ]);

        $user = User::where('phone', $request->phone)->where('country_id', $request->country_id)->first();
        if (!isset($user)) {
            Session::flash('danger', trans('site.wrongData'));
            return back();
        }

        /** If Login Success But User's account has block **/
        if ($user->checked == '0') {
            Auth::logout();
            Session::flash('danger', trans('api.hasBlock'));
            return redirect('contact-us');
        }

        $code = rand(1111, 9999);
        $user->code = $code;
        $user->save();

        /** send code to phone **/
        $sms_phone_format = convert_phone_to_international_format(Country::whereId($request->country_id)->first()->code, $request->phone);
        send_mobile_sms_yammah($sms_phone_format, trans('site.send_code') . $code);

        Session::flash('success', trans('site.sendCode'));
        return redirect('reset_password/' . $user->id);
    }

    /////////////////reset
    public function reset_password($id)
    {
        $user = User::findOrFail($id);
        return view('site.reset_password', ['user' => $user]);
    }

    public function post_reset_password(Request $request)
    {
        $this->validate($request, [
            'code'           => 'required',
            'password'       => 'required|min:8|max:255|confirmed',
        ]);

        #check user
        $user = User::find($request->id);
        if (!isset($user))
            return back();

        #wrong Code so back with msg
        if ($user->code != $request->code) {
            Session::flash('danger', trans('site.wrongCode'));
            return back();
        }

        #success code so reset new password
        $user->password = bcrypt($request->password);
        $user->save();

        Session::flash('success', trans('site.updatePassword'));
        return redirect('login');
    }

    public function profile()
    {
        return view('site.profile');
    }

    public function post_profile(Request $request)
    {
        $this->validate($request, [
            'first_name'     => 'required|min:2|max:255',
            'last_name'      => 'required|min:2|max:255',
            'phone'          => 'required|min:10|max:15|unique:users,phone,' . Auth::id(),
            'email'          => 'required|email|max:255|unique:users,email,' . Auth::id(),
            'birthdate'      => 'required|max:255',
            'gender'         => 'required|max:255',
            'country_id'     => 'required|max:255',
            'city_id'        => 'required|max:255',
            'specialist_id'  => 'required|max:255',
            'job'            => 'required|max:255',
        ]);

        /** Save data to users **/
        $user     = User::find(Auth::id());
        // if (!empty($request->password) && isset($request->password)) {
        //     if (empty($request->password_confirmation) || !isset($request->password_confirmation) || $request->password != $request->password_confirmation) {
        //         Session::flash('danger', trans('site.not_same_password'));
        //         return back();
        //     }
        //     $user->password         = bcrypt($request->password);
        // }
        $user->first_name       = $request->first_name;
        $user->last_name        = $request->last_name;
        $user->email            = $request->email;
        $user->phone            = convert2english($request->phone);
        $user->code             = Country::whereId($request->country_id)->first()->code;
        $user->birthdate        = $request->birthdate;
        $user->gender           = $request->gender;
        $user->country_id       = $request->country_id;
        $user->city_id          = $request->city_id;
        $user->specialist_id    = $request->specialist_id;
        $user->job              = $request->job;
        if ($request->hasFile('avatar')) $user->avatar = uploadAvatar($request->file('avatar'), 'public/images/users');
        $user->save();

        Session::flash('success', trans('site.update'));
        return back();
    }

    public function update_password()
    {
        return view('site.update_password');
    }

    public function post_update_password(Request $request)
    {
        $this->validate($request, [
            'old_password'  => 'required',
            'password'       => 'required|min:8|max:255|confirmed',
        ]);

        /** Save data to users **/
        $user     = User::find(Auth::id());
        if (Hash::check($request->old_password, $user->password)) {

            /** update Password **/
            $user->password  = bcrypt($request->password);
            $user->save();

            Auth::logout();
            Session::flash('success', trans('site.updatePassword'));
            return redirect('login');
        }

        Session::flash('danger', trans('site.wrongOldPassword'));
        return back();
    }
}
