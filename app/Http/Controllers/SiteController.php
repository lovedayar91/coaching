<?php

namespace App\Http\Controllers;

use App\Models\Coach;
use App\Models\Contact;
use App\Models\Country;
use App\Models\Section;
use App\Models\Service;
use App\Models\Favourite;
use App\Models\Link;
use App\Models\Notification;
use App\Models\Order;
use App\Models\Package;
use App\Models\Question;
use App\Models\Register_law;
use App\User;
use File;
use Illuminate\Http\Request;
use App\Models\Service_image;
use App\Models\Service_price;
use App\Models\Time_format;
use App\Models\User_package;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Session;

class SiteController extends Controller
{

    public function __construct()
    { }

    ##################### GENERAL WORK #####################

    #site language
    public function language($lang)
    {
        Session()->has('language') ? Session()->forget('language') : '';
        if ($lang == 'ar') {
            Session()->put('language', 'ar');
        } else {
            Session()->put('language', 'en');
        }
        return back();
    }

    #Home page
    public function index()
    {
        return view('site.index');
    }

    #about us page
    public function about_Us()
    {
        return view('site.about_us');
    }

    #about train page
    public function about_train()
    {
        return view('site.about_train');
    }

    #conditions page
    public function conditions()
    {
        $data   = Register_law::get();
        return view('site.conditions', compact('data'));
    }

    #links page
    public function links()
    {
        $data   = Link::get();
        return view('site.links', compact('data'));
    }

    #questions page
    public function questions()
    {
        $data   = Question::get();
        return view('site.questions', compact('data'));
    }

    #trainer page
    public function trainer($id)
    {
        $data   = Coach::with(['Images'])->whereId($id)->firstOrFail();
        return view('site.trainer', compact('data'));
    }

    #contact us page
    public function contact()
    {
        return view('site.contact_us');
    }

    #post contact message
    public function post_contact(Request $request)
    {
        $this->validate($request, [
            'name'            => 'required|min:2|max:255',
            'email'           => 'required|min:5|max:255|email',
            'phone'           => 'required|numeric|digits_between:7,20',
            'message'         => 'required',
        ]);

        /** Save Contact **/
        $contact   = new Contact;
        $contact->name    = $request->name;
        $contact->email   = $request->email;
        $contact->phone   = convert2english($request->phone);
        $contact->message = $request->message;
        $contact->save();

        Session::flash('success', trans('api.sendSuccess'));
        return back();
    }

    public function packages($id)
    {
        $data = Section::with('Packages')->whereId($id)->firstOrFail();
        return view('site.packages', ['data' => $data]);
    }

    public function packages_checkout_info($id)
    {
        $data = Package::with('Section')->whereId($id)->firstOrFail();
        return view('site.checkout_info', ['data' => $data]);
    }

    public function packages_checkout($id)
    {
        $data = Package::with('Section')->whereId($id)->firstOrFail();
        return view('site.checkout', ['data' => $data]);
    }

    public function post_packages_checkout(Request $request)
    {
        // payment

        // package data
        $package = User_package::where('package_id', $request->package_id)->where('user_id', $request->user_id)->first();
        // create new subscripe with package
        if (isset($package)) {
            $package->session_count = $package->session_count + $request->session_count;
            $package->save();
        } else User_package::create($request->all()); // Update session_count in package

        // increase session count
        $user = User::whereId(Auth::id())->first();
        $user->session_count = $user->session_count + $request->session_count;
        $user->save();

        // redirect to checkout_success page
        return redirect()->route('site_packages_checkout_success', $request->package_id);
    }

    public function packages_checkout_success($id)
    {
        $data = Package::with('Section')->whereId($id)->firstOrFail();
        return view('site.checkout_success', ['data' => $data]);
    }

    public function orders()
    {
        $current_orders    = Order::where('user_id', Auth::id())->whereDate('full_date', '>=', Carbon::now())->orderBy('full_date', 'asc')->get();
        $finish_orders     = Order::where('user_id', Auth::id())->whereDate('full_date', '<', Carbon::now())->get();
        $data = [
            'current_orders'      => $current_orders,
            'finish_orders'       => $finish_orders,
        ];

        // redirect to order page if has sessions count
        return view('site.orders', $data);
    }

    public function add_order($id)
    {
        $section            = Section::whereId($id)->firstOrFail();
        $session_count      = count_sessions($section->id);
        $data = [
            'section'       => $section,
            'session_count' => $session_count
        ];

        // redirect to packages page if has no session count
        if ($session_count == 0) return redirect()->route('site_packages', $id);

        // redirect to order page if has sessions count
        return view('site.add_order', $data);
    }

    public function post_add_order(Request $request)
    {
        $time_ids = $request->time_ids;
        foreach ($time_ids as $key => $time_id) {
            $time = Time_format::whereId($time_id)->first();
            $add = new Order;
            $add->coach_id      = $request->coach_id;
            $add->section_id    = $request->section_id;
            $add->user_id       = $request->user_id;
            $add->date          = $request->date;
            $add->time_id       = $time_id;
            $add->full_date     = Carbon::parse($request->date . $time->time);
            $add->save();
        }

        $package = User_package::where('section_id', $request->section_id)->where('user_id', $request->user_id)->first();
        $package->session_count = $request->session_count;
        $package->save();

        Session::flash('success', trans('api.save'));
        return redirect()->route('site_orders');
    }

    public function edit_order($id)
    {
        $order              = Order::whereId($id)->firstOrFail();
        $section            = Section::whereId($order->section_id)->first();
        $session_count      = count_sessions($section->id);
        $data = [
            'order'         => $order,
            'section'       => $section,
            'session_count' => $session_count
        ];

        // redirect to order page if has sessions count
        return view('site.edit_order', $data);
    }

    public function post_edit_order(Request $request)
    {
        $time = Time_format::whereId($request->time_id)->first();
        $add = Order::whereId($request->id)->firstOrFail();
        $add->coach_id      = $request->coach_id;
        $add->date          = $request->date;
        $add->time_id       = $request->time_id;
        $add->full_date     = Carbon::parse($request->date . $time->time);
        $add->save();

        Session::flash('success', trans('api.save'));
        return redirect()->route('site_orders');
    }

    public function delete_order(Request $request)
    {
        $order = Order::whereId($request->id)->firstOrFail();
        // increase session count for user
        $package = User_package::where('section_id', $order->section_id)->where('user_id', $order->user_id)->first();
        $package->session_count = $package->session_count + 1;
        $package->save();
        // delete order
        $order->delete();
        // back with msg
        return 1;
    }
}
