<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\Coach;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ajaxController extends Controller
{
    /////////////show_city
    public function show_city(Request $request)
    {
        $lang  = Session::has('language') ? Session('language') : 'ar';
        $title = 'title_' . $lang;
        $data = City::where('country_id', $request->country_id)->orderBy($title, 'asc')->get();
        return view('ajax.show_city', ['data' => $data]);
    }

    /////////////check_avaliable_date
    public function check_avaliable_date(Request $request)
    {
        $data = [
            'holidays' => get_hloidays($request->coach_id),
            'dates'    => get_dates($request->coach_id),
        ];

        return response()->json($data);
    }

    /////////////check_avaliable_date_dashboard
    public function check_avaliable_date_dashboard(Request $request)
    {
        $data = [
            'holidays' => get_hloidays($request->coach_id),
            'dates'    => get_dates_dashboard($request->coach_id),
        ];

        return response()->json($data);
    }

    /////////////check_avaliable_time
    public function check_avaliable_time(Request $request)
    {
        $data = [
            'time_ids'  => get_times($request->coach_id, $request->date),
        ];

        return response()->json($data);
    }
}
