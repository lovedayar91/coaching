<?php

namespace App\Http\Controllers\Admin;

use App\Models\AppSetting;
use App\Models\Social;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class SettingController extends Controller
{
    public function index()
    {
        $setting = AppSetting::all();
        $socials = Social::all();
        return view('dashboard.settings.index', [
            'setting'  => $setting,
            'socials'  => $socials,
        ]);
    }

    public function updateSiteInfo(Request $request)
    {
        if ($request->hasFile('site_logo')) {
            $sitesite_logo = AppSetting::where('key', 'site_logo')->first();
            $sitesite_logo->value = uploadImage($request->site_logo, 'public/images/setting');
            $sitesite_logo->save();
        }

        $sitesite_name = AppSetting::where('key', 'site_name')->first();
        $sitesite_name->value = $request->site_name;
        $sitesite_name->save();

        $siteemail = AppSetting::where('key', 'email')->first();
        $siteemail->value = $request->email;
        $siteemail->save();

        $sitephone = AppSetting::where('key', 'phone')->first();
        $sitephone->value = $request->phone;
        $sitephone->save();

        $sitemobile = AppSetting::where('key', 'mobile')->first();
        $sitemobile->value = $request->mobile;
        $sitemobile->save();

        $siteaddress_ar = AppSetting::where('key', 'address_ar')->first();
        $siteaddress_ar->value = $request->address_ar;
        $siteaddress_ar->save();

        $siteaddress_en = AppSetting::where('key', 'address_en')->first();
        $siteaddress_en->value = $request->address_en;
        $siteaddress_en->save();

        $sitewhatsapp = AppSetting::where('key', 'whatsapp')->first();
        $sitewhatsapp->value = $request->whatsapp;
        $sitewhatsapp->save();

        $sitesnapchat = AppSetting::where('key', 'snapchat')->first();
        $sitesnapchat->value = $request->snapchat;
        $sitesnapchat->save();

        $sitehours_unallow_count = AppSetting::where('key', 'hours_unallow_count')->first();
        $sitehours_unallow_count->value = $request->hours_unallow_count;
        $sitehours_unallow_count->save();

        $ip = $request->ip();

        addReport(auth()->user()->id, 'بتحديث اعدادات الموقع', $ip);
        Session::flash('success', 'تم تعديل اعدادات الموقع');
        return back();
    }

    public function intro(Request $request)
    {
        $siteabout_training_short_desc_ar = AppSetting::where('key', 'about_training_short_desc_ar')->first();
        $siteabout_training_short_desc_ar->value = $request->about_training_short_desc_ar;
        $siteabout_training_short_desc_ar->save();

        $siteabout_training_short_desc_en = AppSetting::where('key', 'about_training_short_desc_en')->first();
        $siteabout_training_short_desc_en->value = $request->about_training_short_desc_en;
        $siteabout_training_short_desc_en->save();

        $siteabout_training_desc_en = AppSetting::where('key', 'about_training_desc_en')->first();
        $siteabout_training_desc_en->value = $request->about_training_desc_en;
        $siteabout_training_desc_en->save();

        $siteabout_training_desc_ar = AppSetting::where('key', 'about_training_desc_ar')->first();
        $siteabout_training_desc_ar->value = $request->about_training_desc_ar;
        $siteabout_training_desc_ar->save();

        $siteabout_training_video = AppSetting::where('key', 'about_training_video')->first();
        $siteabout_training_video->value = $request->about_training_video;
        $siteabout_training_video->save();

        $ip = $request->ip();

        addReport(auth()->user()->id, 'بتحديث بيانات عن التدريب', $ip);
        Session::flash('success', 'تم تعديل بيانات عن التدريب');
        return back();
    }

    public function pages(Request $request)
    {
        $sitewho_us_short_desc_ar = AppSetting::where('key', 'who_us_short_desc_ar')->first();
        $sitewho_us_short_desc_ar->value = $request->who_us_short_desc_ar;
        $sitewho_us_short_desc_ar->save();

        $sitewho_us_short_desc_en = AppSetting::where('key', 'who_us_short_desc_en')->first();
        $sitewho_us_short_desc_en->value = $request->who_us_short_desc_en;
        $sitewho_us_short_desc_en->save();

        $sitewho_us_desc_en = AppSetting::where('key', 'who_us_desc_en')->first();
        $sitewho_us_desc_en->value = $request->who_us_desc_en;
        $sitewho_us_desc_en->save();

        $sitewho_us_desc_ar = AppSetting::where('key', 'who_us_desc_ar')->first();
        $sitewho_us_desc_ar->value = $request->who_us_desc_ar;
        $sitewho_us_desc_ar->save();

        if ($request->hasFile('who_us_image')) {
            $sitewho_us_image = AppSetting::where('key', 'who_us_image')->first();
            $sitewho_us_image->value = uploadImage($request->who_us_image, 'public/images/setting');
            $sitewho_us_image->save();
        }

        $sitewho_us_vission_ar = AppSetting::where('key', 'who_us_vission_ar')->first();
        $sitewho_us_vission_ar->value = $request->who_us_vission_ar;
        $sitewho_us_vission_ar->save();

        $sitewho_us_vission_en = AppSetting::where('key', 'who_us_vission_en')->first();
        $sitewho_us_vission_en->value = $request->who_us_vission_en;
        $sitewho_us_vission_en->save();

        $sitewho_us_message_en = AppSetting::where('key', 'who_us_message_en')->first();
        $sitewho_us_message_en->value = $request->who_us_message_en;
        $sitewho_us_message_en->save();

        $sitewho_us_message_ar = AppSetting::where('key', 'who_us_message_ar')->first();
        $sitewho_us_message_ar->value = $request->who_us_message_ar;
        $sitewho_us_message_ar->save();

        $ip = $request->ip();

        addReport(auth()->user()->id, 'بتحديث بيانات من نحن', $ip);
        Session::flash('success', 'تم تعديل بيانات من نحن');
        return back();
    }

    public function app_links(Request $request)
    {
        $siteclient_android_url = AppSetting::where('key', 'client_android_url')->first();
        $siteclient_android_url->value = $request->client_android_url;
        $siteclient_android_url->save();

        $siteprovider_android_url = AppSetting::where('key', 'provider_android_url')->first();
        $siteprovider_android_url->value = $request->provider_android_url;
        $siteprovider_android_url->save();


        $siteclient_ios_url = AppSetting::where('key', 'client_ios_url')->first();
        $siteclient_ios_url->value = $request->client_ios_url;
        $siteclient_ios_url->save();

        $siteprovider_ios_url = AppSetting::where('key', 'provider_ios_url')->first();
        $siteprovider_ios_url->value = $request->provider_ios_url;
        $siteprovider_ios_url->save();

        $ip = $request->ip();

        addReport(auth()->user()->id, 'بتحديث روابط التطبيق', $ip);
        Session::flash('success', 'تم تعديل روابط التطبيق');
        return back();
    }

    public function about(Request $request)
    {
        $siteAbout = AppSetting::where('key', 'about_us')->first();
        $siteAbout->value = $request->about_us;
        $siteAbout->save();

        $ip = $request->ip();

        addReport(auth()->user()->id, 'بتحديث بيانات من نحن', $ip);
        Session::flash('success', 'تم تعديل بيانات من نحن');
        return back();
    }

    public function SEO(Request $request)
    {
        $siteDescription = AppSetting::where('key', 'description')->first();
        $siteDescription->value = $request->description;
        $siteDescription->save();

        $siteKeyWords = AppSetting::where('key', 'key_words')->first();
        $siteKeyWords->value = $request->key_words;
        $siteKeyWords->save();

        $ip = $request->ip();

        addReport(auth()->user()->id, 'بتحديث بيانات ال SEO', $ip);
        Session::flash('success', 'تم تعديل بيانات ال SEO');
        return back();
    }

    public function storeSocial(Request $request)
    {
        $rules = [
            'site_name' => 'required|min:2|max:190',
            'icon'      => 'required|min:2|max:190',
            'url'       => 'required|url',
        ];
        $messages = [
            'site_name.required'    => 'اسم الموقع مطلوب',
            'site_name.min'         => 'اسم الموقع لابد ان يكون اكتر من حرفين',
            'site_name.max'         => 'اسم الموقع لابد ان يكون اقل من 190 حرف',
            'icon.required'         => 'الشعار مطلوب',
            'icon.min'              => 'الشعار لابد ان يكون اكبر من حرفين',
            'icon.max'              => 'الشعار لابد ان يكون اقل من 190 حرف',
            'url.required'          => 'الرابط مطلوب',
            'url.url'               => 'الرابط غير صحيح',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        $social = new Social();
        $social->site_name = $request->site_name;
        $social->icon = $request->icon;
        $social->url = $request->url;
        $social->save();

        $ip = $request->ip();

        addReport(auth()->user()->id, 'باضافة موقع تواصل جدبد', $ip);
        Session::flash('success', 'تم اضافة الموقع');
        return back();
    }

    public function updateSocial(Request $request)
    {
        if (isset($request->edit_name) && $request->edit_name == 'snapchat') {
            $rules = [
                'edit_name' => 'required|min:2|max:190',
                'edit_icon' => 'required|min:2|max:190',
                'edit_url'  => 'required',
                'id'        => 'required',
            ];
            $messages = [
                'edit_name.required'    => 'اسم الموقع مطلوب',
                'edit_name.min'         => 'اسم الموقع لابد ان يكون اكتر من حرفين',
                'edit_name.max'         => 'اسم الموقع لابد ان يكون اقل من 190 حرف',
                'edit_icon.required'         => 'الشعار مطلوب',
                'edit_icon.min'              => 'الشعار لابد ان يكون اكبر من حرفين',
                'edit_icon.max'              => 'الشعار لابد ان يكون اقل من 190 حرف',
                'edit_url.required'          => 'الرابط مطلوب',
            ];
        } else {
            $rules = [
                'edit_name' => 'required|min:2|max:190',
                'edit_icon' => 'required|min:2|max:190',
                'edit_url'  => 'required|url',
                'id'        => 'required',
            ];
            $messages = [
                'edit_name.required'    => 'اسم الموقع مطلوب',
                'edit_name.min'         => 'اسم الموقع لابد ان يكون اكتر من حرفين',
                'edit_name.max'         => 'اسم الموقع لابد ان يكون اقل من 190 حرف',
                'edit_icon.required'         => 'الشعار مطلوب',
                'edit_icon.min'              => 'الشعار لابد ان يكون اكبر من حرفين',
                'edit_icon.max'              => 'الشعار لابد ان يكون اقل من 190 حرف',
                'edit_url.required'          => 'الرابط مطلوب',
                'edit_url.url'               => 'الرابط غير صحيح',
            ];
        }


        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        $social = Social::findOrFail($request->id);
        $social->site_name = $request->edit_name;
        $social->icon = $request->edit_icon;
        $social->url = $request->edit_url;
        $social->save();

        $ip = $request->ip();

        addReport(auth()->user()->id, 'بتحديث موقع تواصل', $ip);
        Session::flash('success', 'تم تحديث الموقع');
        return back();
    }

    public function deleteSocial($id, Request $request)
    {
        Social::where('id', $id)->delete();

        $ip = $request->ip();
        addReport(auth()->user()->id, 'بحذف موقع تواصل', $ip);
        Session::flash('success', 'تم حذف الموقع');
        return back();
    }
}
