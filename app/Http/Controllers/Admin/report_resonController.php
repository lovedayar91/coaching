<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Report_reson;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class report_resonController extends Controller
{

    public function index()
    {
        $data       = Report_reson::get();
        $roles      = Role::latest()->get();
        return view('dashboard.report_reson.index', compact('data', 'roles'));
    }

    public function store(Request $request)
    {

        // Validation rules
        $rules = [
            'title_ar'        => 'required',
            'title_en'        => 'required',
        ];

        // Validator messages
        $messages = [
            'title_ar.required'     => 'السبب بالعربية مطلوب',
            'title_en.required'     => 'السبب بالانجليزية مطلوب',
        ];

        // Validation
        $validator = Validator::make($request->all(), $rules, $messages);

        // If failed
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        //store report_reson
        $add = new Report_reson;
        $add->title_ar = $request->title_ar;
        $add->title_en = $request->title_en;
        $add->save();

        addReport(auth()->user()->id, 'باضافة سبب  جديد', $request->ip());
        Session::flash('success', 'تم الأضافة بنجاح');
        return back();
    }

    public function update(Request $request)
    {
        // Validation rules
        $rules = [
            'title_ar'        => 'required',
            'title_en'        => 'required',
        ];

        // Validator messages
        $messages = [
            'title_ar.required'     => 'السبب بالعربية مطلوب',
            'title_en.required'     => 'السبب بالانجليزية مطلوب',
        ];

        // Validation
        $validator = Validator::make($request->all(), $rules, $messages);

        // If failed
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        //store report_reson
        $add = Report_reson::findOrFail($request->id);
        $add->title_ar = $request->title_ar;
        $add->title_en = $request->title_en;
        $add->save();

        addReport(auth()->user()->id, 'بتعديل بيانات سبب', $request->ip());
        Session::flash('success', 'تم التعديل بنجاح');
        return back();
    }

    public function delete(Request $request)
    {

        Report_reson::findOrFail($request->delete_id)->delete();
        addReport(auth()->user()->id, 'بحذف سبب', $request->ip());
        Session::flash('success', 'تم الحذف بنجاح');
        return back();
    }

    public function deleteAll(Request $request)
    {
        $requestIds = json_decode($request->data);
        foreach ($requestIds as $id) {
            $ids[] = $id->id;
        }
        if (Report_reson::whereIn('id', $ids)->delete()) {
            addReport(auth()->user()->id, 'قام بحذف العديد من الصور', $request->ip());
            Session::flash('success', 'تم الحذف بنجاح');
            return response()->json('success');
        } else {
            return response()->json('failed');
        }
    }
}
