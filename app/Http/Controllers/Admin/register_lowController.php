<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Register_law;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class register_lowController extends Controller
{

    public function index()
    {
        $data       = Register_law::get();
        $roles      = Role::latest()->get();
        return view('dashboard.register_low.index', compact('data', 'roles'));
    }

    public function store(Request $request)
    {

        // Validation rules
        $rules = [
            'title_ar'        => 'required',
            'title_en'        => 'required',
            'desc_ar'         => 'required',
            'desc_en'         => 'required',
        ];

        // Validator messages
        $messages = [
            'title_ar.required'     => 'القانون بالعربية مطلوب',
            'title_en.required'     => 'القانون بالانجليزية مطلوب',
            'desc_ar.required'      => 'التفاصيل بالعربية مطلوب',
            'desc_en.required'      => 'التفاصيل بالانجليزية مطلوب',
        ];

        // Validation
        $validator = Validator::make($request->all(), $rules, $messages);

        // If failed
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        //store register_low
        $add = new Register_law;
        $add->title_ar = $request->title_ar;
        $add->title_en = $request->title_en;
        $add->desc_ar  = $request->desc_ar;
        $add->desc_en  = $request->desc_en;
        $add->save();

        addReport(auth()->user()->id, 'باضافة قانون جديد', $request->ip());
        Session::flash('success', 'تم الأضافة بنجاح');
        return back();
    }

    public function update(Request $request)
    {
        // Validation rules
        $rules = [
            'title_ar'        => 'required',
            'title_en'        => 'required',
            'desc_ar'         => 'required',
            'desc_en'         => 'required',
        ];

        // Validator messages
        $messages = [
            'title_ar.required'     => 'القانون بالعربية مطلوب',
            'title_en.required'     => 'القانون بالانجليزية مطلوب',
            'desc_ar.required'      => 'التفاصيل بالعربية مطلوب',
            'desc_en.required'      => 'التفاصيل بالانجليزية مطلوب',
        ];

        // Validation
        $validator = Validator::make($request->all(), $rules, $messages);

        // If failed
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        //store register_low
        $add = Register_law::findOrFail($request->id);
        $add->title_ar = $request->title_ar;
        $add->title_en = $request->title_en;
        $add->desc_ar  = $request->desc_ar;
        $add->desc_en  = $request->desc_en;
        $add->save();

        addReport(auth()->user()->id, 'بتعديل بيانات قانون', $request->ip());
        Session::flash('success', 'تم التعديل بنجاح');
        return back();
    }

    public function delete(Request $request)
    {

        Register_law::findOrFail($request->delete_id)->delete();
        addReport(auth()->user()->id, 'بحذف قانون', $request->ip());
        Session::flash('success', 'تم الحذف بنجاح');
        return back();
    }

    public function deleteAll(Request $request)
    {
        $requestIds = json_decode($request->data);
        foreach ($requestIds as $id) {
            $ids[] = $id->id;
        }
        if (Register_law::whereIn('id', $ids)->delete()) {
            addReport(auth()->user()->id, 'قام بحذف العديد من القوانين', $request->ip());
            Session::flash('success', 'تم الحذف بنجاح');
            return response()->json('success');
        } else {
            return response()->json('failed');
        }
    }
}
