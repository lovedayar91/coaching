<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\UploadFile;
use App\Http\Controllers\Controller;
use App\Models\Slider;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class sliderController extends Controller
{

    public function index()
    {
        $data       = Slider::get();
        $roles      = Role::latest()->get();
        return view('dashboard.slider.index', compact('data', 'roles'));
    }

    public function store(Request $request)
    {

        // Validation rules
        $rules = [
            'desc_ar'        => 'required',
            'desc_en'        => 'required',
            'url'            => 'required',
            'image'          => 'required',
        ];

        // Validator messages
        $messages = [
            'desc_ar.required'     => 'الوصف بالعربية مطلوب',
            'desc_en.required'     => 'الوصف بالعربية مطلوب',
            'url.required'         => 'الرابط مطلوب',
            'image.required'       => 'الصورة مطلوبة',
        ];

        // Validation
        $validator = Validator::make($request->all(), $rules, $messages);

        // If failed
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        //store Slider
        $add = new Slider;
        $add->desc_ar = $request->desc_ar;
        $add->desc_en = $request->desc_en;
        $add->url     = $request->url;
        $add->image = uploadImage($request->file('image'), 'public/images/slider');
        $add->save();

        addReport(auth()->user()->id, 'باضافة صورة للعرض جديدة', $request->ip());
        Session::flash('success', 'تم الأضافة بنجاح');
        return back();
    }

    public function update(Request $request)
    {
        // Validation rules
        $rules = [
            'desc_ar'        => 'required',
            'desc_en'        => 'required',
            'url'            => 'required',
        ];

        // Validator messages
        $messages = [
            'desc_ar.required'     => 'الوصف بالعربية مطلوب',
            'desc_en.required'     => 'الوصف بالعربية مطلوب',
            'url.required'         => 'الرابط مطلوب',
        ];

        // Validation
        $validator = Validator::make($request->all(), $rules, $messages);

        // If failed
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        //store Slider
        $add = Slider::findOrFail($request->id);
        $add->desc_ar = $request->desc_ar;
        $add->desc_en = $request->desc_en;
        $add->url     = $request->url;
        if ($request->hasFile('image')) $add->image = uploadImage($request->file('image'), 'public/images/Slider');
        $add->save();

        addReport(auth()->user()->id, 'بتعديل بيانات صورة', $request->ip());
        Session::flash('success', 'تم التعديل بنجاح');
        return back();
    }

    public function delete(Request $request)
    {

        Slider::findOrFail($request->delete_id)->delete();
        addReport(auth()->user()->id, 'بحذف صورة من العرض', $request->ip());
        Session::flash('success', 'تم الحذف بنجاح');
        return back();
    }

    public function deleteAll(Request $request)
    {
        $requestIds = json_decode($request->data);
        foreach ($requestIds as $id) {
            $ids[] = $id->id;
        }
        if (Slider::whereIn('id', $ids)->delete()) {
            addReport(auth()->user()->id, 'قام بحذف العديد من الصور من العرض', $request->ip());
            Session::flash('success', 'تم الحذف بنجاح');
            return response()->json('success');
        } else {
            return response()->json('failed');
        }
    }

    public function change_activation(Request $request)
    {
        //check data
        $slider = Slider::find($request->id);
        if (!isset($slider)) return 0;
        //update data
        $slider->active = !$slider->active;
        $slider->save();
        //add report
        $slider->checked == 1 ?
            addReport(auth()->user()->id, 'بتفعيل صورة', $request->ip()) : addReport(auth()->user()->id, 'بإلغاء تفعيل صورة', $request->ip());
        return 1;
    }
}
