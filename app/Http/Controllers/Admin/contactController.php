<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\UploadFile;
use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Mail\replayMail;
use Mail;

class contactController extends Controller
{
    //all msgs
    public function index()
    {
        $data   = Contact::OrderBy('id', 'desc')->get();
        $roles  = Role::latest()->get();
        return view('dashboard.contact.index', compact('data', 'roles'));
    }

    public function notifys()
    {
        $users   = User::where('role', 0)->where('provider' , '0')->OrderBy('name_ar', 'asc')->get();
        $doctors = User::where('role', 0)->where('provider' , '1')->OrderBy('name_ar', 'asc')->get();
        $roles   = Role::latest()->get();
        return view('dashboard.contact.notifys', compact('users','doctors', 'roles'));
    }

    public function send_notifys(Request $request)
    {
        if($request->type == 'city' && $request->city_id == 'all')     $users = User::with('Devices')->where('role', 0)->get();
        elseif($request->type == 'user' && $request->to == 'all')      $users = User::with('Devices')->where('role', 0)->get();
        elseif($request->type == 'city')  $users = User::with('Devices')->where('city_id',$request->city_id)->get();
        elseif($request->to == 'doctors') $users = User::with('Devices')->where('role', 0)->where('provider' , '1')->get();
        elseif($request->to == 'users')   $users = User::with('Devices')->where('role', 0)->where('provider' , '0')->get();
        else                              $users = User::with('Devices')->whereId($request->to)->get();

        foreach ($users as $user){
            $lang = $user->lang;
            $msg = [
                'title_ar'    => 'اشعار',
                'title_en'    => 'notification',
            ];
            $data   = [];
            $data['title']      = $msg['title_'.$lang];
            $data['msg']        = $request->desc;
            $data['status']     = 'notify';
            foreach ($user->Devices as $device){
                Send_FCM_Badge($device->device_id ,$data, $device->device_type);
            }
        }

        addReport(auth()->user()->id, 'بارسال اشعارات', $request->ip());
        Session::flash('success', 'تم الارسال بنجاح');
        return back();
    }

    //show msg
    public function show($id)
    {
        $data   = Contact::findOrFail($id);
        if ($data->seen == '0') {
            $data->seen = '1';
            $data->save();
        }
        $roles  = Role::latest()->get();
        return view('dashboard.contact.show', compact('data', 'roles'));
    }

    //send msg
    public function send(Request $request)
    {
        $data   = Contact::find($request->id);
        if (!isset($data)) return 'err';
        $email      = $data->email;
        $message    = $request->message;

        Mail::to($email)->send(new replayMail($message));
        return 'success';
    }

    public function delete(Request $request)
    {
        Contact::findOrFail($request->delete_id)->delete();
        addReport(auth()->user()->id, 'بحذف أتصل بنا', $request->ip());
        Session::flash('success', 'تم الحذف بنجاح');
        return back();
    }

    public function deleteAll(Request $request)
    {
        $requestIds = json_decode($request->data);
        foreach ($requestIds as $id) {
            $ids[] = $id->id;
        }
        if (Contact::whereIn('id', $ids)->delete()) {
            addReport(auth()->user()->id, 'قام بحذف العديد من أتصل بنا', $request->ip());
            Session::flash('success', 'تم الحذف بنجاح');
            return response()->json('success');
        } else {
            return response()->json('failed');
        }
    }
}
