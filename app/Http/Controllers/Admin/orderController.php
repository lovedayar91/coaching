<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\UploadFile;
use App\Http\Controllers\Controller;
use App\Models\Section;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Models\Order;
use Carbon\Carbon;

class orderController extends Controller
{

    public function index()
    {
        $roles      = Role::latest()->get();
        return view('dashboard.order.index', compact('roles'));
    }

    public function current()
    {
        $search     = isset($_GET["search"]) ? $_GET["search"] : '';
        $data       = Order::whereDate('full_date', '>=', Carbon::now())->orderBy('full_date', 'asc')->get();
        $roles      = Role::latest()->get();
        return view('dashboard.order.current', compact('data', 'roles', 'search'));
    }

    public function today()
    {
        $search     = isset($_GET["search"]) ? $_GET["search"] : '';
        $data       = Order::whereDate('full_date', Carbon::now())->get();
        $roles      = Role::latest()->get();
        return view('dashboard.order.today', compact('data', 'roles', 'search'));
    }

    public function tomorrow()
    {
        $search     = isset($_GET["search"]) ? $_GET["search"] : '';
        $data       = Order::whereDate('full_date', Carbon::now()->addDay())->orderBy('full_date', 'asc')->get();
        $roles      = Role::latest()->get();
        return view('dashboard.order.tomorrow', compact('data', 'roles', 'search'));
    }

    public function finish()
    {
        $search     = isset($_GET["search"]) ? $_GET["search"] : '';
        $data       = Order::whereDate('full_date', '<', Carbon::now())->orderBy('full_date', 'asc')->get();
        $roles      = Role::latest()->get();
        return view('dashboard.order.finish', compact('data', 'roles', 'search'));
    }

    public function delete(Request $request)
    {

        $order = Order::findOrFail($request->delete_id);
        // increase user's session count

        $order->save();
        addReport(auth()->user()->id, 'بحذف طلب', $request->ip());
        Session::flash('success', 'تم الحذف بنجاح');
        return back();
    }
}
