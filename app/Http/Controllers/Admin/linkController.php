<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Link;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class linkController extends Controller
{

    public function index()
    {
        $data       = Link::get();
        $roles      = Role::latest()->get();
        return view('dashboard.link.index', compact('data', 'roles'));
    }

    public function store(Request $request)
    {

        // Validation rules
        $rules = [
            'title_ar'        => 'required',
            'title_en'        => 'required',
            'url'             => 'required',
        ];

        // Validator messages
        $messages = [
            'title_ar.required'     => 'العنوان بالعربية مطلوب',
            'title_en.required'     => 'العنوان بالانجليزية مطلوب',
            'url.required'          => 'الرابط مطلوب',
        ];

        // Validation
        $validator = Validator::make($request->all(), $rules, $messages);

        // If failed
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        //store link
        $add = new Link;
        $add->title_ar = $request->title_ar;
        $add->title_en = $request->title_en;
        $add->url      = $request->url;
        $add->save();

        addReport(auth()->user()->id, 'باضافة رابط جديد', $request->ip());
        Session::flash('success', 'تم الأضافة بنجاح');
        return back();
    }

    public function update(Request $request)
    {
        // Validation rules
        $rules = [
            'title_ar'        => 'required',
            'title_en'        => 'required',
            'url'             => 'required',
        ];

        // Validator messages
        $messages = [
            'title_ar.required'     => 'العنوان بالعربية مطلوب',
            'title_en.required'     => 'العنوان بالانجليزية مطلوب',
            'url.required'          => 'الرابط مطلوب',
        ];

        // Validation
        $validator = Validator::make($request->all(), $rules, $messages);

        // If failed
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        //store link
        $add = Link::findOrFail($request->id);
        $add->title_ar = $request->title_ar;
        $add->title_en = $request->title_en;
        $add->url      = $request->url;
        $add->save();

        addReport(auth()->user()->id, 'بتعديل بيانات رابط', $request->ip());
        Session::flash('success', 'تم التعديل بنجاح');
        return back();
    }

    public function delete(Request $request)
    {

        Link::findOrFail($request->delete_id)->delete();
        addReport(auth()->user()->id, 'بحذف رابط', $request->ip());
        Session::flash('success', 'تم الحذف بنجاح');
        return back();
    }

    public function deleteAll(Request $request)
    {
        $requestIds = json_decode($request->data);
        foreach ($requestIds as $id) {
            $ids[] = $id->id;
        }
        if (Link::whereIn('id', $ids)->delete()) {
            addReport(auth()->user()->id, 'قام بحذف العديد من الروابط', $request->ip());
            Session::flash('success', 'تم الحذف بنجاح');
            return response()->json('success');
        } else {
            return response()->json('failed');
        }
    }
}
