<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\UploadFile;
use App\Http\Controllers\Controller;
use App\Models\Offer;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class offerController extends Controller
{

    public function index()
    {
        $data       = Offer::orderBy('confirm','asc')->orderBy('id','desc')->get();
        $roles      = Role::latest()->get();
        return view('dashboard.offer.index', compact('data', 'roles'));
    }

    public function change_checked(Request $request)
    {
        //check data
        $offer = Offer::find($request->id);
        if (!isset($offer)) return 0;
        $offer->confirm = !$offer->confirm;
        $offer->save();
        //add report
        $offer->confirm == 1 ?
            addReport(auth()->user()->id, 'بالسماح بظهور عرض', $request->ip()) : addReport(auth()->user()->id, 'بمنع ظهور عرض', $request->ip());
        return 1;
    }

    public function store(Request $request)
    {

        // Validation rules
        $rules = [
            'type'              => 'required',
            'image'             => 'required',
        ];

        // Validator messages
        $messages = [
            'type.required'         => 'النوع مطلوب',
            'image.required'        => 'العرض مطلوبة',
        ];

        // Validation
        $validator = Validator::make($request->all(), $rules, $messages);

        // If failed
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        //store offer
        $add = new Offer;
        $add->desc_ar        = $request->desc_ar;
        $add->desc_en        = $request->desc_en;
        $add->type           = $request->type;
        $add->service_id     = $request->service_id;
        $add->start_date     = $request->start_date;
        $add->end_date       = $request->end_date;
        $add->status         = 1;
        $add->image = uploadImage($request->file('image'), 'public/images/offer');
        $add->save();

        addReport(auth()->user()->id, 'باضافة عرض ', $request->ip());
        Session::flash('success', 'تم الأضافة بنجاح');
        return back();
    }

    public function update(Request $request)
    {
        // Validation rules
        $rules = [
            'type'              => 'required',
        ];

        // Validator messages
        $messages = [
            'type.required'         => 'النوع مطلوب',
        ];

        // Validation
        $validator = Validator::make($request->all(), $rules, $messages);

        // If failed
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        //store offer
        $add = Offer::findOrFail($request->id);
        $add->desc_ar        = $request->desc_ar;
        $add->desc_en        = $request->desc_en;
        $add->type           = $request->type;
        $add->service_id     = $request->service_id;
        $add->start_date     = $request->start_date;
        $add->end_date       = $request->end_date;
        if ($request->has('image')) $add->image = uploadImage($request->file('image'), 'public/images/offer');
        $add->save();

        addReport(auth()->user()->id, 'بتعديل بيانات عرض', $request->ip());
        Session::flash('success', 'تم التعديل بنجاح');
        return back();
    }

    public function delete(Request $request)
    {

        Offer::findOrFail($request->delete_id)->delete();
        addReport(auth()->user()->id, 'بحذف عرض ', $request->ip());
        Session::flash('success', 'تم الحذف بنجاح');
        return back();
    }

    public function deleteAll(Request $request)
    {
        $requestIds = json_decode($request->data);
        foreach ($requestIds as $id) {
            $ids[] = $id->id;
        }
        if (Offer::whereIn('id', $ids)->delete()) {
            addReport(auth()->user()->id, 'قام بحذف العديد من العروض ', $request->ip());
            Session::flash('success', 'تم الحذف بنجاح');
            return response()->json('success');
        } else {
            return response()->json('failed');
        }
    }

    public function change_activation(Request $request)
    {
        //check data
        $offer = Offer::find($request->id);
        if (!isset($offer)) return 0;
        //update data
        $offer->active = !$offer->active;
        $offer->save();
        //add report
        $offer->checked == 1 ?
            addReport(auth()->user()->id, 'بتفعيل عرض', $request->ip()) : addReport(auth()->user()->id, 'بإلغاء تفعيل عرض', $request->ip());
        return 1;
    }
}