<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\UploadFile;
use App\Http\Controllers\Controller;
use App\Models\Section;
use App\Models\Service;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Models\Service_image;
use App\Models\Service_cut;

class serviceController extends Controller
{
    public function change_special_service(Request $request)
    {
        //check data
        $service = Service::find($request->id);
        if (!isset($service)) return 0;
        //update data
        $service->special = !$service->special;
        $service->save();
        //add report
        $service->special == 1 ?
            addReport(auth()->user()->id, 'بتمييز خدمة كالأكثر طلبا', $request->ip()) : addReport(auth()->user()->id, 'بالغاء تمييز خدمة كالأكثر طلبا', $request->ip());
        return 1;
    }

    public function change_new_service(Request $request)
    {
        //check data
        $section = Service::find($request->id);
        if (!isset($section)) return 0;
        //update data
        $section->new = !$section->new;
        $section->save();
        //add report
        $section->new == 1 ?
            addReport(auth()->user()->id, 'بتمييز خدمة كجديدة', $request->ip()) : addReport(auth()->user()->id, 'بالغاء بتمييز خدمة كجديدة', $request->ip());
        return 1;
    }

    public function new($id)
    {
        $roles      = Role::latest()->get();
        return view('dashboard.service.add', compact('roles', 'id'));
    }

    public function store(Request $request)
    {
        // Validation rules
        $rules = [
            'title_ar'                         => 'required|max:255',
            'title_en'                         => 'required|max:255',
            'short_desc_ar'                    => 'required',
            'short_desc_en'                    => 'required',
            'desc_ar'                          => 'required',
            'desc_en'                          => 'required',
            'price'                            => 'required',
            'cut_ids'                          => 'required',
            'images'                           => 'required',
        ];

        // Validator messages
        $messages = [
            'title_ar.required'                 => 'اسم المنتج بالعربية مطلوب',
            'title_en.required'                 => 'اسم المنتج بالانجليزية مطلوب',
            'short_desc_ar.required'            => 'وصف مختصر المنتج بالعربية مطلوب',
            'short_desc_en.required'            => 'وصف مختصر المنتج بالانجليزية مطلوب',
            'desc_ar.required'                  => 'وصف كامل المنتج بالعربية مطلوب',
            'desc_en.required'                  => 'وصف كامل المنتج بالانجليزية مطلوب',
            'price.required'                    => 'السعر مطلوب',
            'cut_ids.required'                  => 'اختار طرقة تقطيع واحدة على الاقل ',
            'images.required'                   => 'ارفع صورة واحدة على الاقل ',
        ];

        // Validation
        $validator = Validator::make($request->all(), $rules, $messages);

        // If failed
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        //store service
        $add = Service::create($request->except(['_token', 'images', 'cut_ids']));

        //store Service_images
        foreach ($request->file('images') as $photo) {
            #store image to DB
            $add_img = new Service_image;
            $add_img->image      = uploadImage($photo, 'public/images/service');
            $add_img->service_id = $add->id;
            $add_img->save();
        }

        //store Service_cut
        foreach ($request->cut_ids as $id) {
            $add_img = new Service_cut;
            $add_img->cut_id     = $id;
            $add_img->service_id = $add->id;
            $add_img->save();
        }

        addReport(auth()->user()->id, 'باضافة منتج جديد', $request->ip());
        Session::flash('success', 'تم الأضافة بنجاح');
        return redirect('admin/show-services/' . $add->section_id);
    }

    public function edit($id)
    {
        $data       = Service::findOrFail($id);
        $cut_ids    = $data->Cuts->pluck('cut_id')->toArray();
        $roles      = Role::latest()->get();
        return view('dashboard.service.edit', compact('data', 'roles', 'cut_ids'));
    }

    public function update(Request $request)
    {
        // Validation rules
        $rules = [
            'title_ar'                         => 'required|max:255',
            'title_en'                         => 'required|max:255',
            'short_desc_ar'                    => 'required',
            'short_desc_en'                    => 'required',
            'desc_ar'                          => 'required',
            'desc_en'                          => 'required',
            'price'                            => 'required',
            'cut_ids'                          => 'required',
        ];

        // Validator messages
        $messages = [
            'title_ar.required'                 => 'اسم المنتج بالعربية مطلوب',
            'title_en.required'                 => 'اسم المنتج بالانجليزية مطلوب',
            'short_desc_ar.required'            => 'وصف مختصر المنتج بالعربية مطلوب',
            'short_desc_en.required'            => 'وصف مختصر المنتج بالانجليزية مطلوب',
            'desc_ar.required'                  => 'وصف كامل المنتج بالعربية مطلوب',
            'desc_en.required'                  => 'وصف كامل المنتج بالانجليزية مطلوب',
            'price.required'                    => 'السعر مطلوب',
            'cut_ids.required'                  => 'اختار طرقة تقطيع واحدة على الاقل مطلوب',
        ];

        // Validation
        $validator = Validator::make($request->all(), $rules, $messages);

        // If failed
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        //update service
        Service::findOrFail($request->id)->update($request->except(['_token', 'id', 'images', 'cut_ids']));

        //store Service_images
        if ($request->has('images')) {
            foreach ($request->file('images') as $photo) {
                #store image to DB
                $add_img = new Service_image;
                $add_img->image = uploadImage($photo, 'public/images/service');
                $add_img->service_id = $request->id;
                $add_img->save();
            }
        }

        //store Service_cut
        Service_cut::where('service_id', $request->id)->delete();
        foreach ($request->cut_ids as $id) {
            $add_img = new Service_cut;
            $add_img->cut_id     = $id;
            $add_img->service_id = $request->id;
            $add_img->save();
        }

        addReport(auth()->user()->id, 'بتعديل بيانات منتج', $request->ip());
        Session::flash('success', 'تم التعديل بنجاح');
        return redirect('admin/show-services/' . $request->section_id);
    }

    public function delete(Request $request)
    {
        $add = Service::findOrFail($request->delete_id);
        $add->delete();
        addReport(auth()->user()->id, 'بحذف منتج', $request->ip());
        Session::flash('success', 'تم الحذف بنجاح');
        return back();
    }

    public function deleteAll(Request $request)
    {
        $requestIds = json_decode($request->data);
        foreach ($requestIds as $id) {
            $ids[] = $id->id;
        }
        if (Service::whereIn('id', $ids)->delete()) {
            addReport(auth()->user()->id, 'قام بحذف العديد من المنتجات', $request->ip());
            Session::flash('success', 'تم الحذف بنجاح');
            return response()->json('success');
        } else {
            return response()->json('failed');
        }
    }

    public function rmvImage(Request $request)
    {
        $data = Service_image::find($request->id);
        if (!isset($data)) return 'err';

        $count = Service_image::where('service_id', $data->service_id)->count();
        if ($count <= 1) return 'err';

        $data->delete();
        return 'success';
    }
}