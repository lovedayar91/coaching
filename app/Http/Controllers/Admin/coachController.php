<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Coach;
use App\Models\Coach_date;
use App\Models\Coach_image;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class coachController extends Controller
{

    public function index()
    {
        $data       = Coach::with('Images')->get();
        $roles      = Role::latest()->get();
        return view('dashboard.coach.index', compact('data', 'roles'));
    }

    public function edit($id)
    {
        $data       = Coach::with(['Dates', 'Times', 'Images'])->whereId($id)->firstOrFail();
        $roles      = Role::latest()->get();
        return view('dashboard.coach.edit', compact('data', 'roles'));
    }

    public function rmvImage(Request $request)
    {
        $data = Coach_image::find($request->id);
        if (!isset($data)) return 'err';

        $count = Coach_image::where('coach_id', $data->coach_id)->count();
        if ($count <= 1) return 'err';

        $data->delete();
        return 'success';
    }

    public function rmvData(Request $request)
    {
        $data = Coach_date::find($request->id);
        if (!isset($data)) return 'err';
        $data->delete();
        return 'success';
    }

    public function addData(Request $request)
    {
        $data = new Coach_date;
        $data->coach_id = $request->coach_id;
        $data->date     = $request->date;
        $data->save();
        return $data;
    }

    public function store(Request $request)
    {

        // Validation rules
        $rules = [
            'name_ar'         => 'required',
            'name_en'         => 'required',
            'email'           => 'required',
            'skype_id'        => 'required',
            'desc_ar'         => 'required',
            'desc_en'         => 'required',
            'image'           => 'required',
        ];

        // Validator messages
        $messages = [
            'name_ar.required'      => 'الأسم بالعربية مطلوب',
            'name_en.required'      => 'الأسم بالانجليزية مطلوب',
            'email.required'        => 'البريد الاكلتروني مطلوب',
            'skype_id.required'     => 'الرقم التعريفي لسكابي مطلوب',
            'desc_ar.required'      => 'التفاصيل بالعربية مطلوب',
            'desc_en.required'      => 'التفاصيل بالانجليزية مطلوب',
            'image.required'        => 'الصورة مطلوبة',
        ];

        // Validation
        $validator = Validator::make($request->all(), $rules, $messages);

        // If failed
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        //store coach
        $add = new Coach;
        $add->name_ar  = $request->name_ar;
        $add->name_en  = $request->name_en;
        $add->email    = $request->email;
        $add->skype_id = $request->skype_id;
        $add->desc_ar  = $request->desc_ar;
        $add->desc_en  = $request->desc_en;
        $add->save();

        #image
        foreach ($request->file('image') as $photo) {
            #store image to DB
            $add_img = new Service_image;
            $add_img->image      = uploadImage($photo, 'public/images/coach');
            $add_img->coach_id   = $add->id;
            $add_img->save();
        }

        #dates & times

        addReport(auth()->user()->id, 'باضافة مدربة جديد', $request->ip());
        Session::flash('success', 'تم الأضافة بنجاح');
        return back();
    }

    public function update(Request $request)
    {
        // Validation rules
        $rules = [
            'name_ar'         => 'required',
            'name_en'         => 'required',
            'email'           => 'required',
            'skype_id'        => 'required',
            'desc_ar'         => 'required',
            'desc_en'         => 'required',
        ];

        // Validator messages
        $messages = [
            'name_ar.required'      => 'الأسم بالعربية مطلوب',
            'name_en.required'      => 'الأسم بالانجليزية مطلوب',
            'email.required'        => 'البريد الاكلتروني مطلوب',
            'skype_id.required'     => 'الرقم التعريفي لسكابي مطلوب',
            'desc_ar.required'      => 'التفاصيل بالعربية مطلوب',
            'desc_en.required'      => 'التفاصيل بالانجليزية مطلوب',
        ];

        // Validation
        $validator = Validator::make($request->all(), $rules, $messages);

        // If failed
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        //store coach
        $add = Coach::findOrFail($request->id);
        $add->name_ar  = $request->name_ar;
        $add->name_en  = $request->name_en;
        $add->email    = $request->email;
        $add->skype_id = $request->skype_id;
        $add->desc_ar  = $request->desc_ar;
        $add->desc_en  = $request->desc_en;
        $add->save();

        #image
        if ($request->has('image')) {
            foreach ($request->file('image') as $photo) {
                #store image to DB
                $add_img = new Coach_image;
                $add_img->image      = uploadImage($photo, 'public/images/coach');
                $add_img->coach_id   = $add->id;
                $add_img->save();
            }
        }

        #dates & times

        addReport(auth()->user()->id, 'بتعديل بيانات مدربة', $request->ip());
        Session::flash('success', 'تم التعديل بنجاح');
        return redirect()->route('coachs');
    }

    public function delete(Request $request)
    {

        Coach::findOrFail($request->delete_id)->delete();
        addReport(auth()->user()->id, 'بحذف مدربة', $request->ip());
        Session::flash('success', 'تم الحذف بنجاح');
        return back();
    }

    public function deleteAll(Request $request)
    {
        $requestIds = json_decode($request->data);
        foreach ($requestIds as $id) {
            $ids[] = $id->id;
        }
        if (Coach::whereIn('id', $ids)->delete()) {
            addReport(auth()->user()->id, 'قام بحذف العديد من المدربات', $request->ip());
            Session::flash('success', 'تم الحذف بنجاح');
            return response()->json('success');
        } else {
            return response()->json('failed');
        }
    }
}
