<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Our_customer;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class our_customerController extends Controller
{

    public function index()
    {
        $data       = Our_customer::get();
        $roles      = Role::latest()->get();
        return view('dashboard.our_customer.index', compact('data', 'roles'));
    }

    public function store(Request $request)
    {

        // Validation rules
        $rules = [
            'name_ar'         => 'required',
            'name_en'         => 'required',
            'desc_ar'         => 'required',
            'desc_en'         => 'required',
            'image'           => 'required',
        ];

        // Validator messages
        $messages = [
            'name_ar.required'      => 'الأسم بالعربية مطلوب',
            'name_en.required'      => 'الأسم بالانجليزية مطلوب',
            'desc_ar.required'      => 'التفاصيل بالعربية مطلوب',
            'desc_en.required'      => 'التفاصيل بالانجليزية مطلوب',
            'image.required'        => 'الصورة مطلوبة',
        ];

        // Validation
        $validator = Validator::make($request->all(), $rules, $messages);

        // If failed
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        //store our_customer
        $add = new Our_customer;
        $add->name_ar  = $request->name_ar;
        $add->name_en  = $request->name_en;
        $add->desc_ar  = $request->desc_ar;
        $add->desc_en  = $request->desc_en;
        if ($request->has('image')) $add->image = uploadImage($request->file('image'), 'public/images/our_customer');
        $add->save();

        addReport(auth()->user()->id, 'باضافة رأي جديد', $request->ip());
        Session::flash('success', 'تم الأضافة بنجاح');
        return back();
    }

    public function update(Request $request)
    {
        // Validation rules
        $rules = [
            'name_ar'         => 'required',
            'name_en'         => 'required',
            'desc_ar'         => 'required',
            'desc_en'         => 'required',
        ];

        // Validator messages
        $messages = [
            'name_ar.required'      => 'الأسم بالعربية مطلوب',
            'name_en.required'      => 'الأسم بالانجليزية مطلوب',
            'desc_ar.required'      => 'التفاصيل بالعربية مطلوب',
            'desc_en.required'      => 'التفاصيل بالانجليزية مطلوب',
        ];

        // Validation
        $validator = Validator::make($request->all(), $rules, $messages);

        // If failed
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        //store our_customer
        $add = Our_customer::findOrFail($request->id);
        $add->name_ar  = $request->name_ar;
        $add->name_en  = $request->name_en;
        $add->desc_ar  = $request->desc_ar;
        $add->desc_en  = $request->desc_en;
        if ($request->has('image')) $add->image = uploadImage($request->file('image'), 'public/images/our_customer');
        $add->save();

        addReport(auth()->user()->id, 'بتعديل بيانات رأي', $request->ip());
        Session::flash('success', 'تم التعديل بنجاح');
        return back();
    }

    public function delete(Request $request)
    {

        Our_customer::findOrFail($request->delete_id)->delete();
        addReport(auth()->user()->id, 'بحذف رأي', $request->ip());
        Session::flash('success', 'تم الحذف بنجاح');
        return back();
    }

    public function deleteAll(Request $request)
    {
        $requestIds = json_decode($request->data);
        foreach ($requestIds as $id) {
            $ids[] = $id->id;
        }
        if (Our_customer::whereIn('id', $ids)->delete()) {
            addReport(auth()->user()->id, 'قام بحذف العديد من الأراء', $request->ip());
            Session::flash('success', 'تم الحذف بنجاح');
            return response()->json('success');
        } else {
            return response()->json('failed');
        }
    }
}
