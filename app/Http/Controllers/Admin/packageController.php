<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User_package;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Models\Package;
use App\Models\Section;

class packageController extends Controller
{

    public function index()
    {
        $data       = Package::get();
        $sections   = Section::orderBy('title_ar', 'asc')->get();
        $roles      = Role::latest()->get();
        return view('dashboard.package.index', compact('data', 'roles', 'sections'));
    }

    public function user_packages()
    {
        $data       = User_package::latest()->get();
        $roles      = Role::latest()->get();
        return view('dashboard.package.user_packages', compact('data', 'roles'));
    }

    public function store(Request $request)
    {
        // Validation rules
        $rules = [
            'section_id'            => 'required|exists:sections,id',
            'title_ar'              => 'required',
            'title_en'              => 'required',
            'desc_ar'               => 'required',
            'desc_en'               => 'required',
            'price'                 => 'required',
            'session_count'         => 'required',
        ];

        // Validator messages
        $messages = [
            'title_ar.required'         => 'العنوان بالعربية مطلوب',
            'title_en.required'         => 'العنوان بالانجليزية مطلوب',
            'desc_ar.required'          => 'التفاصيل بالعربية مطلوب',
            'desc_en.required'          => 'التفاصيل بالانجليزية مطلوب',
            'price.required'            => 'المبلغ مطلوب',
            'session_count.required'    => 'عدد الجلسات مطلوب',
            'section_id.required'       => 'القسم مطلوب',
            'section_id.exists'         => 'القسم غير صحيح',
        ];

        // Validation
        $validator = Validator::make($request->all(), $rules, $messages);

        // If failed
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        //store City
        $add = new Package;
        $add->title_ar          = $request->title_ar;
        $add->title_en          = $request->title_en;
        $add->desc_ar           = $request->desc_ar;
        $add->desc_en           = $request->desc_en;
        $add->price             = $request->price > 0 ? $request->price : 0;
        $add->session_count     = $request->session_count > 0 ? $request->session_count : 0;
        $add->section_id        = $request->section_id;
        $add->save();

        addReport(auth()->user()->id, 'باضافة باقة جديدة', $request->ip());
        Session::flash('success', 'تم الأضافة بنجاح');
        return back();
    }

    public function update(Request $request)
    {
        // Validation rules
        $rules = [
            'section_id'            => 'required|exists:sections,id',
            'title_ar'              => 'required',
            'title_en'              => 'required',
            'desc_ar'               => 'required',
            'desc_en'               => 'required',
            'price'                 => 'required',
            'session_count'         => 'required',
        ];

        // Validator messages
        $messages = [
            'title_ar.required'         => 'العنوان بالعربية مطلوب',
            'title_en.required'         => 'العنوان بالانجليزية مطلوب',
            'desc_ar.required'          => 'التفاصيل بالعربية مطلوب',
            'desc_en.required'          => 'التفاصيل بالانجليزية مطلوب',
            'price.required'            => 'المبلغ مطلوب',
            'session_count.required'    => 'عدد الجلسات مطلوب',
            'section_id.required'       => 'القسم مطلوب',
            'section_id.exists'         => 'القسم غير صحيح',
        ];

        // Validation
        $validator = Validator::make($request->all(), $rules, $messages);

        // If failed
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        //store City
        $add = Package::findOrFail($request->id);
        $add->title_ar          = $request->title_ar;
        $add->title_en          = $request->title_en;
        $add->desc_ar           = $request->desc_ar;
        $add->desc_en           = $request->desc_en;
        $add->price             = $request->price > 0 ? $request->price : 0;
        $add->session_count     = $request->session_count > 0 ? $request->session_count : 0;
        $add->section_id        = $request->section_id;
        $add->save();

        addReport(auth()->user()->id, 'بتعديل بيانات الباقة', $request->ip());
        Session::flash('success', 'تم التعديل بنجاح');
        return back();
    }

    public function delete(Request $request)
    {

        Package::findOrFail($request->delete_id)->delete();
        addReport(auth()->user()->id, 'بحذف باقة', $request->ip());
        Session::flash('success', 'تم الحذف بنجاح');
        return back();
    }

    public function deleteAll(Request $request)
    {
        $requestIds = json_decode($request->data);
        foreach ($requestIds as $id) {
            $ids[] = $id->id;
        }
        if (Package::whereIn('id', $ids)->delete()) {
            addReport(auth()->user()->id, 'قام بحذف العديد من الباقات', $request->ip());
            Session::flash('success', 'تم الحذف بنجاح');
            return response()->json('success');
        } else {
            return response()->json('failed');
        }
    }
}
