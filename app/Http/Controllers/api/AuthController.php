<?php

namespace App\Http\Controllers\api;

use Auth;
use App\User;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserCollection;
use App\Models\Device;
use Carbon\Carbon;

class AuthController extends Controller
{
    public $lang;
    public $title;

    public function __construct(Request $request)
    {
        SetLang($request->lang);
        $this->lang = isset($request->lang) && $request->lang == 'en' ? 'en' : 'ar';
        $this->title = isset($request->lang) && $request->lang == 'en' ? 'title_en' : 'title_ar';
    }

    #sign up & send active code
    public function Register(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'name_ar'       => 'required',
            'phone'         => 'required|unique:users',
            'email'         => 'required|email|unique:users',
            'password'      => 'required',
            'doctor'        => 'required',
            'name_en'       => 'nullable',
            'gender'        => 'nullable',
            'city_id'       => 'nullable|exists:cities,id',
            'avatar'	    => 'nullable',
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        /** Save data to users **/
        $user     = new User;
        $user->name_ar      = $request->name_ar;
        $user->name_en      = empty($request->name_en) ? $request->name_ar : $request->name_en;
        $user->phone        = convert2english($request->phone);
        $user->email        = $request->email;
        $user->gender       = $request->gender == 'female' ? 'female' : 'male';
        $user->password     = bcrypt($request->password);
        $user->city_id      = $request->city_id;
        $user->provider     = $request->doctor == 1 ? 1 : 0;
        if (!is_null($request->avatar))
            $user->avatar = uploadAvatar($request->file('avatar'), 'public/images/users');
        else
            $user->avatar       = 'default.png';
        $user->lang         = 'ar';
        $user->checked      = 1;
        $user->notify_send  = 1;
        $user->activation   = 0; // not active account
        $user->complete     = 0; // if user is doctor 0=not complete data , 1=complete data
        $user->confirm      = $request->doctor == 1 ? 0 : 1; // if user is doctor 0=not confirm data , 1=confirm data
        $user->has_package  = 0; // if user is doctor 0=has not Package , 1=has Package
        // $user->code         = rand(1111, 9999);
        $user->code         = 1234; //for test
        $user->save();

        /** Send Code To User's Using SMS **/
        send_mobile_sms_mobily(convert_phone_to_sa_format($user->phone), trans('api.activeCode') . $user->code);

        /** send data **/
        return apiResponse('1', trans('api.codeSentToPhoneSuccessfully'), new UserCollection($user));
    }

    #check active code
    public function ActiveCode(Request $request)
    {

        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'user_id'           => 'required|exists:users,id',
            'code'              => 'required',
            'device_id'         => 'required|max:255',
            'device_type'       => 'required|in:android,ios',
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        $user = User::whereId($request->user_id)->first();
        /** Check Code **/
        if ($user->code == $request->code) {
            /** update checked **/
            $user->code         = bcrypt(1234);
            $user->activation   = '1';
            $user->save();

            //check if this user already login on this device
            $device = Device::where('device_id', $request->device_id)->where('device_type', $request->device_type)->where('user_id', $user->id)->first();
            //check if another user already login on this device
            if (!isset($device)) $device = Device::where('device_id', $request->device_id)->where('device_type', $request->device_type)->first();
            if (!isset($device)) { // if there isn't user login on this device
                $device = new Device;
                $device->user_id     = $user->id;
                $device->device_id   = $request->device_id;
                $device->device_type = $request->device_type;
                $device->save();
            } else { // if there is user login on this device
                $device->user_id     = $user->id;
                $device->save();
            }

            /** send data **/
            return apiResponse('1', trans('api.activeSuccess'), new UserCollection($user), ['status' => userStatus($user)]);
        }

        /** If Code wrong **/
        return apiResponse('0', trans('api.wrongCode'));
    }

    #resend code
    public function resendActiveCode(Request $request)
    {

        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'user_id'   => 'required|exists:users,id',
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        $user = User::whereId($request->user_id)->first();
        // $user->code         = rand(1111, 9999);
        $user->code         = 1234; //for test
        $user->save();

        /** Send Code To User's Using SMS **/
        send_mobile_sms_mobily(convert_phone_to_sa_format($user->phone), trans('api.activeCode') . $user->code);

        /** send data **/
        return apiResponse('1', trans('api.send'));
    }

    #login and add device
    public function Login(Request $request)
    {

        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'phone_or_email'   => 'required',
            'password'         => 'required',
            'device_id'        => 'required|max:255',
            'device_type'      => 'required|in:android,ios',
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        $email  = $request->phone_or_email;
        $phone  = convert2english($request->phone_or_email);

        /** If Login Success by E-mail **/
        if (Auth::attempt(['email' => $email, 'password' => request('password')])) {
            /** If Login Success But Not Active User **/
            $user = User::find(Auth::id());
            if (Carbon::parse($user->end_package)->isPast()) {
                $user->complete = 0;
                $user->save();
            }

            //check if this user already login on this device
            $device = Device::where('device_id', $request->device_id)->where('device_type', $request->device_type)->where('user_id', $user->id)->first();
            //check if another user already login on this device
            if (!isset($device)) $device = Device::where('device_id', $request->device_id)->where('device_type', $request->device_type)->first();
            if (!isset($device)) { // if there isn't user login on this device
                $device = new Device;
                $device->user_id     = $user->id;
                $device->device_id   = $request->device_id;
                $device->device_type = $request->device_type;
                $device->save();
            } else { // if there is user login on this device
                $device->user_id     = $user->id;
                $device->save();
            }

            /** send data **/
            return apiResponse('1', trans('api.loggedInSuccessfully'), new UserCollection($user), ['status' => userStatus($user)]);
        }

        /** If Login Success by phone **/
        if (Auth::attempt(['phone' => $phone, 'password' => request('password')])) {
            /** If Login Success But Not Active User **/
            $user = User::find(Auth::id());
            if (Carbon::parse($user->end_package)->isPast()) {
                $user->complete = 0;
                $user->save();
            }

            //check if this user already login on this device
            $device = Device::where('device_id', $request->device_id)->where('device_type', $request->device_type)->where('user_id', $user->id)->first();
            //check if another user already login on this device
            if (!isset($device)) $device = Device::where('device_id', $request->device_id)->where('device_type', $request->device_type)->first();
            if (!isset($device)) { // if there isn't user login on this device
                $device = new Device;
                $device->user_id     = $user->id;
                $device->device_id   = $request->device_id;
                $device->device_type = $request->device_type;
                $device->save();
            } else { // if there is user login on this device
                $device->user_id     = $user->id;
                $device->save();
            }

            /** send data **/
            return apiResponse('1', trans('api.loggedInSuccessfully'), new UserCollection($user), ['status' => userStatus($user)]);
        }

        /** If Login wrong **/
        return apiResponse('0', trans('api.wrongLogin'));
    }

    #log out by delete device
    public function logout(Request $request)
    {

        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'user_id'       => 'required|exists:users,id',
            'device_id'     => 'required|max:255',
            'device_type'   => 'required|in:android,ios',
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        /** update User Data **/
        //check if this user already login on this device
        Device::where('device_id', $request->device_id)->where('device_type', $request->device_type)->where('user_id', $request->user_id)->delete();

        return apiResponse('1', trans('api.loggedOutSuccessfully'));
    }

    # foget password get phone and send code to user
    public function checkPhone(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'phone'         => 'required',
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        $phone  = convert2english($request->phone);

        //update code
        $user = User::where('phone', $phone)->first();

        if (!isset($user))
            return apiResponse('0', trans('api.wrongPhone'));

        // $user->code         = rand(1111, 9999);
        $user->code         = 1234; //for test
        $user->save();

        /** Send Code To User's Phone **/
        send_mobile_sms_mobily(convert_phone_to_sa_format($user->phone), trans('api.activeCode') . $user->code);

        /** send data **/
        return apiResponse('1', trans('api.codeSentToPhoneSuccessfully'), ['user_id' => $user->id], ['status' => userStatus($user)]);
    }

    # forget password check code and uopdate password
    public function checkCode(Request $request)
    {

        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'user_id'       => 'required|exists:users,id',
            'code'          => 'required',
            'password'      => 'required',
            'device_id'     => 'required|max:255',
            'device_type'   => 'required|in:android,ios',
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        $user = User::whereId($request->user_id)->first();
        /** Check Code **/
        if ($user->code == $request->code) {
            /** update Password **/
            $user->code      = bcrypt(1234);
            $user->password  = bcrypt($request->password);
            if (Carbon::parse($user->end_package)->isPast())
                $user->complete = 0;
            $user->save();

            //check if this user already login on this device
            $device = Device::where('device_id', $request->device_id)->where('device_type', $request->device_type)->where('user_id', $user->id)->first();
            //check if another user already login on this device
            if (!isset($device)) $device = Device::where('device_id', $request->device_id)->where('device_type', $request->device_type)->first();
            if (!isset($device)) { // if there isn't user login on this device
                $device = new Device;
                $device->user_id     = $user->id;
                $device->device_id   = $request->device_id;
                $device->device_type = $request->device_type;
                $device->save();
            } else { // if there is user login on this device
                $device->user_id     = $user->id;
                $device->save();
            }

            /** send data **/
            return apiResponse('1', trans('api.passwordUpdated'), new UserCollection($user), ['status' => userStatus($user)]);
        }

        /** If Code wrong **/
        return apiResponse('0', trans('api.wrongCode'));
    }
}
