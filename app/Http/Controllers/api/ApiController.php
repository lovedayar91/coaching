<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Cut;
use App\Models\Rate;
use App\Models\City;
use App\Models\Cart;
use App\Models\Order;
use App\Models\Contact;
use App\Models\Section;
use App\Models\Service;
use App\Models\Question;
use App\Models\Promo_code;
use App\Models\Order_option;
use App\Models\Notification;
use App\Models\Report_reson;
use App\Models\User_package;
use App\Http\Resources\SayCollection;
use App\Http\Resources\CartCollection;
use App\Http\Resources\ChatCollection;
use App\Http\Resources\CityCollection;
use App\Http\Resources\DayCollection;
use App\Http\Resources\DoctorCollection;
use App\Http\Resources\OrderCollection;
use App\Http\Resources\SectionCollection;
use App\Http\Resources\ServiceCollection;
use App\Http\Resources\QuestionCollection;
use App\Http\Resources\OrderDebtCollection;
use App\Http\Resources\ReportResonCollection;
use App\Http\Resources\NotificationCollection;
use Validator;
use App\Models\Service_rate;
use App\Http\Resources\offerCollection;
use App\Http\Resources\OfferCollection as AppOfferCollection;
use App\Http\Resources\PackageCollection;
use App\Http\Resources\RateCollection;
use App\Http\Resources\RoomCollection;
use App\Http\Resources\SpecialistCollection;
use App\Http\Resources\UserDoctorCollection;
use App\Http\Resources\UserDoctorsCollection;
use App\Http\Resources\UserFavouritesCollection;
use App\Http\Resources\userOfferCollection;
use App\Http\Resources\PricesCollection;
use App\Http\Resources\UserCollection;
use App\Models\Day;
use App\Models\Favourite;
use App\Models\Offer;
use App\Models\Package;
use App\Models\Room;
use App\Models\Room_chat;
use App\Models\Specialist;
use App\Models\User_day;
use App\Models\User_price;
use App\User;
use App;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller
{
    public $lang;
    public $title;

    public function __construct(Request $request)
    {
        SetLang($request->lang);
        $this->lang  = isset($request->lang) && $request->lang == 'en' ? 'en' : 'ar';
        $this->title = isset($request->lang) && $request->lang == 'en' ? 'title_en' : 'title_ar';
    }

    /*
    |----------------------------------------------------|
    |----------------------------------------------------|
    |                static Page Start                   |
    |----------------------------------------------------|
    |----------------------------------------------------|
    */

    #questions
    public function questions(Request $request)
    {
        return apiResponse('1', trans('api.send'), QuestionCollection::collection(Question::get()));
    }

    #intros
    public function intros(Request $request)
    {
        $data = [];
        $data['first_intro']  = is_null(fixText(settings('first_intro_' . $this->lang))) ? '' : fixText(settings('first_intro_' . $this->lang));
        $data['second_intro'] = is_null(fixText(settings('second_intro_' . $this->lang))) ? '' : fixText(settings('second_intro_' . $this->lang));
        $data['third_intro']  = is_null(fixText(settings('third_intro_' . $this->lang))) ? '' : fixText(settings('third_intro_' . $this->lang));
        return apiResponse('1', trans('api.send'), $data);
    }

    #about-us
    public function about_app(Request $request)
    {
        $data = is_null(settings('about_us_' . $this->lang)) ? '' : settings('about_us_' . $this->lang);
        return apiResponse('1', trans('api.send'), fixText($data));
    }

    #condition
    public function condition(Request $request)
    {
        $data = is_null(settings('condition_' . $this->lang)) ? '' : settings('condition_' . $this->lang);
        return apiResponse('1', trans('api.send'), fixText($data));
    }

    #policy
    public function policy(Request $request)
    {
        $data = is_null(settings('policy_' . $this->lang)) ? '' : settings('policy_' . $this->lang);
        return apiResponse('1', trans('api.send'), fixText($data));
    }


    /*
    |----------------------------------------------------|
    |----------------------------------------------------|
    |                updateDeviceId Start                |
    |----------------------------------------------------|
    |----------------------------------------------------|
    */

    #update device id
    public function updateDeviceId(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'user_id'       => 'required|exists:users,id',
            'device_id'     => 'required',
            'device_type'   => 'required',
        ]);


        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        //check if this user already login on this device
        $device = Device::where('device_id', $request->device_id)->where('device_type', $request->device_type)->where('user_id', $request->user_id)->first();
        //check if another user already login on this device
        if (!isset($device)) $device = Device::where('device_id', $request->device_id)->where('device_type', $request->device_type)->first();
        if (!isset($device)) { // if there isn't user login on this device
            $device = new Device;
            $device->user_id     = $request->user_id;
            $device->device_id   = $request->device_id;
            $device->device_type = $request->device_type;
            $device->save();
        } else { // if there is user login on this device
            $device->user_id     = $request->user_id;
            $device->save();
        }

        return apiResponse('1', trans('api.save'));
    }


    /*
    |----------------------------------------------------|
    |----------------------------------------------------|
    |                    Data Start                      |
    |----------------------------------------------------|
    |----------------------------------------------------|
    */

    #all packages
    public function show_all_packages(Request $request)
    {
        return apiResponse('1', trans('api.send'), PackageCollection::collection(Package::get()));
    }

    #all Days
    public function show_all_days(Request $request)
    {
        return apiResponse('1', trans('api.send'), DayCollection::collection(Day::get()));
    }

    #all cities
    public function show_all_cities(Request $request)
    {
        return apiResponse('1', trans('api.send'), CityCollection::collection(City::get()));
    }

    #all specialists
    public function show_all_specialists(Request $request)
    {
        return apiResponse('1', trans('api.send'), SpecialistCollection::collection(Specialist::get()));
    }

    #all specialists and cities
    public function show_all_cities_and_specialists(Request $request)
    {
        $data = [];
        $data['city_data']       = CityCollection::collection(City::get());
        $data['specialist_data'] = SpecialistCollection::collection(Specialist::get());
        return apiResponse('1', trans('api.send'), $data);
    }

    /*
    |----------------------------------------------------|
    |----------------------------------------------------|
    |                Contact Page Start                  |
    |----------------------------------------------------|
    |----------------------------------------------------|
    */

    #contact info
    public function Contact_Info(Request $request)
    {

        /** Send Data **/
        $data  = [];
        $data['phone']      = settings('phone');
        $data['mobile']     = settings('mobile');
        $data['facebook']   = social('facebook');
        $data['twitter']    = social('twitter');
        $data['instagram']  = social('instagram');
        $data['snapchat']   = social('snapchat');

        return apiResponse('1', trans('api.save'), $data);
    }

    #send contact
    public function Contact_Send(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'name'            => 'required',
            'phone'           => 'required',
            'message'         => 'required',
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        /** Save Contact **/
        $contact   = new Contact;
        $contact->name    = $request->name;
        $contact->phone   = $request->phone;
        $contact->type    = $request->type == 'doctor' ? 'doctor' : 'user';
        $contact->message = $request->message;
        $contact->save();

        /** Send Success Massage **/
        return apiResponse('1', trans('api.send'));
    }


    /*
    |----------------------------------------------------|
    |----------------------------------------------------|
    |                 Doctor Pages Start                 |
    |----------------------------------------------------|
    |----------------------------------------------------|
    */

    /*
        |---------------------------------------------|
        |               profile Pages                 |
        |---------------------------------------------|
        */

    #show Doctor Profile
    public function show_doctor_profile(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'user_id'   => 'required|exists:users,id',
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        $notify_count   =  isset($request->user_id) && !is_null($request->user_id) && !empty($request->user_id) ? notify_count($request->user_id) : 0;
        return apiResponse('1', trans('api.send'), new DoctorCollection(User::whereId($request->user_id)->first()) , ['notify_count' => $notify_count]);
    }

    #show Doctor Rate
    public function show_doctor_rate(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'user_id'   => 'required|exists:users,id',
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        return apiResponse('1', trans('api.send'), RateCollection::collection(Rate::where('to_id', $request->user_id)->get()));
    }

    #store doctor
    public function store_doctor(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'user_id'           => 'required|exists:users,id',
            'package_id'        => 'required|exists:packages,id',
            'specialist_id'     => 'required',
            'lat'               => 'required',
            'lng'               => 'required',
            'price'             => 'required',
            'address_ar'        => 'required',
            'address_en'        => 'required',
            'desc_ar'           => 'nullable',
            'desc_en'           => 'nullable',
            'job_ar'            => 'required',
            'job_en'            => 'required',
            'qualifaction_ar'   => 'nullable',
            'qualifaction_en'   => 'nullable',
            'start_at'          => 'required',
            'end_at'            => 'required',
            'days'              => 'required',
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        /** Package Data **/
        $package = Package::whereId($request->package_id)->first();

        /** complete doctor data **/
        $user   = User::whereId($request->user_id)->first();
        $user->lat           = $request->lat;
        $user->lng           = $request->lng;
        $user->specialist_id = $request->specialist_id;
        $user->price         = $request->price;
        $user->address_ar    = $request->address_ar;
        $user->address_en    = $request->address_en;
        $user->desc_ar       = $request->desc_ar;
        $user->desc_en       = $request->desc_en;
        $user->job_ar        = $request->job_ar;
        $user->job_en        = $request->job_en;
        $user->start_at      = Carbon::parse($request->start_at);
        $user->end_at        = Carbon::parse($request->end_at);
        $user->package_id    = $request->package_id;
        $user->end_package   = Carbon::now()->addYears($package->year)->addMonths($package->month)->addDays($package->day);
        $user->has_package   = 1;
        $user->complete      = 1; // if user is doctor 0=not complete data , 1=complete data
        $user->qualifaction_en = $request->qualifaction_en;
        $user->qualifaction_ar = $request->qualifaction_ar;
        $user->save();

        $user_package = new User_package;
        $user_package->user_id      = $request->user_id;
        $user_package->package_id   = $request->package_id;
        $user_package->end_date     = Carbon::now()->addYears($package->year)->addMonths($package->month)->addDays($package->day);
        $user_package->save();

        $days_id = json_decode($request->days);
        User_day::where('user_id',$request->user_id)->delete();
        foreach ($days_id as $i => $day) {
            $add = new User_day;
            $add->user_id   = $request->user_id;
            $add->day_id    = $day->day_id;
            $add->start_at  = $day->start_at == 0 ? Carbon::parse($request->start_at) : Carbon::parse($day->start_at);
            $add->end_at    = $day->end_at == 0 ? Carbon::parse($request->end_at) : Carbon::parse($day->end_at);
            $add->type      = $day->start_at == 0 ? 0 : 1; // 0=like static time in this day , 1=special time in this day
            $add->save();
        }

        /** Send Success Massage **/
        return apiResponse('1', trans('api.save'), new UserCollection($user));
    }

    #update doctor package
    public function update_doctor_package(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'user_id'           => 'required|exists:users,id',
            'package_id'        => 'required|exists:packages,id',
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        /** Package Data **/
        $package = Package::whereId($request->package_id)->first();

        /** complete doctor data **/
        $user   = User::whereId($request->user_id)->first();
        $user->package_id    = $request->package_id;
        $user->end_package   = Carbon::now()->addYears($package->year)->addMonths($package->month)->addDays($package->day);
        $user->has_package   = 1;
        $user->save();

        /** Send Success Massage **/
        return apiResponse('1', trans('api.save'));
    }

    #store doctor data
    public function update_doctor_data(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'user_id'           => 'required|exists:users,id',
            'specialist_id'     => 'required',
            'lat'               => 'required',
            'lng'               => 'required',
            'price'             => 'required',
            'address_ar'        => 'required',
            'address_en'        => 'required',
            'desc_ar'           => 'nullable',
            'desc_en'           => 'nullable',
            'job_ar'            => 'required',
            'job_en'            => 'required',
            'qualifaction_ar'   => 'nullable',
            'qualifaction_en'   => 'nullable',
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        /** complete doctor data **/
        $user   = User::whereId($request->user_id)->first();
        $user->lat             = $request->lat;
        $user->lng             = $request->lng;
        $user->specialist_id   = $request->specialist_id;
        $user->price           = $request->price;
        $user->address_ar      = $request->address_ar;
        $user->address_en      = $request->address_en;
        $user->desc_ar         = $request->desc_ar;
        $user->desc_en         = $request->desc_en;
        $user->job_ar          = $request->job_ar;
        $user->job_en          = $request->job_en;
        $user->qualifaction_en = $request->qualifaction_en;
        $user->qualifaction_ar = $request->qualifaction_ar;
        $user->save();

        /** Send Success Massage **/
        return apiResponse('1', trans('api.save'));
    }

    #store doctor days
    public function update_doctor_days(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'user_id'           => 'required|exists:users,id',
            'start_at'          => 'required',
            'end_at'            => 'required',
            'days'              => 'required',
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        /** complete doctor data **/
        $user   = User::whereId($request->user_id)->first();
        $user->start_at     = Carbon::parse($request->start_at);
        $user->end_at       = Carbon::parse($request->end_at);
        $user->save();

        User_day::where('user_id', $request->user_id)->delete();
        $days_id = json_decode($request->days);
        foreach ($days_id as $i => $day) {
            $add = new User_day;
            $add->user_id   = $request->user_id;
            $add->day_id    = $day->day_id;
            $add->start_at  = $day->start_at == 0 ? Carbon::parse($request->start_at) : Carbon::parse($day->start_at);
            $add->end_at    = $day->end_at == 0 ? Carbon::parse($request->end_at) : Carbon::parse($day->end_at);
            $add->type      = $day->start_at == 0 ? 0 : 1; // 0=like static time in this day , 1=special time in this day
            $add->save();
        }

        /** Send Success Massage **/
        return apiResponse('1', trans('api.save'));
    }

    #store doctor socials
    public function update_doctor_socials(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'user_id'       => 'required|exists:users,id',
            'facebook'      => 'nullable',
            'twitter'       => 'nullable',
            'instagram'     => 'nullable',
            'snapchat'      => 'nullable',
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        /** complete doctor data **/
        $user   = User::whereId($request->user_id)->first();
        $user->facebook     = $request->facebook;
        $user->twitter      = $request->twitter;
        $user->instagram    = $request->instagram;
        $user->snapchat     = $request->snapchat;
        $user->save();

        /** Send Success Massage **/
        return apiResponse('1', trans('api.save'));
    }

    #store doctor prices
    public function store_doctor_prices(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'user_id'       => 'required|exists:users,id',
            'title_ar'      => 'required',
            'title_en'      => 'required',
            'price'         => 'required',
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        /** complete doctor data **/
        $add    = new User_price;
        $add->user_id    = $request->user_id;
        $add->title_ar   = $request->title_ar;
        $add->title_en   = $request->title_en;
        $add->price      = $request->price;
        $add->save();

        /** Send Success Massage **/
        return apiResponse('1', trans('api.save'),new PricesCollection($add));
    }

    #update doctor prices
    public function update_doctor_prices(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'price_id'      => 'required|exists:user_prices,id',
            'title_ar'      => 'required',
            'title_en'      => 'required',
            'price'         => 'required',
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        /** complete doctor data **/
        $add    = User_price::whereId($request->price_id)->first();
        $add->title_ar   = $request->title_ar;
        $add->title_en   = $request->title_en;
        $add->price      = $request->price;
        $add->save();

        /** Send Success Massage **/
        return apiResponse('1', trans('api.save'),new PricesCollection($add));
    }

    #delete doctor prices
    public function delete_doctor_prices(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'price_id'      => 'required|exists:user_prices,id',
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        /** complete doctor data **/
        User_price::whereId($request->price_id)->delete();

        /** Send Success Massage **/
        return apiResponse('1', trans('api.delete'));
    }

    /*
        |---------------------------------------------|
        |                offers Pages                 |
        |---------------------------------------------|
        */

    #show Doctor Offer
    public function show_doctor_offer(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'user_id'   => 'required|exists:users,id',
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        return apiResponse('1', trans('api.send'), AppOfferCollection::collection(Offer::where('user_id', $request->user_id)->get()));
    }

    #store doctor offer
    public function store_doctor_offer(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'user_id'       => 'required|exists:users,id',
            'title_ar'      => 'required',
            'title_en'      => 'required',
            'desc_ar'       => 'required',
            'desc_en'       => 'required',
            'old_price'     => 'required',
            'new_price'     => 'required',
            'image'         => 'required',
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        /** complete doctor data **/
        $add    = new Offer;
        $add->user_id    = $request->user_id;
        $add->title_ar   = $request->title_ar;
        $add->title_en   = $request->title_en;
        $add->desc_ar    = $request->desc_ar;
        $add->desc_en    = $request->desc_en;
        $add->old_price  = $request->old_price;
        $add->new_price  = $request->new_price;
        $add->confirm    = 0;
        $add->image      = uploadImage($request->file('image'), 'public/images/offer');
        $add->save();

        /** Send Success Massage **/
        return apiResponse('1', trans('api.save'),new AppOfferCollection($add));
    }

    #update doctor offer
    public function update_doctor_offer(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'offer_id'      => 'required|exists:offers,id',
            'title_ar'      => 'required',
            'title_en'      => 'required',
            'desc_ar'       => 'required',
            'desc_en'       => 'required',
            'old_price'     => 'required',
            'new_price'     => 'required',
            'image'         => 'nullable',
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        /** complete doctor data **/
        $add    = Offer::whereId($request->offer_id)->first();
        $add->title_ar   = $request->title_ar;
        $add->title_en   = $request->title_en;
        $add->desc_ar    = $request->desc_ar;
        $add->desc_en    = $request->desc_en;
        $add->old_price  = $request->old_price;
        $add->new_price  = $request->new_price;
        if (!is_null($request->image)) $add->image      = uploadImage($request->file('image'), 'public/images/offer');
        $add->save();

        /** Send Success Massage **/
        return apiResponse('1', trans('api.save'),new AppOfferCollection($add));
    }

    #delete doctor offer
    public function delete_doctor_offer(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'offer_id'      => 'required|exists:offers,id',
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        /** complete doctor data **/
        Offer::whereId($request->offer_id)->delete();

        /** Send Success Massage **/
        return apiResponse('1', trans('api.delete'));
    }

    #report comment
    public function report_comment(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'rate_id'    => 'required|exists:rates,id',
            'reson_id'   => 'nullable',
            'reason_desc' => 'nullable',
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        $reason = Report_reson::whereId($request->reson_id)->first();

        $rate   = Rate::whereId($request->rate_id)->first();
        $rate->report       = 1;
        $rate->reson_id     = isset($reason) ? $reason->id : null;
        $rate->reason_desc  = isset($reason) ? $reason->title_ar : $request->reason_desc;
        $rate->save();

        return apiResponse('1', trans('api.save'));
    }


    /*
    |----------------------------------------------------|
    |----------------------------------------------------|
    |             Client - User Pages Start              |
    |----------------------------------------------------|
    |----------------------------------------------------|
    */

    /*
        |---------------------------------------------|
        |               doctors Pages                 |
        |---------------------------------------------|
        */

    #show user_home
    public function home(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'user_id'   => 'nullable|exists:users,id',
            'lat'       => 'required',
            'lng'       => 'required',
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        $data = [];
        $data['notify_count']   =  isset($request->user_id) && !is_null($request->user_id) && !empty($request->user_id) ? notify_count($request->user_id) : 0;
        $data['docotrs_data']   =  UserDoctorsCollection::collection(User::getDoctorByLocation($request->lat, $request->lng));

        if(isset($request->user_id) && !empty($request->user_id)){
            $user = User::whereId($request->user_id)->first();
            if(isset($user))
                return apiResponse('1', trans('api.send'), $data , ['status' => userStatus($user)]);
        }

        return apiResponse('1', trans('api.send'), $data , ['status' => '']);
    }

    #show doctors by name
    public function searchDoctors(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'user_id'   => 'nullable|exists:users,id',
            'name'      => 'required',
            'lat'       => 'required',
            'lng'       => 'required',
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        $query = User::query();
        $query->where('provider', 1);
        $query->where('checked', 1);
        $query->where('confirm', 1);
        $query->whereDate('end_package', '>=', Carbon::now());
        $query->having('distance', '<=', 1000000000000000000000)
            ->select(
                DB::raw("*,
                (3959 * ACOS(COS(RADIANS($request->lat))
                * COS(RADIANS(lat))
                * COS(RADIANS($request->lng) - RADIANS(lng))
                + SIN(RADIANS($request->lat))
                * SIN(RADIANS(lat)))) AS distance")
            );
        $query->where('name_ar', 'like', '%' . $request->name . '%')->orWhere('name_en', 'like', '%' . $request->name . '%');
        $doctors = $query->get();

        return apiResponse('1', trans('api.send'), UserDoctorsCollection::collection($doctors));
    }

    #show doctors after filter
    public function filterDoctors(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'location'        => 'nullable', // -1=near to far , 0=all , {city_id}=this city 
            'specialist_id'   => 'nullable', //0=all , {specialist_id}=this specialist
            'gender'          => 'nullable', // male or female
            'lat'             => 'required',
            'lng'             => 'required',
            'type'            => 'nullable',//rate
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        /** Get Data **/
        $query = User::query();
        $query->where('provider', 1);
        $query->where('checked', 1);
        $query->where('confirm', 1);
        $query->whereDate('end_package', '>=', Carbon::now());


        if (isset($request->location)) {
            if ($request->location == -1) {
                $query->having('distance', '<=', 100)
                    ->select(
                        DB::raw("*,
                            (3959 * ACOS(COS(RADIANS($request->lat))
                            * COS(RADIANS(lat))
                            * COS(RADIANS($request->lng) - RADIANS(lng))
                            + SIN(RADIANS($request->lat))
                            * SIN(RADIANS(lat)))) AS distance")
                    );
            }
            elseif ($request->location == 0) {
                $query->having('distance', '<=', 1000000000000000000000)
                    ->select(
                        DB::raw("*,
                            (3959 * ACOS(COS(RADIANS($request->lat))
                            * COS(RADIANS(lat))
                            * COS(RADIANS($request->lng) - RADIANS(lng))
                            + SIN(RADIANS($request->lat))
                            * SIN(RADIANS(lat)))) AS distance")
                    );
            }
            else{
                $query->where('city_id', $request->location);
                $query->having('distance', '<=', 1000000000000000000000)
                    ->select(
                        DB::raw("*,
                            (3959 * ACOS(COS(RADIANS($request->lat))
                            * COS(RADIANS(lat))
                            * COS(RADIANS($request->lng) - RADIANS(lng))
                            + SIN(RADIANS($request->lat))
                            * SIN(RADIANS(lat)))) AS distance")
                        );
            }
        }

        if (isset($request->specialist_id) && $request->specialist_id > 0)
            $query->where('specialist_id', $request->specialist_id);

        if (isset($request->gender) && !is_null($request->gender))
            $query->where('gender', $request->gender);

        if($request->type == 'rate'){
            $doctors_data = $query->get();
            $doctors      = $doctors_data->sortByDesc(function ($q, $key) {
                return round($q->Rates->avg('rate'));
            });
        }else{
            $doctors      = $query->get();
        }


        return apiResponse('1', trans('api.send'), UserDoctorsCollection::collection($doctors));
    }

    #show doctor profile
    public function showDoctor(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'user_id'     => 'nullable|exists:users,id',
            'doctor_id'   => 'required|exists:users,id',
            'lat'         => 'required',
            'lng'         => 'required',
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        $query = User::query();
        $query->where('id', $request->doctor_id);
        $query->where('provider', 1);
        $query->where('checked', 1);
        $query->where('confirm', 1);
        $query->whereDate('end_package', '>=', Carbon::now());
        $query->having('distance', '<=', 1000000000000000000000)
            ->select(
                DB::raw("*,
                (3959 * ACOS(COS(RADIANS($request->lat))
                * COS(RADIANS(lat))
                * COS(RADIANS($request->lng) - RADIANS(lng))
                + SIN(RADIANS($request->lat))
                * SIN(RADIANS(lat)))) AS distance")
            );
        $doctor = $query->first();

        $data = [];
        $data['favourite']   = isset($request->user_id) && !is_null($request->user_id) && !empty($request->user_id) ? hasFavourite($request->user_id, $request->doctor_id) : false;
        $data['doctor_data'] = new UserDoctorCollection($doctor);
        return apiResponse('1', trans('api.send'), $data);
    }

    #favoutite or remove favourite about Doctor
    public function favoutiteDoctor(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'user_id'     => 'nullable|exists:users,id',
            'doctor_id'   => 'required|exists:users,id',
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        $check = Favourite::where('user_id', $request->user_id)->where('doctor_id', $request->doctor_id)->first();
        if (isset($check))
            $check->delete();
        else {
            $add = new Favourite;
            $add->user_id   = $request->user_id;
            $add->doctor_id = $request->doctor_id;
            $add->save();
        }

        return apiResponse('1', trans('api.save'));
    }

    #rate doctor
    public function rateDoctor(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'doctor_id'     => 'required|exists:users,id',
            'user_id'       => 'required|exists:users,id',
            'rate'          => 'required',
            'comment'       => 'required',
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        /** Get Data **/
        $rate = Rate::where('from_id', $request->user_id)->where('to_id', $request->doctor_id)->first();
        if (!isset($rate)) $rate = new Rate;
        $rate->to_id        = $request->doctor_id;
        $rate->from_id      = $request->user_id;
        $rate->comment      = $request->comment;
        $rate->rate         = $request->rate;
        $rate->type         = 'doctor';
        $rate->report       = 0;
        $rate->save();

        /** Get Data **/
        return apiResponse('1', trans('api.save'), RateCollection::collection(Rate::where('to_id', $request->doctor_id)->orderBy('updated_at', 'desc')->get()));
    }

        /*
        |---------------------------------------------|
        |                Offers Pages                 |
        |---------------------------------------------|
        */

    #show all doctor offers
    public function show_doctor_offers(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'doctor_id' => 'nullable',
            'lat'       => 'required',
            'lng'       => 'required',
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        $lat = $request->lat;
        $lng = $request->lng;

        $query = Offer::query();
        $query->where('confirm', 1);
        if(isset($request->doctor_id) && !is_null($request->doctor_id))
            $query->where('user_id',$request->doctor_id);

        $query->whereHas('User', function ($q) {
            return $q->whereDate('end_package', '>=', Carbon::now());
        });
        $query->whereHas('User', function ($q) use ($lat, $lng) {
            return $q->having('distance', '<=', 1000000000000000000000)
                ->select(
                    DB::raw("*,
                    (3959 * ACOS(COS(RADIANS($lat))
                    * COS(RADIANS(lat))
                    * COS(RADIANS($lng) - RADIANS(lng))
                    + SIN(RADIANS($lat))
                    * SIN(RADIANS(lat)))) AS distance")
                );
        });

        $query->orderBy('id', 'desc');
        $offers = $query->get();

        return apiResponse('1', trans('api.send'), userOfferCollection::collection($offers));
    }

    #show all offers
    public function showOffers(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'doctor_id' => 'nullable',
            'lat'       => 'required',
            'lng'       => 'required',
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        $lat = $request->lat;
        $lng = $request->lng;

        $query = Offer::query();
        $query->where('confirm', 1);
        if(isset($request->doctor_id) && !is_null($request->doctor_id))
            $query->where('user_id',$request->doctor_id);

        $query->whereHas('User', function ($q) {
            return $q->where('checked', '1');
        });
        $query->whereHas('User', function ($q) {
            return $q->where('confirm', '1');
        });
        $query->whereHas('User', function ($q) {
            return $q->whereDate('end_package', '>=', Carbon::now());
        });
        $query->whereHas('User', function ($q) use ($lat, $lng) {
            return $q->having('distance', '<=', 1000000000000000000000)
                ->select(
                    DB::raw("*,
                    (3959 * ACOS(COS(RADIANS($lat))
                    * COS(RADIANS(lat))
                    * COS(RADIANS($lng) - RADIANS(lng))
                    + SIN(RADIANS($lat))
                    * SIN(RADIANS(lat)))) AS distance")
                );
        });

        $query->orderBy('id', 'desc');
        $offers = $query->get();

        return apiResponse('1', trans('api.send'), userOfferCollection::collection($offers));
    }

    #show offers by name
    public function searchOffers(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'user_id'   => 'nullable|exists:users,id',
            'title'     => 'required',
            'lat'       => 'required',
            'lng'       => 'required',
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        $lat = $request->lat;
        $lng = $request->lng;

        $query = Offer::query();
        $query->where('confirm', 1);
        $query->whereHas('User', function ($q) {
            return $q->where('checked', '1');
        });
        $query->whereHas('User', function ($q) {
            return $q->where('confirm', '1');
        });
        $query->whereHas('User', function ($q) {
            return $q->whereDate('end_package', '>=', Carbon::now());
        });
        $query->whereHas('User', function ($q) use ($lat, $lng) {
            return $q->having('distance', '<=', 1000000000000000000000)
                ->select(
                    DB::raw("*,
                    (3959 * ACOS(COS(RADIANS($lat))
                    * COS(RADIANS(lat))
                    * COS(RADIANS($lng) - RADIANS(lng))
                    + SIN(RADIANS($lat))
                    * SIN(RADIANS(lat)))) AS distance")
                );
        });

        $query->where('title_ar', 'like', '%' . $request->title . '%')->orWhere('title_en', 'like', '%' . $request->title . '%');
        $query->orderBy('id', 'desc');
        $offers = $query->get();

        return apiResponse('1', trans('api.send'), userOfferCollection::collection($offers));
    }

    #show offers after filter
    public function filterOffers(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'location' => 'nullable', // -1=near to far , 0=all , {city_id}=this city
            'specialist_id' => 'nullable', //0=all , {specialist_id}=this specialist
            'gender' => 'nullable', // male or female
            'lat' => 'nullable',
            'lng' => 'nullable',
            'type' => 'nullable',//rate
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        /** Get Data **/
        $lat = $request->lat;
        $lng = $request->lng;
        $city_id = $request->location;
        $specialist_id = $request->specialist_id;
        $gender = $request->gender;

        $query = Offer::query();
        $query->where('confirm', 1);
        $query->whereHas('User', function ($q) {
            return $q->where('checked', '1');
        });
        $query->whereHas('User', function ($q) {
            return $q->where('confirm', '1');
        });
        $query->whereHas('User', function ($q) {
            return $q->whereDate('end_package', '>=', Carbon::now());
        });


        if (isset($request->location)) {
            if ($request->location == -1) {
                $query->whereHas('User', function ($q) use ($lat, $lng) {
                    return $q->having('distance', '<=', 100)
                        ->select(
                            DB::raw("*,
                            (3959 * ACOS(COS(RADIANS($lat))
                            * COS(RADIANS(lat))
                            * COS(RADIANS($lng) - RADIANS(lng))
                            + SIN(RADIANS($lat))
                            * SIN(RADIANS(lat)))) AS distance")
                        );
                });
            } elseif ($request->location == 0) {
                $query->whereHas('User', function ($q) use ($lat, $lng) {
                    return $q->having('distance', '<=', 1000000000000000000000)
                        ->select(
                            DB::raw("*,
                            (3959 * ACOS(COS(RADIANS($lat))
                            * COS(RADIANS(lat))
                            * COS(RADIANS($lng) - RADIANS(lng))
                            + SIN(RADIANS($lat))
                            * SIN(RADIANS(lat)))) AS distance")
                        );
                });
            }else{
                $query->whereHas('User', function ($q) use ($city_id) {
                    return $q->where('city_id', $city_id);
                });

                $query->whereHas('User', function ($q) use ($lat, $lng) {
                    return $q->having('distance', '<=', 1000000000000000000000)
                        ->select(
                            DB::raw("*,
                        (3959 * ACOS(COS(RADIANS($lat))
                        * COS(RADIANS(lat))
                        * COS(RADIANS($lng) - RADIANS(lng))
                        + SIN(RADIANS($lat))
                        * SIN(RADIANS(lat)))) AS distance")
                        );
                });
            }
        }

        if (isset($request->specialist_id) && $request->specialist_id > 0)
            $query->whereHas('User', function ($q) use ($specialist_id) {
                return $q->where('specialist_id', $specialist_id);
            });

        if (isset($request->gender) && !is_null($request->gender))
            $query->whereHas('User', function ($q) use ($gender) {
                return $q->where('gender', $gender);
            });

        if($request->type == 'rate') {
            $offers_data = $query->get();
            $offers = $offers_data->sortByDesc(function ($q, $key) {
                return round($q->Rates->avg('rate'));
            });
        }else{
            $offers = $query->get();
        }


        return apiResponse('1', trans('api.send'), userOfferCollection::collection($offers));
    }

    #show offer
    public function showOffer(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'offer_id'    => 'required|exists:offers,id',
            'lat'         => 'required',
            'lng'         => 'required',
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        /** Get Data **/
        $lat = $request->lat;
        $lng = $request->lng;

        $query = Offer::query();
        $query->where('id', $request->offer_id);
        $query->where('confirm', 1);

        $query->whereHas('User', function ($q) {
            return $q->whereDate('end_package', '>=', Carbon::now());
        });

        $query->whereHas('User', function ($q) use ($lat, $lng) {
            return $q->having('distance', '<=', 1000000000000000000000)
                ->select(
                    DB::raw("*,
                    (3959 * ACOS(COS(RADIANS($lat))
                    * COS(RADIANS(lat))
                    * COS(RADIANS($lng) - RADIANS(lng))
                    + SIN(RADIANS($lat))
                    * SIN(RADIANS(lat)))) AS distance")
                );
        });
        $offer = $query->first();

        return apiResponse('1', trans('api.send'), new userOfferCollection($offer));
    }

    #rate offer
    public function rateOffer(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'offer_id'     => 'required|exists:offers,id',
            'user_id'       => 'required|exists:users,id',
            'rate'          => 'required',
            'comment'       => 'required',
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        /** Get Data **/
        $rate = Rate::where('from_id', $request->user_id)->where('offer_id', $request->offer_id)->first();
        if (!isset($rate)) $rate = new Rate;
        $rate->offer_id     = $request->offer_id;
        $rate->from_id      = $request->user_id;
        $rate->comment      = $request->comment;
        $rate->rate         = $request->rate;
        $rate->type         = 'offer';
        $rate->report       = 0;
        $rate->save();

        /** Get Data **/
        return apiResponse('1', trans('api.save'), RateCollection::collection(Rate::where('offer_id', $request->offer_id)->orderBy('updated_at', 'desc')->get()));
    }

    /*
        |---------------------------------------------|
        |              Favourite Pages                |
        |---------------------------------------------|
        */

    #show all offers
    public function showFavourites(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'user_id'   => 'nullable|exists:users,id',
            'lat'       => 'required',
            'lng'       => 'required',
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        /** Get Data **/
        $lat = $request->lat;
        $lng = $request->lng;

        $query = Favourite::query();
        $query->where('user_id', $request->user_id);
        $query->whereHas('Doctor', function ($q) {
            return $q->whereDate('end_package', '>=', Carbon::now());
        });
        $query->whereHas('Doctor', function ($q) {
            return $q->where('checked', '1');
        });
        $query->whereHas('Doctor', function ($q) {
            return $q->where('confirm', '1');
        });
        $query->whereHas('Doctor', function ($q) use ($lat, $lng) {
            return $q->having('distance', '<=', 1000000000000000000000)
                ->select(
                    DB::raw("*,
                    (3959 * ACOS(COS(RADIANS($lat))
                    * COS(RADIANS(lat))
                    * COS(RADIANS($lng) - RADIANS(lng))
                    + SIN(RADIANS($lat))
                    * SIN(RADIANS(lat)))) AS distance")
                );
        });

        $query->orderBy('id', 'desc');
        $favourites = $query->get();

        return apiResponse('1', trans('api.send'), UserFavouritesCollection::collection($favourites));
    }

    /*
    |----------------------------------------------------|
    |----------------------------------------------------|
    |                 Chat Page Start                    |
    |----------------------------------------------------|
    |----------------------------------------------------|
    */

    #store chat
    public function store_chat(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'from_id'   => 'required|exists:users,id',
            'to_id'     => 'required|exists:users,id',
            'message'   => 'required',
            'type'      => 'required', //text or image
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        /** Data **/
        $from_id    = $request->from_id;
        $to_id      = $request->to_id;

        $room  = Room::where('user_id', $from_id)->where('doctor_id', $to_id)->first();
        if (!isset($room)) $room  = Room::where('user_id', $to_id)->where('doctor_id', $from_id)->first();
        if (!isset($room)) {
            $room = new Room;
            $room->user_id      = $from_id;
            $room->doctor_id    = $to_id;
            $room->save();
        }

        $chat = new Room_chat;
        $chat->room_id  = $room->id;
        $chat->from_id  = $from_id;
        $chat->to_id    = $to_id;
        $chat->message  = $request->type == 'image' ? uploadImage($request->file('message'), 'public/images/chat') : $request->message;
        $chat->type     = $request->type == 'image' ? 'image' : 'text';
        $chat->seen     = 0;
        $chat->save();

        $user = User::whereId($request->to_id)->first();
        $lang = $user->lang;
        $name = App::getLocale() == 'en' ? 'name_en' : 'name_ar';
        $msg  = [
            'title_ar'    => 'رسالة جديدة',
            'title_en'    => 'New message',
        ];
        $fcm_data   = [];
        $fcm_data['title']          = $msg['title_'.$lang];
        $fcm_data['room_id']        = $room->id;
        $fcm_data['from_id']        = (int)    $chat->from_id;
        $fcm_data['from_name']      = (string) $chat->From->$name;
        $fcm_data['from_gender']    = (string) $chat->From->gender;
        $fcm_data['from_avatar']    = !is_null($chat->From) && !is_null($chat->From->avatar) ? url('public/images/users/' . $chat->From->avatar) : url('public/images/users/default.png');
        $fcm_data['msg']            = $request->type == 'image' ? url('' . $chat->message) : $chat->message;
        $fcm_data['type']           = $request->type;
        $fcm_data['status']         = 'new_message';
        foreach ($user->Devices as $device){
            Send_FCM_Badge($device->device_id ,$fcm_data, $device->device_type);
        }

        $data = [
            'room_id'   => $chat->room_id,
            'from_id'   => $chat->from_id,
            'from_image'=> !is_null($chat->From) && !is_null($chat->From->avatar) ? url('public/images/users/' . $chat->From->avatar) : url('public/images/users/default.png'),
            'to_id'     => $chat->to_id,
            'to_image'  => !is_null($chat->To) && !is_null($chat->To->avatar) ? url('public/images/users/' . $chat->To->avatar) : url('public/images/users/default.png'),
            'date'      => (string) Carbon::parse($chat->created_at)->diffForHumans(),
            'message'   => $chat->type == 'image' ? url('' . $chat->message) : (string)  $chat->message,
            'type'      => (string) $chat->type,
        ];

        /** Send Data **/
        return apiResponse('1', trans('api.save'), $data);
    }

    #store chat
    public function check_room(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'from_id'   => 'required|exists:users,id',
            'to_id'     => 'required|exists:users,id',
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        /** Data **/
        $from_id    = $request->from_id;
        $to_id      = $request->to_id;

        $room  = Room::where('user_id', $from_id)->where('doctor_id', $to_id)->first();
        if (!isset($room)) $room  = Room::where('user_id', $to_id)->where('doctor_id', $from_id)->first();
        if (!isset($room)) { //has not room so create one
            $room = new Room;
            $room->user_id      = $from_id;
            $room->doctor_id    = $to_id;
            $room->save();
        }

        /** Send Data **/
        return apiResponse('1', trans('api.save'), $room->id);
    }

    #show all chats
    public function show_all_chats(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'user_id'   => 'required|exists:users,id',
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        $rooms      = Room::has('Chats')->where('user_id', $request->user_id)->orWhere('doctor_id', $request->user_id)->orderBy('id','desc')->get();
        $name       = App::getLocale() == 'en' ? 'name_en' : 'name_ar';
        $data       = [];
        foreach($rooms as $i=>$room){
            if((int)$request->user_id == (int)$room->Doctor->id){
                $user_id       = (int) $room->User->id;
                $user_name     = !is_null($room->User) ? (string) $room->User->name_ar : '';
                $user_gender   = !is_null($room->User) && !empty((string) $room->User->gender) ? (string) $room->User->gender : 'male';
                $user_avatar   = !is_null($room->User) &&  !is_null($room->User->avatar) ? url('public/images/users/' . $room->User->avatar) : '';
            }elseif((int)$request->user_id == (int)$room->User->id){
                $user_id       = (int) $room->Doctor->id;
                $user_name     = !is_null($room->Doctor) ? (string) $room->Doctor->$name : '';
                $user_gender   = !is_null($room->Doctor) && !empty((string) $room->Doctor->gender) ? (string) $room->Doctor->gender : 'male';
                $user_avatar   = !is_null($room->Doctor) &&  !is_null($room->Doctor->avatar) ? url('public/images/users/' . $room->Doctor->avatar) : '';
            }
            $data[$i] = [
                'id'            => (int) $room->id,
                'user_id'       => $user_id,
                'user_name'     => $user_name,
                'user_gender'   => $user_gender,
                'user_avatar'   => $user_avatar,
                'last_message'  => !is_null($room->Chats) ? lastMsg($room->id)['last_message'] : '',
                'type'          => !is_null($room->Chats) ? lastMsg($room->id)['type'] : '',
                'sender_id'     => !is_null($room->Chats) ? lastMsg($room->id)['sender_id'] : '',
            ];
        }

        /** Send Data **/
        return apiResponse('1', trans('api.send'), $data);
    }

    #show chat
    public function show_chat(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'from_id'   => 'required|exists:users,id',
            'to_id'     => 'required|exists:users,id',
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        /** Data **/
        $from_id    = $request->from_id;
        $to_id      = $request->to_id;

        #find the chat room or create one
        $room  = Room::where('user_id', $from_id)->where('doctor_id', $to_id)->first();
        if (!isset($room)) $room  = Room::where('user_id', $to_id)->where('doctor_id', $from_id)->first();
        if (!isset($room)) { //has not room so create one
            $room = new Room;
            $room->user_id      = $from_id;
            $room->doctor_id    = $to_id;
            $room->save();
        }

        #make user seen messages these sent to him
        $chat_ids = $room->Chats->pluck('id')->toArray();
        Room_chat::whereIn('id', $chat_ids)->where('to_id', $from_id)->where('seen', '0')->update(['seen' => '1']);

        return apiResponse('1', trans('api.send'), ChatCollection::collection($room->Chats));
    }

    /*
    |----------------------------------------------------|
    |----------------------------------------------------|
    |             notification Page Start                |
    |----------------------------------------------------|
    |----------------------------------------------------|
    */

    #all notification
    public function show_all_notification(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'user_id'   => 'required|exists:users,id',
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        /** Update seen **/
        Notification::where('to_id', $request->user_id)->where('seen', '0')->update(['seen' => '1']);

        /** Send Data **/
        return apiResponse('1', trans('api.send'), NotificationCollection::collection(Notification::where('to_id', $request->user_id)->orderBy('id', 'desc')->get()));
    }

    #delete notification
    public function delete_notification(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'notification_id'   => 'required|exists:notifications,id',
        ]);

        /** Send Error Massages **/
        if ($validate->fails())
            return apiResponse('0', $validate->errors()->first());

        /** Send Data **/
        Notification::whereId($request->notification_id)->delete();

        return apiResponse('1', trans('api.delete'));
    }
}
