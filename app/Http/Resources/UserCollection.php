<?php

namespace App\Http\Resources;

// use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;
use App;

class UserCollection extends JsonResource
{
    public function toArray($request)
    {
        // return parent::toArray($request);
        $name    = App::getLocale() == 'en' ? 'name_en' : 'name_ar';
        $title   = App::getLocale() == 'en' ? 'title_en' : 'title_ar';
        return [
            'id'                => (int)    $this->id,
            'name'              => (string) $this->$name,
            'name_ar'           => (string) $this->name_ar,
            'name_en'           => (string) $this->name_en,
            'phone'             => (string) $this->phone,
            'email'             => (string) $this->email,
            'gender'            => (string) $this->gender,
            'doctor'            => (string) $this->provider ?? '1',
            'complete'          => (string) $this->complete ?? '1',
            'lang'              => (string) $this->lang ?? 'ar',
            'avatar'            => !is_null($this->avatar) ? url('public/images/users/' . $this->avatar) : url('public/images/users/default.png'),
            'is_active'         => $this->activation == '1' ? true : false,
            'is_blocked'        => $this->checked == '0' ? true : false,
            'city_id'           => !is_null($this->City) ? (int) $this->City->id : 0,
            'city_title'        => !is_null($this->City) ? (string) $this->City->$title : '',
        ];
    }
}
