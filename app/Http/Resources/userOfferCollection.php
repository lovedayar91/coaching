<?php

namespace App\Http\Resources;

// use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\RateCollection;
use App\Http\Resources\RateOfferCollection;
use App;
use Carbon\Carbon;
use App\Models\Rate;

class userOfferCollection extends JsonResource
{
    public function toArray($request)
    {
        // return parent::toArray($request);
        $lang     = App::getLocale() == 'en' ? 'en' : 'ar';
        $name     = App::getLocale() == 'en' ? 'name_en' : 'name_ar';
        $address  = App::getLocale() == 'en' ? 'address_en' : 'address_ar';
        $title    = App::getLocale() == 'en' ? 'title_en' : 'title_ar';
        $desc     = App::getLocale() == 'en' ? 'desc_en' : 'desc_ar';
        return [
            'id'                 => (int)    $this->id,
            'image'              => is_null($this->image) ? '' : url('' . $this->image),
            'title'              => (string) $this->$title,
            'desc'               => (string) $this->$desc,
            'old_price'          => (float)  $this->old_price,
            'new_price'          => (float)  $this->new_price,
            'date'               => (string) Carbon::parse($this->created_at)->format('Y-m-d'),
            'user_avatar'        => !is_null($this->User) && !is_null($this->User->avatar) ? url('public/images/users/' . $this->User->avatar) : url('public/images/users/default.png'),
            'user_name'          => !is_null($this->User) ? (string) $this->User->$name :'',
            'user_address'       => !is_null($this->User) ? (string) $this->User->$address :'',
            'has_offer'          => !is_null($this->User) ? has_offer($this->user_id) : false,
            'user_id'            => is_null($this->User) ? 0 : (int)    $this->user_id,
            'user_phone'         => is_null($this->User) ? '' : (string) $this->User->phone,
            'user_email'         => is_null($this->User) ? '' : (string) $this->User->email,
            'user_gender'        => is_null($this->User) ? '' : (string) $this->User->gender,
            'user_price'         => is_null($this->User) ? '' : (string) $this->User->price,
            'user_lat'           => is_null($this->User) ? '' : (string) $this->User->lat,
            'user_lng'           => is_null($this->User) ? '' : (string) $this->User->lng,
            'rate'               => (int) Rate::where('offer_id', $this->id)->avg('rate'),
            'rate_count'         => (int) Rate::where('offer_id', $this->id)->count(),
            'user_rate'          => (int) Rate::where('to_id', $this->user_id)->avg('rate'),
            'user_rate_count'    => (int) Rate::where('to_id', $this->user_id)->count(),
            'distance'           => (float) round($this->distance, 2),
            'days_desc'          => (string) showDaysDesc($this->user_id, $lang,false),
            'dates_desc'         => (string) showDatesDesc($this->user_id, $lang,false),
            'offer_rates'        => RateOfferCollection::collection($this->Rates),
            'doctor_rates'       => RateCollection::collection($this->User->Rates)
        ];
    }
}
