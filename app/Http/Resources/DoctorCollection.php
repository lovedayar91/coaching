<?php

namespace App\Http\Resources;

// use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\DaysCollection;
use App\Http\Resources\PricesCollection;
use App\Http\Resources\RateCollection;
use App\Models\Rate;
use App;
use App\Models\Report_reson;
use Carbon\Carbon;

class DoctorCollection extends JsonResource
{
    public function toArray($request)
    {
        // return parent::toArray($request);
        $lang           = App::getLocale() == 'en' ? 'en' : 'ar';
        $name           = App::getLocale() == 'en' ? 'name_en' : 'name_ar';
        $title          = App::getLocale() == 'en' ? 'title_en' : 'title_ar';
        $address        = App::getLocale() == 'en' ? 'address_en' : 'address_ar';
        $desc           = App::getLocale() == 'en' ? 'desc_en' : 'desc_ar';
        $job            = App::getLocale() == 'en' ? 'job_en' : 'job_ar';
        $qualifaction   = App::getLocale() == 'en' ? 'qualifaction_en' : 'qualifaction_ar';
        $specialist     = App::getLocale() == 'en' ? 'public specialist' : 'اختصاصي عام';
        return [
            'id'                => (int)    $this->id,
            'name_ar'           => (string) $this->name_ar,
            'name_en'           => (string) $this->name_en,
            'name'              => (string) $this->$name,
            'phone'             => (string) $this->phone,
            'email'             => (string) $this->email,
            'gender'            => (string) $this->gender,
            'lat'               => (string) $this->lat,
            'lng'               => (string) $this->lng,
            'price'             => (string) $this->price,
            'address'           => (string) $this->$address,
            'address_ar'        => (string) $this->address_ar,
            'address_en'        => (string) $this->address_en,
            'desc'              => (string) $this->$desc,
            'desc_ar'           => (string) $this->desc_ar,
            'desc_en'           => (string) $this->desc_en,
            'job'               => (string) $this->$job,
            'job_ar'            => (string) $this->job_ar,
            'job_en'            => (string) $this->job_en,
            'qualifaction'      => (string) $this->$qualifaction,
            'qualifaction_ar'   => (string) $this->qualifaction_ar,
            'qualifaction_en'   => (string) $this->qualifaction_en,
            'start_at'          => (string) Carbon::parse($this->start_at)->format('h:i a'),
            'end_at'            => (string) Carbon::parse($this->end_at)->format('h:i a'),
            'facebook'          => (string) $this->facebook,
            'twitter'           => (string) $this->twitter,
            'instagram'         => (string) $this->instagram,
            'snapchat'          => (string) $this->snapchat,
            'rate'              => (int) Rate::where('to_id', $this->id)->avg('rate'),
            'rate_count'        => (int) Rate::where('to_id', $this->id)->count(),
            'lang'              => (string) $this->lang ?? 'ar',
            'avatar'            => !is_null($this->avatar) ? url('public/images/users/' . $this->avatar) : url('public/images/users/default.png'),
            'city_id'           => !is_null($this->City) ? (int) $this->City->id : 0,
            'city_title'        => !is_null($this->City) ? (string) $this->City->$title : '',
            'package_id'        => !is_null($this->Package) ? (int) $this->Package->id : 0,
            'package_title'     => !is_null($this->Package) ? (string) $this->Package->$title : '',
            'package_end_at'    => (string) Carbon::parse($this->end_package)->format('Y-m-d'),
            'package_status'    => (bool) Carbon::parse($this->end_package)->greaterThanOrEqualTo(Carbon::now()),
            'specialist_id'     => !is_null($this->Specialist) ? (int) $this->Specialist->id : 0,
            'specialist_title'  => !is_null($this->Specialist) ? (string) $this->Specialist->$title : $specialist,
            'days_desc'         => (string) showDaysDesc($this->id, $lang,false),
            'dates_desc'        => (string) showDatesDesc($this->id, $lang,false),
            'has_offer'         => has_offer($this->id) ,
            'days'              => showDaysData($this,$title),
            'prices'            => PricesCollection::collection($this->Prices),
            'report_resaons'    => ReportResonCollection::collection(Report_reson::get()),
            'rates'             => RateCollection::collection($this->Rates),
        ];
    }
}
