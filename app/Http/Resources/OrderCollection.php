<?php

namespace App\Http\Resources;

// use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;
use App;
use App\Http\Resources\OrderOptionCollection;

class OrderCollection extends JsonResource
{
    public function toArray($request)
    {
        // return parent::toArray($request);
        $title   = App::getLocale() == 'en' ? 'title_en' : 'title_ar';
        $desc    = App::getLocale() == 'en' ? 'desc_en' : 'desc_ar';
        $cash    = App::getLocale() == 'en' ? 'Cash' : 'كاش';
        $onLine  = App::getLocale() == 'en' ? 'OnLine' : 'اون لاين';
        return [
            'id'                 => (int)    $this->id,
            'order_number'       => !is_null($this->order_number) ? (string) $this->order_number : (string) $this->id,
            'status'             => (int)    $this->status, //[o=>new,1=>agree,2=>refused,3=>has_provider,4=>in_way,5=>finishFromClient,6=>finishFromProvider]
            'lat'                => (float)  $this->lat,
            'lng'                => (float)  $this->lng,
            'day'                => !is_null($this->Day) ? (string)  $this->Day->$title : '',
            'date'               => (string) $this->date,
            'payment_method'     => $this->payment_method == 0 ? $cash : $onLine, //[0=>cash,1=>onLine]
            'delivery'           => (float)  $this->delivery,
            'value_added'        => (float)  $this->value_added,
            'total_before_promo' => (float)  $this->total_before_promo,
            'total_after_promo'  => (float)  $this->total_after_promo,

            'city_title'         => !is_null($this->City) ? (string) $this->City->$title : '',
            'neighborhood_title' => !is_null($this->Neighborhood) ? (string) $this->Neighborhood->$title : '',
            'other_data'         => (string) $this->other_data,

            'user_name'          => !is_null($this->User) && is_null($this->name) ? (string) $this->User->name : (string) $this->name,
            'user_phone'         => !is_null($this->User) && is_null($this->phone) ? (string) $this->User->phone : (string) $this->phone,
            'user_avatar'        => !is_null($this->User) ? url('public/images/users/' . $this->User->avatar) : '',

            'provider_name'      => !is_null($this->Provider) && is_null($this->name) ? (string) $this->Provider->name : (string) $this->name,
            'provider_phone'     => !is_null($this->Provider) && is_null($this->phone) ? (string) $this->Provider->phone : (string) $this->phone,
            'provider_avatar'    => !is_null($this->Provider) ? url('public/images/Providers/' . $this->Provider->avatar) : '',

            'order_services'    => OrderOptionCollection::collection($this->Order_options)
        ];
    }
}