<?php

namespace App\Http\Resources;

// use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;
use App;

class CartCollection extends JsonResource
{
    public function toArray($request)
    {
        // return parent::toArray($request);
        $title         = App::getLocale() == 'en' ? 'title_en' : 'title_ar';
        $short_desc    = App::getLocale() == 'en' ? 'short_desc_en' : 'short_desc_ar';
        $desc          = App::getLocale() == 'en' ? 'desc_en' : 'desc_ar';

        $service_total = is_null($this->Service) ? 0 : (float) $this->Service->price * (int) $this->amount;
        $cut_total     = is_null($this->Cut) ? 0 : (float) $this->Cut->price * (int) $this->amount;
        $total         = $service_total + $cut_total;
        return [
            'id'                    => (int)   $this->id,
            'service_id'            => (int)   $this->service_id,
            'service_title'         => is_null($this->Service) ? '' : (string) $this->Service->$title,
            'service_short_desc'    => is_null($this->Service) ? '' : (string) $this->Service->$short_desc,
            'service_desc'          => is_null($this->Service) ? '' : (string) $this->Service->$desc,
            'service_price'         => is_null($this->Service) ? 0 : (float) $this->Service->price,
            'service_first_image'   => is_null($this->Service) ? '' : url('' . $this->Service->Images->first()->image),

            'cut_id'                => (int)   $this->cut_id,
            'cut_title'             => is_null($this->Cut) ? 0 : (float) $this->Cut->$title,
            'cut_price'             => is_null($this->Cut) ? 0 : (float) $this->Cut->price,

            'amount'                => (int)   $this->amount,
            'service_total'         => $service_total,
            'cut_total'             => $cut_total,
            'total'                 => $total,
        ];
    }
}
