<?php

namespace App\Http\Resources;

// use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\DaysCollection;
use App\Models\Rate;
use App;
use Carbon\Carbon;

class UserDoctorsCollection extends JsonResource
{
    public function toArray($request)
    {
        // return parent::toArray($request);
        $lang           = App::getLocale() == 'en' ? 'en' : 'ar';
        $name           = App::getLocale() == 'en' ? 'name_en' : 'name_ar';
        $title          = App::getLocale() == 'en' ? 'title_en' : 'title_ar';
        $address        = App::getLocale() == 'en' ? 'address_en' : 'address_ar';
        $desc           = App::getLocale() == 'en' ? 'desc_en' : 'desc_ar';
        $job            = App::getLocale() == 'en' ? 'job_en' : 'job_ar';
        $specialist     = App::getLocale() == 'en' ? 'public specialist' : 'اختصاصي عام';
        return [
            'id'                => (int)    $this->id,
            'name'              => (string) $this->$name,
            'phone'             => (string) $this->phone,
            'email'             => (string) $this->email,
            'gender'            => (string) $this->gender,
            'lat'               => (string) $this->lat,
            'lng'               => (string) $this->lng,
            'desc'              => (string) $this->$desc,
            'price'             => (string) $this->price,
            'address'           => (string) $this->$address,
            'job'               => (string) $this->$job,
            'start_at'          => (string) Carbon::parse($this->start_at)->format('h:i a'),
            'end_at'            => (string) Carbon::parse($this->end_at)->format('h:i a'),
            'distance'          => (float) round($this->distance, 2),
            'rate'              => (int) Rate::where('to_id', $this->id)->avg('rate'),
            'rate_count'        => (int) Rate::where('to_id', $this->id)->count(),
            'avatar'            => !is_null($this->avatar) ? url('public/images/users/' . $this->avatar) : url('public/images/users/default.png'),
            'city_id'           => !is_null($this->City) ? (int) $this->City->id : 0,
            'city_title'        => !is_null($this->City) ? (string) $this->City->$title : '',
            'specialist_id'     => !is_null($this->Specialist) ? (int) $this->Specialist->id : 0,
            'specialist_title'  => !is_null($this->Specialist) ? (string) $this->Specialist->$title : $specialist,
            'has_offer'         => has_offer($this->id) ,
            'days_desc'         => (string) showDaysDesc($this->id, $lang),
            'dates_desc'        => (string) showDatesDesc($this->id, $lang),
            'days'              => showDaysData($this,$title),
        ];
    }
}
