<?php

namespace App\Http\Resources;

// use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\DaysCollection;
use App\Models\Rate;
use App;
use Carbon\Carbon;

class UserFavouritesCollection extends JsonResource
{
    public function toArray($request)
    {
        // return parent::toArray($request);
        $lang           = App::getLocale() == 'en' ? 'en' : 'ar';
        $name           = App::getLocale() == 'en' ? 'name_en' : 'name_ar';
        $title          = App::getLocale() == 'en' ? 'title_en' : 'title_ar';
        $address        = App::getLocale() == 'en' ? 'address_en' : 'address_ar';
        $job            = App::getLocale() == 'en' ? 'job_en' : 'job_ar';
        $desc           = App::getLocale() == 'en' ? 'desc_en' : 'desc_ar';
        $specialist     = App::getLocale() == 'en' ? 'public specialist' : 'اختصاصي عام';
        return [
            'id'                => (int)    $this->Doctor->id,
            'name'              => (string) $this->Doctor->$name,
            'phone'             => (string) $this->Doctor->phone,
            'email'             => (string) $this->Doctor->email,
            'gender'            => (string) $this->Doctor->gender,
            'lat'               => (string) $this->Doctor->lat,
            'lng'               => (string) $this->Doctor->lng,
            'desc'              => (string) $this->Doctor->$desc,
            'price'             => (string) $this->Doctor->price,
            'address'           => (string) $this->Doctor->$address,
            'job'               => (string) $this->Doctor->$job,
            'has_offer'         => has_offer($this->Doctor->id) ,
            'start_at'          => (string) Carbon::parse($this->Doctor->start_at)->format('h:i a'),
            'end_at'            => (string) Carbon::parse($this->Doctor->end_at)->format('h:i a'),
            'distance'          => (float) round($this->Doctor->distance, 2),
            'rate'              => (int) Rate::where('to_id', $this->Doctor->id)->avg('rate'),
            'rate_count'        => (int) Rate::where('to_id', $this->Doctor->id)->count(),
            'avatar'            => !is_null($this->Doctor->avatar) ? url('public/images/users/' . $this->Doctor->avatar) : url('public/images/users/default.png'),
            'city_id'           => !is_null($this->Doctor->City) ? (int) $this->Doctor->City->id : 0,
            'city_title'        => !is_null($this->Doctor->City) ? (string) $this->Doctor->City->$title : '',
            'specialist_id'     => !is_null($this->Doctor->Specialist) ? (int) $this->Doctor->Specialist->id : 0,
            'specialist_title'  => !is_null($this->Doctor->Specialist) ? (string) $this->Doctor->Specialist->$title : $specialist,
            'days_desc'         => (string) showDaysDesc($this->Doctor->id, $lang),
            'dates_desc'        => (string) showDatesDesc($this->Doctor->id, $lang),
            'days'              => DaysCollection::collection($this->Doctor->Days),
        ];
    }
}
