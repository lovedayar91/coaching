<?php

namespace App\Http\Resources;

// use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\App;

class RateCollection extends JsonResource
{
    public function toArray($request)
    {
        // return parent::toArray($request);
        $name           = App::getLocale() == 'en' ? 'name_en' : 'name_ar';
        return [
            'id'          => (int)    $this->id,
            'rate'        => (int)    $this->rate,
            'comment'     => (string) $this->comment,
            'report'      => (int)    $this->report,
            'date'        => date('Y-m-d', strtotime($this->created_at)),
            'user_id'     => (int)    $this->From->id,
            'user_name'   => !is_null($this->From) ? (string) $this->From->name_ar : '',
            // 'user_avatar' => !is_null($this->From) &&  !is_null($this->From->avatar) ? url('public/images/users/' . $this->From->avatar) : '',
        ];
    }
}
