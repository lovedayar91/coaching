<?php

namespace App\Http\Resources;

// use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\App;

class RoomCollection extends JsonResource
{
    public function toArray($request)
    {
        // return parent::toArray($request);
        $name           = App::getLocale() == 'en' ? 'name_en' : 'name_ar';
        return [
            'id'            => (int)    $this->id,
            'from_id'       => (int)    $this->User->id,
            'from_name'     => !is_null($this->User) ? (string) $this->User->$name : '',
            'from_avatar'   => !is_null($this->User) &&  !is_null($this->User->avatar) ? url('public/images/users/' . $this->User->avatar) : '',
            'to_id'         => (int)    $this->Doctor->id,
            'to_name'       => !is_null($this->Doctor) ? (string) $this->Doctor->$name : '',
            'to_avatar'     => !is_null($this->Doctor) &&  !is_null($this->Doctor->avatar) ? url('public/images/users/' . $this->Doctor->avatar) : '',
            'last_message'  => !is_null($this->Chats) ? lastMsg($this->id)['last_message'] : '',
            'sender_id'     => !is_null($this->Chats) ? lastMsg($this->id)['sender_id'] : '',
        ];
    }
}
