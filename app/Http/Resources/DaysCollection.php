<?php

namespace App\Http\Resources;

// use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;
use App;
use Carbon\Carbon;

class DaysCollection extends JsonResource
{
    public function toArray($request)
    {
        // return parent::toArray($request);
        $title   = App::getLocale() == 'en' ? 'title_en' : 'title_ar';
        return [
            'id'                => (int)    $this->id,
            'title'             => (string) $this->Day->$title,
            'title_ar'          => (string) $this->Day->title_ar,
            'title_en'          => (string) $this->Day->title_en,
            'start_at'          => (string) Carbon::parse($this->start_at)->format('h:i a'),
            'end_at'            => (string) Carbon::parse($this->end_at)->format('h:i a'),
        ];
    }
}
