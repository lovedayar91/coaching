<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Favourite;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * @method where(string $string, int $int)
 * @method static create(array $array)
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'phone_code', 'phone', 'code', 'avatar', 'provider', 'device_id', 'active', 'checked', 'activation', 'complete', 'notify_send', 'country_id', 'city_id', 'role', 'lat', 'lng', 'gender', 'session_count', 'job', 'price', 'birthdate', 'social_status', 'device_type', 'lang'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function Role()
    {
        return $this->hasOne('App\Models\Role', 'id', 'role');
    }

    public function City()
    {
        return $this->belongsTo('App\Models\City', 'city_id', 'id');
    }

    public function Country()
    {
        return $this->belongsTo('App\Models\Country', 'country_id', 'id');
    }

    public function Specialist()
    {
        return $this->belongsTo('App\Models\Specialist', 'specialist_id', 'id');
    }

    public function Devices()
    {
        return $this->hasMany('App\Models\Device', 'user_id', 'id');
    }
}
