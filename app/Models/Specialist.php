<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Specialist extends Model
{
    protected $table = 'specialists';
    protected $fillable = [
        'title_ar', 'title_en'
    ];

    public function Users()
    {
        return $this->hasMany('App\User', 'specialist_id', 'id');
    }
}
