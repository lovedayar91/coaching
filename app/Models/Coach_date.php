<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coach_date extends Model
{
    protected $table = 'coach_dates';
    protected $fillable = [
        'date', 'coach_id'
    ];

    public function Times()
    {
        return $this->hasMany('App\Models\Coach_time', 'coach_date_id', 'id');
    }

    public function Coach()
    {
        return $this->belongsTo('App\Models\Coach', 'coach_id', 'id');
    }
}
