<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $fillable = [
        'title_ar', 'title_en', 'desc_ar', 'desc_en', 'price', 'session_count', 'section_id'
    ];

    public function Section()
    {
        return $this->belongsTo('App\Models\Section', 'section_id', 'id');
    }
}
