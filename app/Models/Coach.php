<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Coach extends Model
{
    protected $table = 'coachs';
    protected $fillable = [
        'name_ar', 'name_en', 'desc_ar', 'desc_en', 'email', 'skype_id'
    ];

    public function Dates()
    {
        return $this->hasMany('App\Models\Coach_date', 'coach_id', 'id');
    }

    public function current_dates()
    {
        return $this->hasMany('App\Models\Coach_date', 'coach_id', 'id')->whereDate('date', '>=', Carbon::now());
    }

    public function Times()
    {
        return $this->hasMany('App\Models\Coach_time', 'coach_id', 'id');
    }

    public function Images()
    {
        return $this->hasMany('App\Models\Coach_image', 'coach_id', 'id');
    }
}
