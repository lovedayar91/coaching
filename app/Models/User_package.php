<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User_package extends Model
{
    protected $fillable = [
        'package_id', 'section_id', 'user_id', 'session_count'
    ];

    public function Package()
    {
        return $this->belongsTo('App\Models\Package', 'package_id', 'id');
    }

    public function User()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
