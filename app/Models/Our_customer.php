<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Our_customer extends Model
{
    protected $fillable = [
        'url', 'title_ar', 'title_en'
    ];
}
