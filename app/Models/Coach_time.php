<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coach_time extends Model
{
    protected $table = 'coach_times';
    protected $fillable = [
        'time_id', 'coach_id', 'coach_date_id'
    ];

    public function Coach()
    {
        return $this->belongsTo('App\Models\Coach', 'coach_id', 'id');
    }

    public function Date()
    {
        return $this->belongsTo('App\Models\Coach_date', 'coach_date_id', 'id');
    }

    public function Time()
    {
        return $this->belongsTo('App\Models\Time_format', 'time_id', 'id');
    }
}
