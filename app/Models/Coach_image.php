<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coach_image extends Model
{
    protected $table = 'coach_images';
    protected $fillable = [
        'image', 'coach_id'
    ];

    public function Coach()
    {
        return $this->belongsTo('App\Models\Coach', 'coach_id', 'id');
    }
}
