<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Time_format extends Model
{
    protected $table = 'time_formats';
    protected $fillable = [
        'title_ar', 'title_en', 'time'
    ];
}
