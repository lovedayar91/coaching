<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $fillable = [
        'title_ar', 'title_en', 'image', 'active'
    ];

    public function Packages()
    {
        return $this->hasMany('App\Models\Package', 'section_id', 'id');
    }

    static function activeSection()
    {
        return self::where('active', '1')->get();
    }
}
