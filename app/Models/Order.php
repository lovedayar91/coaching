<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'date', 'coach_id', 'user_id', 'time_id', 'section_id',
    ];

    public function User()
    {
        return $this->belongsTo('App\User',  'user_id',  'id');
    }

    public function Coach()
    {
        return $this->belongsTo('App\Models\Coach',  'coach_id',  'id');
    }

    public function Section()
    {
        return $this->belongsTo('App\Models\Section',  'section_id',  'id');
    }

    public function Time()
    {
        return $this->belongsTo('App\Models\Time_format',  'time_id',  'id');
    }
}
