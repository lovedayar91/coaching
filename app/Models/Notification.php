<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = [
        'from_id', 'to_id', 'message_ar', 'message_en', 'seen', 'confirm', 'type'
    ];

    public function To()
    {
        return $this->belongsTo('App\User',  'to_id',  'id');
    }

    public function From()
    {
        return $this->belongsTo('App\User',  'from_id',  'id');
    }

    public function Order()
    {
        return $this->belongsTo('App\Models\Order',  'order_id',  'id');
    }

    static function getUserNotification($user_id)
    {
        return self::where('to_id', (int)$user_id)->orderBy('id', 'desc')->get();
    }
}
