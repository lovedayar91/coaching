<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Register_law extends Model
{
    protected $fillable = [
        'title_ar', 'title_en', 'desc_ar', 'desc_en'
    ];
}
