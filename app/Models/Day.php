<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Day extends Model
{
    protected $table = 'days';
    protected $fillable = [
        'title_ar', 'title_en', 'iso_ar', 'iso_en'
    ];

    public function Days()
    {
        return $this->hasMany('App\Models\City_day', 'day_id', 'id');
    }
}
